package net.metrosystems.domain;

import java.util.Date;

public class CreditRequest {
	
	private String initiatedBy;
	private String versionId;
	private Long currentLimit;
	private Long requestedLimit;
	private Long calculatedMaxLimit;
	private Long approvedLimit;
	private Date startedAt;
	private Date finishedAt;
	private Boolean requestApproved;
	private Boolean requestedLimitApproved;
	private Date limitExpiryDate;
	private Date workflowStartedAt;
	private Date workflowFinishedAt;
	private Long trafficLightId;
	private Integer internalScore;
	private String approvedBy;
	private Integer ruleProcessReasonID;
	private String ruleProcessReasonDesc;
	private Long requestedCustLimit;
	private Long approvedCustLimit;
	private Long creditProgramId;

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public String getVersionId() {
		return versionId;
	}

	public Long getCurrentLimit() {
		return currentLimit;
	}

	public Long getRequestedLimit() {
		return requestedLimit;
	}

	public Long getCalculatedMaxLimit() {
		return calculatedMaxLimit;
	}

	public Long getApprovedLimit() {
		return approvedLimit;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public Boolean getRequestApproved() {
		return requestApproved;
	}

	public Boolean getRequestedLimitApproved() {
		return requestedLimitApproved;
	}

	public Date getLimitExpiryDate() {
		return limitExpiryDate;
	}

	public Date getWorkflowStartedAt() {
		return workflowStartedAt;
	}

	public Date getWorkflowFinishedAt() {
		return workflowFinishedAt;
	}

	public Long getTrafficLightId() {
		return trafficLightId;
	}

	public Integer getInternalScore() {
		return internalScore;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public Integer getRuleProcessReasonID() {
		return ruleProcessReasonID;
	}

	public String getRuleProcessReasonDesc() {
		return ruleProcessReasonDesc;
	}

	public Long getRequestedCustLimit() {
		return requestedCustLimit;
	}
	
	public Long getCreditProgramId() {
		return creditProgramId;
	}

	public Long getApprovedCustLimit() {
		return approvedCustLimit;
	}

}
