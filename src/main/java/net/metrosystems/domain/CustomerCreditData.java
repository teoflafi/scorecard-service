package net.metrosystems.domain;

import java.util.List;


public class CustomerCreditData {

	private long id;
	private String areacd;
	private Long avgDelay;
	private Long clientCd;
	private Long custNo;
	private Long faxNoInd;
	private Long homeStoreId;
	private Boolean isScoredData;
	private Long legalCaseInd;
	private Long legalDurationTime;
	private Long memberOfLottoTotto;
	private Long metroPlusRelation;
	private Long monitionLevelMaxl12m;
	private Long monthId;
	private Long noDunningL1l12m;
	private Long noDunningL2l12m;
	private Long noDunningL3l12m;
	private Long noDunningL4l12m;
	private Long numDebitEntriesl12m;
	private Long numInvoicesl12m;
	private Long numInvoicesl1m;
	private Long numInvoicesl3m;
	private Long numInvoicesl6m;
	private Long numInvoiceslym;
	private Long numInvoicesytm;
	private Long numPurchasesfStorel12m;
	private Long numPurchasesfStorel1m;
	private Long numPurchasesfStorel3m;
	private Long numPurchasesfStorel6m;
	private Long numPurchasesl12m;
	private Long numPurchasesl1m;
	private Long numPurchasesl3m;
	private Long numPurchasesl6m;
	private Long percDelayedPayment;
	private String phoneNo;
	private Long sellValDfdMax1l12m;
	private Long sellValDfdMax2l12m;
	private Long sellValdfMaxl12m;
	private Long sellValLnnbpl12m;
	private Long sellValNspAvgl6m;
	private Long sellValNspfl12m;
	private Long sellValNspForecast;
	private Long sellValNspForecastStore;
	private Long sellValNspl12m;
	private Long sellValNspl12mfStore;
	private Long sellValNspl12mhStore;
	private Long sellValNspl1m;
	private Long sellValNspl24m;
	private Long sellValNspl3m;
	private Long sellValNspl6m;
	private Long sellValNsplyl3m;
	private Long sellValNsplytm;
	private Long sellValNspnfl12m;
	private Long sellValNspTrendl6m6m;
	private Long sellValNspytm;
	private Long timezoneCd;
	private Long topDevelopClassCd;
	private Long trendSalesl12m12mBack;
	private Long trendSalesylyBack;
	private Long valDelayedTransaction;
	private String requestedPaymentTerms;
	private List<DynamicIndicator> dynamicIndicatorList;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAreacd() {
		return areacd;
	}
	public void setAreacd(String areacd) {
		this.areacd = areacd;
	}
	public Long getAvgDelay() {
		return avgDelay;
	}
	public void setAvgDelay(Long avgDelay) {
		this.avgDelay = avgDelay;
	}
	public Long getClientCd() {
		return clientCd;
	}
	public void setClientCd(Long clientCd) {
		this.clientCd = clientCd;
	}
	public Long getCustNo() {
		return custNo;
	}
	public void setCustNo(Long custNo) {
		this.custNo = custNo;
	}
	public Long getFaxNoInd() {
		return faxNoInd;
	}
	public void setFaxNoInd(Long faxNoInd) {
		this.faxNoInd = faxNoInd;
	}
	public Long getHomeStoreId() {
		return homeStoreId;
	}
	public void setHomeStoreId(Long homeStoreId) {
		this.homeStoreId = homeStoreId;
	}
	public Boolean getIsScoredData() {
		return isScoredData;
	}
	public void setIsScoredData(Boolean isScoredData) {
		this.isScoredData = isScoredData;
	}
	public Long getLegalCaseInd() {
		return legalCaseInd;
	}
	public void setLegalCaseInd(Long legalCaseInd) {
		this.legalCaseInd = legalCaseInd;
	}
	public Long getLegalDurationTime() {
		return legalDurationTime;
	}
	public void setLegalDurationTime(Long legalDurationTime) {
		this.legalDurationTime = legalDurationTime;
	}
	public Long getMemberOfLottoTotto() {
		return memberOfLottoTotto;
	}
	public void setMemberOfLottoTotto(Long memberOfLottoTotto) {
		this.memberOfLottoTotto = memberOfLottoTotto;
	}
	public Long getMetroPlusRelation() {
		return metroPlusRelation;
	}
	public void setMetroPlusRelation(Long metroPlusRelation) {
		this.metroPlusRelation = metroPlusRelation;
	}
	public Long getMonitionLevelMaxl12m() {
		return monitionLevelMaxl12m;
	}
	public void setMonitionLevelMaxl12m(Long monitionLevelMaxl12m) {
		this.monitionLevelMaxl12m = monitionLevelMaxl12m;
	}
	public Long getMonthId() {
		return monthId;
	}
	public void setMonthId(Long monthId) {
		this.monthId = monthId;
	}
	public Long getNoDunningL1l12m() {
		return noDunningL1l12m;
	}
	public void setNoDunningL1l12m(Long noDunningL1l12m) {
		this.noDunningL1l12m = noDunningL1l12m;
	}
	public Long getNoDunningL2l12m() {
		return noDunningL2l12m;
	}
	public void setNoDunningL2l12m(Long noDunningL2l12m) {
		this.noDunningL2l12m = noDunningL2l12m;
	}
	public Long getNoDunningL3l12m() {
		return noDunningL3l12m;
	}
	public void setNoDunningL3l12m(Long noDunningL3l12m) {
		this.noDunningL3l12m = noDunningL3l12m;
	}
	public Long getNoDunningL4l12m() {
		return noDunningL4l12m;
	}
	public void setNoDunningL4l12m(Long noDunningL4l12m) {
		this.noDunningL4l12m = noDunningL4l12m;
	}
	public Long getNumDebitEntriesl12m() {
		return numDebitEntriesl12m;
	}
	public void setNumDebitEntriesl12m(Long numDebitEntriesl12m) {
		this.numDebitEntriesl12m = numDebitEntriesl12m;
	}
	public Long getNumInvoicesl12m() {
		return numInvoicesl12m;
	}
	public void setNumInvoicesl12m(Long numInvoicesl12m) {
		this.numInvoicesl12m = numInvoicesl12m;
	}
	public Long getNumInvoicesl1m() {
		return numInvoicesl1m;
	}
	public void setNumInvoicesl1m(Long numInvoicesl1m) {
		this.numInvoicesl1m = numInvoicesl1m;
	}
	public Long getNumInvoicesl3m() {
		return numInvoicesl3m;
	}
	public void setNumInvoicesl3m(Long numInvoicesl3m) {
		this.numInvoicesl3m = numInvoicesl3m;
	}
	public Long getNumInvoicesl6m() {
		return numInvoicesl6m;
	}
	public void setNumInvoicesl6m(Long numInvoicesl6m) {
		this.numInvoicesl6m = numInvoicesl6m;
	}
	public Long getNumInvoiceslym() {
		return numInvoiceslym;
	}
	public void setNumInvoiceslym(Long numInvoiceslym) {
		this.numInvoiceslym = numInvoiceslym;
	}
	public Long getNumInvoicesytm() {
		return numInvoicesytm;
	}
	public void setNumInvoicesytm(Long numInvoicesytm) {
		this.numInvoicesytm = numInvoicesytm;
	}
	public Long getNumPurchasesfStorel12m() {
		return numPurchasesfStorel12m;
	}
	public void setNumPurchasesfStorel12m(Long numPurchasesfStorel12m) {
		this.numPurchasesfStorel12m = numPurchasesfStorel12m;
	}
	public Long getNumPurchasesfStorel1m() {
		return numPurchasesfStorel1m;
	}
	public void setNumPurchasesfStorel1m(Long numPurchasesfStorel1m) {
		this.numPurchasesfStorel1m = numPurchasesfStorel1m;
	}
	public Long getNumPurchasesfStorel3m() {
		return numPurchasesfStorel3m;
	}
	public void setNumPurchasesfStorel3m(Long numPurchasesfStorel3m) {
		this.numPurchasesfStorel3m = numPurchasesfStorel3m;
	}
	public Long getNumPurchasesfStorel6m() {
		return numPurchasesfStorel6m;
	}
	public void setNumPurchasesfStorel6m(Long numPurchasesfStorel6m) {
		this.numPurchasesfStorel6m = numPurchasesfStorel6m;
	}
	public Long getNumPurchasesl12m() {
		return numPurchasesl12m;
	}
	public void setNumPurchasesl12m(Long numPurchasesl12m) {
		this.numPurchasesl12m = numPurchasesl12m;
	}
	public Long getNumPurchasesl1m() {
		return numPurchasesl1m;
	}
	public void setNumPurchasesl1m(Long numPurchasesl1m) {
		this.numPurchasesl1m = numPurchasesl1m;
	}
	public Long getNumPurchasesl3m() {
		return numPurchasesl3m;
	}
	public void setNumPurchasesl3m(Long numPurchasesl3m) {
		this.numPurchasesl3m = numPurchasesl3m;
	}
	public Long getNumPurchasesl6m() {
		return numPurchasesl6m;
	}
	public void setNumPurchasesl6m(Long numPurchasesl6m) {
		this.numPurchasesl6m = numPurchasesl6m;
	}
	public Long getPercDelayedPayment() {
		return percDelayedPayment;
	}
	public void setPercDelayedPayment(Long percDelayedPayment) {
		this.percDelayedPayment = percDelayedPayment;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Long getSellValDfdMax1l12m() {
		return sellValDfdMax1l12m;
	}
	public void setSellValDfdMax1l12m(Long sellValDfdMax1l12m) {
		this.sellValDfdMax1l12m = sellValDfdMax1l12m;
	}
	public Long getSellValDfdMax2l12m() {
		return sellValDfdMax2l12m;
	}
	public void setSellValDfdMax2l12m(Long sellValDfdMax2l12m) {
		this.sellValDfdMax2l12m = sellValDfdMax2l12m;
	}
	public Long getSellValdfMaxl12m() {
		return sellValdfMaxl12m;
	}
	public void setSellValdfMaxl12m(Long sellValdfMaxl12m) {
		this.sellValdfMaxl12m = sellValdfMaxl12m;
	}
	public Long getSellValLnnbpl12m() {
		return sellValLnnbpl12m;
	}
	public void setSellValLnnbpl12m(Long sellValLnnbpl12m) {
		this.sellValLnnbpl12m = sellValLnnbpl12m;
	}
	public Long getSellValNspAvgl6m() {
		return sellValNspAvgl6m;
	}
	public void setSellValNspAvgl6m(Long sellValNspAvgl6m) {
		this.sellValNspAvgl6m = sellValNspAvgl6m;
	}
	public Long getSellValNspfl12m() {
		return sellValNspfl12m;
	}
	public void setSellValNspfl12m(Long sellValNspfl12m) {
		this.sellValNspfl12m = sellValNspfl12m;
	}
	public Long getSellValNspForecast() {
		return sellValNspForecast;
	}
	public void setSellValNspForecast(Long sellValNspForecast) {
		this.sellValNspForecast = sellValNspForecast;
	}
	public Long getSellValNspForecastStore() {
		return sellValNspForecastStore;
	}
	public void setSellValNspForecastStore(Long sellValNspForecastStore) {
		this.sellValNspForecastStore = sellValNspForecastStore;
	}
	public Long getSellValNspl12m() {
		return sellValNspl12m;
	}
	public void setSellValNspl12m(Long sellValNspl12m) {
		this.sellValNspl12m = sellValNspl12m;
	}
	public Long getSellValNspl12mfStore() {
		return sellValNspl12mfStore;
	}
	public void setSellValNspl12mfStore(Long sellValNspl12mfStore) {
		this.sellValNspl12mfStore = sellValNspl12mfStore;
	}
	public Long getSellValNspl12mhStore() {
		return sellValNspl12mhStore;
	}
	public void setSellValNspl12mhStore(Long sellValNspl12mhStore) {
		this.sellValNspl12mhStore = sellValNspl12mhStore;
	}
	public Long getSellValNspl1m() {
		return sellValNspl1m;
	}
	public void setSellValNspl1m(Long sellValNspl1m) {
		this.sellValNspl1m = sellValNspl1m;
	}
	public Long getSellValNspl24m() {
		return sellValNspl24m;
	}
	public void setSellValNspl24m(Long sellValNspl24m) {
		this.sellValNspl24m = sellValNspl24m;
	}
	public Long getSellValNspl3m() {
		return sellValNspl3m;
	}
	public void setSellValNspl3m(Long sellValNspl3m) {
		this.sellValNspl3m = sellValNspl3m;
	}
	public Long getSellValNspl6m() {
		return sellValNspl6m;
	}
	public void setSellValNspl6m(Long sellValNspl6m) {
		this.sellValNspl6m = sellValNspl6m;
	}
	public Long getSellValNsplyl3m() {
		return sellValNsplyl3m;
	}
	public void setSellValNsplyl3m(Long sellValNsplyl3m) {
		this.sellValNsplyl3m = sellValNsplyl3m;
	}
	public Long getSellValNsplytm() {
		return sellValNsplytm;
	}
	public void setSellValNsplytm(Long sellValNsplytm) {
		this.sellValNsplytm = sellValNsplytm;
	}
	public Long getSellValNspnfl12m() {
		return sellValNspnfl12m;
	}
	public void setSellValNspnfl12m(Long sellValNspnfl12m) {
		this.sellValNspnfl12m = sellValNspnfl12m;
	}
	public Long getSellValNspTrendl6m6m() {
		return sellValNspTrendl6m6m;
	}
	public void setSellValNspTrendl6m6m(Long sellValNspTrendl6m6m) {
		this.sellValNspTrendl6m6m = sellValNspTrendl6m6m;
	}
	public Long getSellValNspytm() {
		return sellValNspytm;
	}
	public void setSellValNspytm(Long sellValNspytm) {
		this.sellValNspytm = sellValNspytm;
	}
	public Long getTimezoneCd() {
		return timezoneCd;
	}
	public void setTimezoneCd(Long timezoneCd) {
		this.timezoneCd = timezoneCd;
	}
	public Long getTopDevelopClassCd() {
		return topDevelopClassCd;
	}
	public void setTopDevelopClassCd(Long topDevelopClassCd) {
		this.topDevelopClassCd = topDevelopClassCd;
	}
	public Long getTrendSalesl12m12mBack() {
		return trendSalesl12m12mBack;
	}
	public void setTrendSalesl12m12mBack(Long trendSalesl12m12mBack) {
		this.trendSalesl12m12mBack = trendSalesl12m12mBack;
	}
	public Long getTrendSalesylyBack() {
		return trendSalesylyBack;
	}
	public void setTrendSalesylyBack(Long trendSalesylyBack) {
		this.trendSalesylyBack = trendSalesylyBack;
	}
	public Long getValDelayedTransaction() {
		return valDelayedTransaction;
	}
	public void setValDelayedTransaction(Long valDelayedTransaction) {
		this.valDelayedTransaction = valDelayedTransaction;
	}
	public String getRequestedPaymentTerms() {
		return requestedPaymentTerms;
	}
	public void setRequestedPaymentTerms(String requestedPaymentTerms) {
		this.requestedPaymentTerms = requestedPaymentTerms;
	}
	public List<DynamicIndicator> getDynamicIndicatorList() {
		return dynamicIndicatorList;
	}
	public void setDynamicIndicatorList(List<DynamicIndicator> dynamicIndicatorList) {
		this.dynamicIndicatorList = dynamicIndicatorList;
	}
}
