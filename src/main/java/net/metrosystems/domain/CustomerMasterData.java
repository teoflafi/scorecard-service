package net.metrosystems.domain;

import java.util.Date;


public class CustomerMasterData {

	private Long id;
	private String customerNumber;
	private Date birthday;
	private String firstName;
	private String salutation;
	private String countryCode;
	private String companyName;
	private Date companyFoundationDate;
	private String companyOwnerLastName;
	private String companyOwnerFirstName;
	private String street;
	private String houseNumber;
	private String zipCode;
	private String city;
	private Integer checkoutCheck;
	private Integer blockingReason;
	private String accountHolder;
	private String bankAccountNumber;
	private String bankId;
	private String bankName;
	private String bankRoutingCode;
	private String bankRoutingSupplement;
	private String iban;
	private Boolean lastRequestInProgress;
	private Date lastRequestDate;
	private Boolean tempLimitDeleted;
	private Date emailSendDate;
	private String emailSendTo;
	private String profitCenter;
	private String vatNumber;
	private String vatEuNumber;
	private String storeNumber;
	private String branchId;
	private Date registrationDate;
	private String commentForCustomer;
	private String grossProfitMargin;
	private String legalForm;
	private String typeCode;
	private Long companyId;
	private Boolean isDeleted;
	private Boolean isRequestor;
	private Boolean isMaster;
	private Long currentLimit;
	private Integer metroClubTypeId;
	private Boolean isGroupingApproved;
	private String lastName;
	private String mobilePhoneNumber;
	private Integer numberOfCards;
	private String parentStoreKey;
	private String parentCustomerNumber;
	private String phoneNumber;
	private String titleName;
	private Integer branchFamilyId;
	private String customerType;
	private String securityIndicator;
	private String regionNumber;
	private Integer monitionlevel;
	private Integer badDebits;
	private Integer debitEntriesNumber;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String attribute4;
	private String attribute5;
	private Integer invoiceType;
	private Integer summaryInvoiceFreqCd;
	private Double disposableLimit;
	private Integer requestCountL3M;
	private Integer customerRelation;
	private Integer customerAge;
	private Integer ageOfCompany;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Date getCompanyFoundationDate() {
		return companyFoundationDate;
	}
	public void setCompanyFoundationDate(Date companyFoundationDate) {
		this.companyFoundationDate = companyFoundationDate;
	}
	public String getCompanyOwnerLastName() {
		return companyOwnerLastName;
	}
	public void setCompanyOwnerLastName(String companyOwnerLastName) {
		this.companyOwnerLastName = companyOwnerLastName;
	}
	public String getCompanyOwnerFirstName() {
		return companyOwnerFirstName;
	}
	public void setCompanyOwnerFirstName(String companyOwnerFirstName) {
		this.companyOwnerFirstName = companyOwnerFirstName;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getCheckoutCheck() {
		return checkoutCheck;
	}
	public void setCheckoutCheck(Integer checkoutCheck) {
		this.checkoutCheck = checkoutCheck;
	}
	public Integer getBlockingReason() {
		return blockingReason;
	}
	public void setBlockingReason(Integer blockingReason) {
		this.blockingReason = blockingReason;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankRoutingCode() {
		return bankRoutingCode;
	}
	public void setBankRoutingCode(String bankRoutingCode) {
		this.bankRoutingCode = bankRoutingCode;
	}
	public String getBankRoutingSupplement() {
		return bankRoutingSupplement;
	}
	public void setBankRoutingSupplement(String bankRoutingSupplement) {
		this.bankRoutingSupplement = bankRoutingSupplement;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public Boolean getLastRequestInProgress() {
		return lastRequestInProgress;
	}
	public void setLastRequestInProgress(Boolean lastRequestInProgress) {
		this.lastRequestInProgress = lastRequestInProgress;
	}
	public Date getLastRequestDate() {
		return lastRequestDate;
	}
	public void setLastRequestDate(Date lastRequestDate) {
		this.lastRequestDate = lastRequestDate;
	}
	public Boolean getTempLimitDeleted() {
		return tempLimitDeleted;
	}
	public void setTempLimitDeleted(Boolean tempLimitDeleted) {
		this.tempLimitDeleted = tempLimitDeleted;
	}
	public Date getEmailSendDate() {
		return emailSendDate;
	}
	public void setEmailSendDate(Date emailSendDate) {
		this.emailSendDate = emailSendDate;
	}
	public String getEmailSendTo() {
		return emailSendTo;
	}
	public void setEmailSendTo(String emailSendTo) {
		this.emailSendTo = emailSendTo;
	}
	public String getProfitCenter() {
		return profitCenter;
	}
	public void setProfitCenter(String profitCenter) {
		this.profitCenter = profitCenter;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getVatEuNumber() {
		return vatEuNumber;
	}
	public void setVatEuNumber(String vatEuNumber) {
		this.vatEuNumber = vatEuNumber;
	}
	public String getStoreNumber() {
		return storeNumber;
	}
	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getCommentForCustomer() {
		return commentForCustomer;
	}
	public void setCommentForCustomer(String commentForCustomer) {
		this.commentForCustomer = commentForCustomer;
	}
	public String getGrossProfitMargin() {
		return grossProfitMargin;
	}
	public void setGrossProfitMargin(String grossProfitMargin) {
		this.grossProfitMargin = grossProfitMargin;
	}
	public String getLegalForm() {
		return legalForm;
	}
	public void setLegalForm(String legalForm) {
		this.legalForm = legalForm;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Boolean getIsRequestor() {
		return isRequestor;
	}
	public void setIsRequestor(Boolean isRequestor) {
		this.isRequestor = isRequestor;
	}
	public Boolean getIsMaster() {
		return isMaster;
	}
	public void setIsMaster(Boolean isMaster) {
		this.isMaster = isMaster;
	}
	public Long getCurrentLimit() {
		return currentLimit;
	}
	public void setCurrentLimit(Long currentLimit) {
		this.currentLimit = currentLimit;
	}
	public Integer getMetroClubTypeId() {
		return metroClubTypeId;
	}
	public void setMetroClubTypeId(Integer metroClubTypeId) {
		this.metroClubTypeId = metroClubTypeId;
	}
	public Boolean getIsGroupingApproved() {
		return isGroupingApproved;
	}
	public void setIsGroupingApproved(Boolean isGroupingApproved) {
		this.isGroupingApproved = isGroupingApproved;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	public Integer getNumberOfCards() {
		return numberOfCards;
	}
	public void setNumberOfCards(Integer numberOfCards) {
		this.numberOfCards = numberOfCards;
	}
	public String getParentStoreKey() {
		return parentStoreKey;
	}
	public void setParentStoreKey(String parentStoreKey) {
		this.parentStoreKey = parentStoreKey;
	}
	public String getParentCustomerNumber() {
		return parentCustomerNumber;
	}
	public void setParentCustomerNumber(String parentCustomerNumber) {
		this.parentCustomerNumber = parentCustomerNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public Integer getBranchFamilyId() {
		return branchFamilyId;
	}
	public void setBranchFamilyId(Integer branchFamilyId) {
		this.branchFamilyId = branchFamilyId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getSecurityIndicator() {
		return securityIndicator;
	}
	public void setSecurityIndicator(String securityIndicator) {
		this.securityIndicator = securityIndicator;
	}
	public String getRegionNumber() {
		return regionNumber;
	}
	public void setRegionNumber(String regionNumber) {
		this.regionNumber = regionNumber;
	}
	public Integer getMonitionlevel() {
		return monitionlevel;
	}
	public void setMonitionlevel(Integer monitionlevel) {
		this.monitionlevel = monitionlevel;
	}
	public Integer getBadDebits() {
		return badDebits;
	}
	public void setBadDebits(Integer badDebits) {
		this.badDebits = badDebits;
	}
	public Integer getDebitEntriesNumber() {
		return debitEntriesNumber;
	}
	public void setDebitEntriesNumber(Integer debitEntriesNumber) {
		this.debitEntriesNumber = debitEntriesNumber;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	public String getAttribute2() {
		return attribute2;
	}
	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}
	public String getAttribute3() {
		return attribute3;
	}
	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}
	public String getAttribute4() {
		return attribute4;
	}
	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}
	public String getAttribute5() {
		return attribute5;
	}
	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}
	public Integer getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(Integer invoiceType) {
		this.invoiceType = invoiceType;
	}
	public Integer getSummaryInvoiceFreqCd() {
		return summaryInvoiceFreqCd;
	}
	public void setSummaryInvoiceFreqCd(Integer summaryInvoiceFreqCd) {
		this.summaryInvoiceFreqCd = summaryInvoiceFreqCd;
	}
	public Double getDisposableLimit() {
		return disposableLimit;
	}
	public void setDisposableLimit(Double disposableLimit) {
		this.disposableLimit = disposableLimit;
	}
	public Integer getRequestCountL3M() {
		return requestCountL3M;
	}
	public void setRequestCountL3M(Integer requestCountL3M) {
		this.requestCountL3M = requestCountL3M;
	}
	public Integer getCustomerRelation() {
		return customerRelation;
	}
	public void setCustomerRelation(Integer customerRelation) {
		this.customerRelation = customerRelation;
	}
	public Integer getCustomerAge() {
		return customerAge;
	}
	public void setCustomerAge(Integer customerAge) {
		this.customerAge = customerAge;
	}
	public Integer getAgeOfCompany() {
		return ageOfCompany;
	}
	public void setAgeOfCompany(Integer ageOfCompany) {
		this.ageOfCompany = ageOfCompany;
	}
	
	
}