package net.metrosystems.domain;

import java.util.Date;

public class ExternalScore {

	private Long id;
	private String externalCustomerID;
	private String score;
	private Long infoBureauID;
	private String infoBureauName;
	private String maxLimit;
	private Date requestedAt;
	private String subject;

	public Long getId() {
		return id;
	}

	public String getExternalCustomerID() {
		return externalCustomerID;
	}

	public String getScore() {
		return score;
	}

	public Long getInfoBureauID() {
		return infoBureauID;
	}

	public String getInfoBureauName() {
		return infoBureauName;
	}

	public String getMaxLimit() {
		return maxLimit;
	}

	public Date getRequestedAt() {
		return requestedAt;
	}

	public String getSubject() {
		return subject;
	}

}
