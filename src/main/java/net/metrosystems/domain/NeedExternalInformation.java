package net.metrosystems.domain;

public class NeedExternalInformation {

	private Integer informationBureauId;
	private Integer fallBackinformationBureauId;

	public Integer getinformationBureauId() {
		return informationBureauId;
	}

	public void setinformationBureauId(Integer informationBureauId) {
		this.informationBureauId = informationBureauId;
	}

	public Integer getFallBackinformationBureauId() {
		return fallBackinformationBureauId;
	}

	public void setFallBackinformationBureauId(Integer fallBackinformationBureauId) {
		this.fallBackinformationBureauId = fallBackinformationBureauId;
	}

}
