package net.metrosystems.domain;

public class Payment {

	private Long id = null;
	private String typeName = null;
	private String termName = null;
	private String debitType = null;
	private String description = null;
	private Integer paymentAllowanceCd = null;
	private Integer creditSettleTypeCd = null;
	private Integer creditSettlePeriodCd = null;
	private Integer creditSettleFrequencyCd = null;

	public Long getId() {
		return id;
	}

	public String getTypeName() {
		return typeName;
	}

	public String getTermName() {
		return termName;
	}

	public String getDebitType() {
		return debitType;
	}

	public String getDescription() {
		return description;
	}

	public Integer getPaymentAllowanceCd() {
		return paymentAllowanceCd;
	}

	public Integer getCreditSettleTypeCd() {
		return creditSettleTypeCd;
	}

	public Integer getCreditSettlePeriodCd() {
		return creditSettlePeriodCd;
	}

	public Integer getCreditSettleFrequencyCd() {
		return creditSettleFrequencyCd;
	}

}
