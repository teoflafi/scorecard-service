package net.metrosystems.domain;

public enum ProcessTypeEnum {
	
	LIMIT_CHECK_REQUEST_PROCESS(1),
	QUICK_CHECK_REQUEST_PROCESS(2),
	SEASONAL_LIMIT_REQUEST_PROCESS(3),
	INITIAL_CHECK_REQUEST_PROCESS(4);
//	CONI_CHECK_REQUEST_PROCESS(5),
//	ONE_TIME_CREDIT_REQUEST_PROCESS (6);
	private int value;

	private ProcessTypeEnum(int value) {
		this.value = value;
	}
	public int getValue() {
		return value;
	}
}
