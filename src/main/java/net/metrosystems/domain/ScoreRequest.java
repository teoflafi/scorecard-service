package net.metrosystems.domain;

import java.util.List;

public class ScoreRequest {

	private CreditRequest creditRequest;
	private Payment payment;
	private CustomerMasterData customerMasterData;
	private CustomerCreditData customerCreditData;
	private Boolean requestHasAttachment;
	private Boolean requestHasComment;
	private Boolean jobMode;
	private List<ExternalScore> externalScores;
	private ProcessTypeEnum processType;

	public CreditRequest getCreditRequest() {
		return creditRequest;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setCreditRequest(CreditRequest creditRequest) {
		this.creditRequest = creditRequest;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public void setCustomerMasterData(CustomerMasterData customerMasterData) {
		this.customerMasterData = customerMasterData;
	}

	public void setCustomerCreditData(CustomerCreditData customerCreditData) {
		this.customerCreditData = customerCreditData;
	}

	public void setRequestHasAttachment(Boolean requestHasAttachment) {
		this.requestHasAttachment = requestHasAttachment;
	}

	public void setRequestHasComment(Boolean requestHasComment) {
		this.requestHasComment = requestHasComment;
	}

	public void setJobMode(Boolean jobMode) {
		this.jobMode = jobMode;
	}

	public void setExternalScores(List<ExternalScore> externalScores) {
		this.externalScores = externalScores;
	}

	public void setProcessType(ProcessTypeEnum processType) {
		this.processType = processType;
	}

	public CustomerMasterData getCustomerMasterData() {
		return customerMasterData;
	}

	public CustomerCreditData getCustomerCreditData() {
		return customerCreditData;
	}

	public List<ExternalScore> getExternalScores() {
		return externalScores;
	}

	public Boolean getRequestHasAttachment() {
		return requestHasAttachment;
	}

	public Boolean getRequestHasComment() {
		return requestHasComment;
	}

	public ProcessTypeEnum getProcessType() {
		return processType;
	}

	public Boolean getJobMode() {
		return jobMode;
	}

}
