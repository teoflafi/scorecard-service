package net.metrosystems.domain;

import java.util.List;

public class ScoreResult {

	private List<String> missingFields;
	private List<NeedExternalInformation> neededExternalInformation;
	private Integer score;
	private Integer trafficLightID;
	private Integer rejectReasonID;
	private Integer checkoutCheckCD;
	private Integer calculatedLimit;
	private Integer modifiedScore;
	private Integer originalScore;
	
	public List<String> getMissingFields() {
		return missingFields;
	}

	public void setMissingFields(List<String> missingFields) {
		this.missingFields = missingFields;
	}

	public List<NeedExternalInformation> getNeededExternalInformation() {
		return neededExternalInformation;
	}

	public void setNeededExternalInformation(List<NeedExternalInformation> neededExternalInformation) {
		this.neededExternalInformation = neededExternalInformation;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getTrafficLightID() {
		return trafficLightID;
	}

	public void setTrafficLightID(Integer trafficLightID) {
		this.trafficLightID = trafficLightID;
	}

	public Integer getRejectReasonID() {
		return rejectReasonID;
	}

	public void setRejectReasonID(Integer rejectReasonID) {
		this.rejectReasonID = rejectReasonID;
	}

	public Integer getCheckoutCheckCD() {
		return checkoutCheckCD;
	}

	public void setCheckoutCheckCD(Integer checkoutCheckCD) {
		this.checkoutCheckCD = checkoutCheckCD;
	}

	public Integer getCalculatedLimit() {
		return calculatedLimit;
	}

	public void setCalculatedLimit(Integer calculatedLimit) {
		this.calculatedLimit = calculatedLimit;
	}

	public Integer getModifiedScore() {
		return modifiedScore;
	}

	public void setModifiedScore(Integer modifiedScore) {
		this.modifiedScore = modifiedScore;
	}

	public Integer getOriginalScore() {
		return originalScore;
	}

	public void setOriginalScore(Integer originalScore) {
		this.originalScore = originalScore;
	}

}
