package net.metrosystems.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import net.metrosystems.score.BranchGroup;
import net.metrosystems.score.ScoreRuleOperation;

@Entity
@Table(name="TEST")
public class ScoreRule {

	@Id
	private Long id;
	private Long companyId;
	@Enumerated(EnumType.STRING)
	private BranchGroup branchGroup;
	private String attribute;
	@Enumerated(EnumType.STRING)
	private ScoreRuleOperation operation;
	private String value;
	private Long groupId;
	private Integer dynamicIndicatorKey;
	private Integer score;

	public Long getId() {
		return id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public BranchGroup getBranchGroup() {
		return branchGroup;
	}

	public String getAttribute() {
		return attribute;
	}

	public ScoreRuleOperation getOperation() {
		return operation;
	}

	public String getValue() {
		return value;
	}

	public Long getGroupId() {
		return groupId;
	}

	public Integer getDynamicIndicatorKey() {
		return dynamicIndicatorKey;
	}
	
	public Integer getScore() {
		return score;
	}

}
