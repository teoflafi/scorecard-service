package net.metrosystems.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import net.metrosystems.score.BranchGroup;

public interface ScoreRuleRepository extends CrudRepository<ScoreRule, Long> {
	public List<ScoreRule> findByGroupIdAndCompanyId(Long groupId, Long companyId);
	public List<ScoreRule> findByCompanyIdAndBranchGroup(Long companyId, BranchGroup branchGroup);
}
