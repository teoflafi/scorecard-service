package net.metrosystems.score;

public enum BranchGroup {
	HORECA,
	TRADER,
	SCO,
	NEW_CUSTOMER
}
