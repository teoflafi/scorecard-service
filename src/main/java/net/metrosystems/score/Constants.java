package net.metrosystems.score;

public class Constants {
	
	//Reject reasons
	public static final int REJECT_KO_SCORECARD = 1;
	public static final int REJECT_KO_AGE_UNDER_18 = 2;
	public static final int REJECT_KO_DUNNING_LEVEL_TOO_HIGH = 3;
	public static final int REJECT_KO_BLOCK_IN_CFM = 4;
	public static final int REJECT_KO_TOO_MANY_BAD_DEBTS = 5;
	
	public static final int REJECT_SEMI_KO_SCORECARD = 100;
	public static final int REJECT_SEMI_KO_DUNNING_LEVEL_TOO_HIGH = 101 ;
	public static final int REJECT_SEMI_KO_DEBIT_ENTRIES_TOO_HIGH = 102 ;
	public static final int REJECT_SEMI_KO_GROSS_PROFIT_MARGIN_TOO_LOW = 103 ;
	public static final int REJECT_SEMI_KO_ATTACHMENTS_PRESENT = 104;
	public static final int REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M = 105;
	public static final int REJECT_SEMI_KO_COMMENTS_PRESENT = 106;
	public static final int REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH = 107;
	public static final int REJECT_SEMI_KO_PAYMENT_METHOD_QUESTIONABLE = 108;
	public static final int REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH = 109;
	public static final int REJECT_SEMI_KO_TOO_MANY_BAD_DEBTS = 110 ;

	public static final int REJECT_EXTERNAL_KO_CEG = 201 ;
	public static final int REJECT_EXTERNAL_KO_SCHUFA_B2B = 202 ;
	public static final int REJECT_EXTERNAL_KO_SCHUFA_B2C = 203 ;
	public static final int REJECT_EXTERNAL_KO_CREDITREFORM_PL = 204;
	public static final int REJECT_EXTERNAL_KO_KRD = 205;
	public static final int REJECT_EXTERNAL_KO_CREFO_BG = 206;
	public static final int REJECT_EXTERNAL_KO_COFACE_HU = 207 ;
	
	public static final int REJECT_EXTERNAL_SEMI_KO_CEG = 301 ;
	public static final int REJECT_EXTERNAL_SEMI_KO_SCHUFA_B2B = 302 ;
	public static final int REJECT_EXTERNAL_SEMI_KO_SCHUFA_B2C = 303 ;
	public static final int REJECT_EXTERNAL_SEMI_KO_CREDITREFORM_PL = 304;
	public static final int REJECT_EXTERNAL_SEMI_KO_CREFO_BG = 305;
	public static final int REJECT_EXTERNAL_SEMI_KO_COFACE_HU = 306 ;


	public static final int REJECT_STRATEGY_EMPLOYEE = 400;
	public static final int REJECT_STRATEGY_REQUEST_INTEVRAL_TOO_SHORT = 401;
	public static final int REJECT_STRATEGY_MANDATORY_FIELDS_MISSING = 402;
	public static final int REJECT_STRATEGY_NEED_EXTERNAL_INFO = 403;
	public static final int REJECT_STRATEGY_CUSTOMER_INACTIVE_IN_CFM = 405;
	public static final int REJECT_STRATEGY_NEW_CUSTOMER_3M = 413 ;
	public static final int REJECT_STRATEGY_SKO_PAY_CASH_ONLY = 415;
	public static final int REJECT_STRATEGY_SKO_DELETED = 416;
	public static final int REJECT_STRATEGY_SKO_ACTIVE_CUSTOMER = 417;
	public static final int REJECT_STRATEGY_KO_PAY_CASH_ONLY = 418;
	public static final int REJECT_STRATEGY_KO_DELETED = 419;
	public static final int REJECT_STRATEGY_KO_ACTIVE_CUSTOMER = 420;
	public static final int REJECT_STRATEGY_PUBLIC_COMPANY = 423;
	public static final int REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_KO = 424;
	public static final int REJECT_STRATEGY_CREDIT_BLOCKING_FOR_KO = 425;
	public static final int REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_CANCEL = 426;
	public static final int REJECT_STRATEGY_RISK_CATEG_KO = 427;
	public static final int REJECT_STRATEGY_RISK_CATEG_SKO = 428;
	public static final int REJECT_STRATEGY_CREDIT_BLOCKING_FOR_SKO = 429;
	public static final int REJECT_STRATEGY_AFFILIATED_COMPANY = 430;
	public static final int REJECT_STRATEGY_SKO_TURNOVER_MISSING = 436;
	public static final int REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_SKO= 437;
	
	public static final int REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN = 900;
	                                                                       
	//Traffic light
	public static final int TRAFFIC_LIGHT_GREEN = 3;             
	public static final int TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT = 4;
	public static final int TRAFFIC_LIGHT_RED = 1;                
	public static final int TRAFFIC_LIGHT_YELLOW = 2;
	
	//Field names
	public static final String FIELD_COMPANY_NAME = "CompanyName";
	public static final String FIELD_LEGAL_FORM_CODE = "LegalFormCD";
	public static final String FIELD_COMPANY_FOUNDATION_DATE = "CompanyFoundationDate";
	public static final String FIELD_REGISTRATION_DATE = "RegistrationDate";
	public static final String FIELD_STREET = "Street";
	public static final String FIELD_CITY = "Town";
	public static final String FIELD_HOUSE_NO = "HouseNo";
	public static final String FIELD_ZIP_CODE = "ZipCode";
	public static final String FIELD_VAT_SPEC_NO = "vatSpecNo";
	public static final String FIELD_OWNER_LAST_NAME = "OwnerLastName";
	public static final String FIELD_OWNER_FIRST_NAME = "OwnerFirstName";
	public static final String FIELD_BIRTH_DATE = "BirthDate";
	public static final String FIELD_BRANGH_ID = "BranchId";
	
	//Other
//	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	public static final int EXTERNAL_BUREAU_KRD = 6;
	public static final int EXTERNAL_BUREAU_CREDITREFORM = 7;
	public static final int EXTERNAL_BUREAU_CREFO_BG = 9;
}
