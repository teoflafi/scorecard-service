package net.metrosystems.score;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.metrosystems.domain.CreditRequest;
import net.metrosystems.domain.CustomerCreditData;
import net.metrosystems.domain.CustomerMasterData;
import net.metrosystems.domain.ExternalScore;
import net.metrosystems.domain.Payment;
import net.metrosystems.domain.ProcessTypeEnum;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public abstract class ScoreCalculator {

	private ScoreRuleRepository scoreRuleRepository;
	private CreditRequest creditRequest;
	private Payment payment;
	private CustomerMasterData customerMasterData;
	private CustomerCreditData customerCreditData;
	private boolean requestHasAttachment;
	private boolean requestHasComment;
	private boolean jobMode;
	private List<ExternalScore> externalScores;
	private Map<String, Object> customVariables;
	private ProcessTypeEnum processType; 
	
	public ScoreCalculator(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		
		this.scoreRuleRepository = scoreRuleRepository; 
		this.creditRequest = scoreRequest.getCreditRequest();
		this.payment = scoreRequest.getPayment();
		this.customerMasterData = scoreRequest.getCustomerMasterData();
		this.customerCreditData = scoreRequest.getCustomerCreditData();
		this.requestHasAttachment = scoreRequest.getRequestHasAttachment();
		this.requestHasComment = scoreRequest.getRequestHasComment();
		this.jobMode=scoreRequest.getJobMode();
		this.externalScores = scoreRequest.getExternalScores();
		
		
		if (scoreRequest.getProcessType() == null) {
			this.processType = ProcessTypeEnum.LIMIT_CHECK_REQUEST_PROCESS;
		} else {
			this.processType = scoreRequest.getProcessType();
		}
		
		this.customVariables = new HashMap<String, Object>();
	}

	/**
	 * Calculates a score depending on the country and sales line implementation
	 * 
	 * @return
	 */
	public abstract ScoreResult calculateScore();
	
	/**
	 * Extracts the missing fields which are mandatory for the scoring process
	 * 
	 * @return
	 */
	public abstract List<String> extractMissingFields();
	
	
	//============================================== Getters ===================================================
	
	public CreditRequest getCreditRequest() {
		return creditRequest;
	}

	public Payment getPayment() {
		return payment;
	}

	public CustomerMasterData getCustomerMasterData() {
		return customerMasterData;
	}

	public CustomerCreditData getCustomerCreditData() {
		return customerCreditData;
	}

	public List<ExternalScore> getExternalScores() {
		return externalScores;
	}

	public ScoreRuleRepository getScoreRuleRepository() {
		return scoreRuleRepository;
	}
	
	public Map<String, Object> getCustomVariables() {
		return customVariables;
	}

	public boolean getRequestHasAttachment() {
		return requestHasAttachment;
	}

	public boolean getRequestHasComment() {
		return requestHasComment;
	}

	public ProcessTypeEnum getProcessType() {
		return processType;
	}

	public boolean isJobMode() {
		return jobMode;
	}
	

}
