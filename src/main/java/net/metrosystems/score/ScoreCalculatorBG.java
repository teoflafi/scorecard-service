package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.metrosystems.domain.ExternalScore;
import net.metrosystems.domain.Payment;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorBG extends ScoreCalculator {

	private boolean isOneManBusiness = false;
	private BranchGroup branchGroup;
	
	private final String BEFORE_SIX_MONTH_SALES = "beforeSixMonthSales";
	private final String PERC_FOOD = "percFood";
	private final String PERC_NON_FOOD = "percNonFood";
	private Integer branchMainGroupID;
	private Integer assortSectionID;
	
	private final String BRANCH_MAIN_GROUP_ID = "branchMainGroupID";
	private final String ASSORT_SECTION_ID = "assortSectionID";
	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays.asList(new Integer[] {18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 38, 100, 40, 110, 41, 120, 42, 
			130, 43, 44, 45, 46, 47, 140, 48, 150, 49, 160, 74, 170, 75, 190, 29}); 
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] {60, 300, 61, 310, 62, 370, 63, 330, 64, 340, 65, 350, 39, 67, 68, 69});
	private final List<Integer> BRANCH_FAMILIY_SCO = Arrays.asList(new Integer[] {66, 360, 83, 700, 84, 710, 85, 720, 88, 730, 740, 750, 86, 87, 55, 500, 56, 
			510, 57, 520, 58, 530, 999});
	
	public ScoreCalculatorBG(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);
	}

	@Override
	public ScoreResult calculateScore() {
		
		ScoreResult result = new ScoreResult();
		
		//Set checkout check code
		result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());
		
		//Check employee branch code	
		if (Arrays.asList(new String[] {"900", "901", "910", "911", "930", "970"}).contains(getCustomerMasterData().getBranchId())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_EMPLOYEE);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 
		}
		
		//Check one man business
		if (getCustomerMasterData().getLegalForm() != null && getCustomerMasterData().getLegalForm().equals("1")) {
			isOneManBusiness = true;
		}
		
		//Check mandatory fields
		//If one or more 	 fields are missing reject the scoring
		List<String> missingFields = extractMissingFields();
		if (missingFields.size() > 0) {
			result.setMissingFields(missingFields);
			result.setRejectReasonID(Constants.REJECT_STRATEGY_MANDATORY_FIELDS_MISSING);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 			
		}
		
		//Check customer age
		if (isOneManBusiness && (Utils.numberOfYearsSinceDate(getCustomerMasterData().getBirthday()) < 18)) {
			result.setRejectReasonID(Constants.REJECT_KO_AGE_UNDER_18);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 
		}
		
		//Check risk category KO conditions
		if (Arrays.asList(new String[]{"MC1", "MC2", "MCR", "MVE"}).contains(getCustomerMasterData().getSecurityIndicator())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_RISK_CATEG_KO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 			
		}
		
		//Check cancel blocking code
		if (Arrays.asList(new Integer[]{62, 66}).contains(getCustomerMasterData().getBlockingReason())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_CANCEL);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 			
		}
		
		//Check KO blocking code
		if (Arrays.asList(new Integer[]{3, 7, 9, 60, 99}).contains(getCustomerMasterData().getBlockingReason())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_KO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setCheckoutCheckCD(30);
			return result; 			
		}		
		
		//Check credit blocking code for KO
		if (getCustomerMasterData().getCheckoutCheck() != null && getCustomerMasterData().getCheckoutCheck() == (Integer) 35) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CREDIT_BLOCKING_FOR_KO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 	
		}		
		
		//Check affiliated company
		if (Arrays.asList(new String[]{"898", "902", "912", "935", "940", "950"}).contains(getCustomerMasterData().getBranchId())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_AFFILIATED_COMPANY);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result; 			
		}
		
		//Set ScoreCard branch group
//		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchGroup = BranchGroup.HORECA;
//		} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchGroup = BranchGroup.TRADER;
//		} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchGroup = BranchGroup.SCO;
//		} else {
//			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
//			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
//			return result;
//		}
		
		branchGroup = setBranchFamily();
		if (branchGroup == null) {
			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		
		if (Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()) < 12) {
			branchGroup = BranchGroup.NEW_CUSTOMER;
		}
		
		//Food and non food percentage calculation
		if (getCustomerCreditData().getSellValNspl12m() != null && getCustomerCreditData().getSellValNspfl12m() != null 
				&& getCustomerCreditData().getSellValNspnfl12m() != null) {
			
			BigDecimal totalSales = new BigDecimal(0);
			BigDecimal totalFoodSales = new BigDecimal(0);
			BigDecimal totalNonFoodSales = new BigDecimal(0);
			
			totalSales = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			totalFoodSales = new BigDecimal(getCustomerCreditData().getSellValNspfl12m());
			totalNonFoodSales = new BigDecimal(getCustomerCreditData().getSellValNspnfl12m());		
			
			Double percFood = new Double(0);
			Double percNonFood = new Double(0);
			if (!totalSales.equals(new BigDecimal(0))) {
				percFood = totalFoodSales.divide(totalSales, 2, RoundingMode.HALF_UP).doubleValue() * 100;
				percNonFood = totalNonFoodSales.divide(totalSales, 2, RoundingMode.HALF_UP).doubleValue() * 100;	
			}
			
			getCustomVariables().put(PERC_FOOD, percFood);
			getCustomVariables().put(PERC_NON_FOOD, percNonFood);
		}
		
		//Calculate before six months sales
		if (getCustomerCreditData().getSellValNspl12m() != null && getCustomerCreditData().getSellValNspl6m() != null) {
			Long beforeSixMonthSales = getCustomerCreditData().getSellValNspl12m() - getCustomerCreditData().getSellValNspl6m();
			getCustomVariables().put(BEFORE_SIX_MONTH_SALES, beforeSixMonthSales);
		}
		
		//Set branch main group
		if (Arrays.asList(new Integer[] {20, 22, 23, 24, 25, 100, 29}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 10;
		} else if (Arrays.asList(new Integer[] {40, 110}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 11;
		} else if (Arrays.asList(new Integer[] {41, 120}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 12;
		} else if (Arrays.asList(new Integer[] {42, 130}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 13;
		} else if (Arrays.asList(new Integer[] {43, 140}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 14;
		} else if (Arrays.asList(new Integer[] {48, 150}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 15;
		} else if (Arrays.asList(new Integer[] {49, 160}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 16;
		} else if (Arrays.asList(new Integer[] {74, 170}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 17;
		} else if (Arrays.asList(new Integer[] {75, 190}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 19;
		} else if (Arrays.asList(new Integer[] {60, 300}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 30;
		} else if (Arrays.asList(new Integer[] {61, 310}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 31;
		} else if (Arrays.asList(new Integer[] {62, 370}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 32;
		} else if (Arrays.asList(new Integer[] {63, 330}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 33;
		} else if (Arrays.asList(new Integer[] {64, 340}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 34;
		} else if (Arrays.asList(new Integer[] {65, 350}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 35;
		} else if (Arrays.asList(new Integer[] {66, 360}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 36;
		} else if (Arrays.asList(new Integer[] {55, 500}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 50;
		} else if (Arrays.asList(new Integer[] {56, 510}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 51;
		} else if (Arrays.asList(new Integer[] {57, 520}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 52;
		} else if (Arrays.asList(new Integer[] {58, 530}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 53;
		} else if (Arrays.asList(new Integer[] {83, 700}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 70;
		} else if (Arrays.asList(new Integer[] {84, 710}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 71;
		} else if (Arrays.asList(new Integer[] {85, 720}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 72;
		} else if (Arrays.asList(new Integer[] {88, 730, 740, 750}).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 78;
		} else if (getCustomerMasterData().getBranchFamilyId().equals((Integer) 86)) {
			branchMainGroupID = 81;
		} else if (getCustomerMasterData().getBranchFamilyId().equals((Integer) 87)) {
			branchMainGroupID = 82;
		} else if (getCustomerMasterData().getBranchFamilyId().equals((Integer) 999)) {
			branchMainGroupID = 99;
		} else {
			throw new IllegalStateException("branchMainGroup not found");
		}
		if (branchMainGroupID != null) {
			getCustomVariables().put(BRANCH_MAIN_GROUP_ID, branchMainGroupID);
		}
		
		//Set assort sectionId
		if (Arrays.asList(new Integer[] {10 , 11 , 12 , 13 , 14 , 15 , 16 , 17 , 19 , 29}).contains(branchMainGroupID)) {
			assortSectionID = 1;
		} else if (Arrays.asList(new Integer[] {30 , 31 , 32 , 33 , 34 , 35 , 37 , 38 , 39}).contains(branchMainGroupID)) {
			assortSectionID = 3;
		} else if (Arrays.asList(new Integer[] {50 , 51 , 52 , 53 , 58 , 59}).contains(branchMainGroupID)) {
			assortSectionID = 5;
		} else if (Arrays.asList(new Integer[] {30 , 31 , 32 , 33 , 34 , 35 , 37 , 38 , 39}).contains(branchMainGroupID)) {
			assortSectionID = 1;
		} else if (Arrays.asList(new Integer[] {36 , 70 , 71 , 72 , 78 , 81 , 82 , 89 }).contains(branchMainGroupID)) {
			assortSectionID = 7;
		} else if (Arrays.asList(new Integer[] {30 , 31 , 32 , 33 , 34 , 35 , 37 , 38 , 39}).contains(branchMainGroupID)) {
			assortSectionID = 1;
		}
		if (assortSectionID != null) {
			getCustomVariables().put(ASSORT_SECTION_ID, assortSectionID);
		}
		
		//Calculate score
		ScoreRuleEngine scoreRuleEngine = new ScoreRuleEngine(getScoreRuleRepository(), getCustomerMasterData(), 
				getCustomerCreditData(), getPayment(), branchGroup, getCustomVariables());
		result.setScore(scoreRuleEngine.calculateScore());
		
		int crefoBGScore = 0;
		//Check risk category SKO		
		if (Arrays.asList(new String[]{"MCC", "MCO", "MDP", "MG1", "MG1", "MG2", "MG3", "MGF", "MGL", "MPC"})
				.contains(getCustomerMasterData().getSecurityIndicator())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_RISK_CATEG_SKO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);	
			crefoBGScore = -2;
			return result; 			
		}
		
		BigDecimal recomendedLimit = new BigDecimal(0);
		boolean crefoBGFound = false;
		
		if (!isJobMode()) {
			//Check Crefo Information Available
			for (ExternalScore e : getExternalScores()) {
				if ((e.getInfoBureauID() == Constants.EXTERNAL_BUREAU_CREFO_BG) && Utils.numberOfMonthsSinceDate(e.getRequestedAt()) < 90) {
					crefoBGScore = Integer.parseInt(e.getScore());
					if (e.getMaxLimit() != null) {
						 recomendedLimit = new BigDecimal(e.getMaxLimit());
					} else {
						recomendedLimit = new BigDecimal(0);
					}
					crefoBGFound = true;
					break;
				} else {
					crefoBGFound = false;
				}
			}
			
			if (!crefoBGFound) {
				crefoBGScore = -1;
				if (getCreditRequest().getRequestedLimit() > 1000) {
					// Check Crefo Information Available
					// crefoBGScore=-1 means crefo sends blank score for company with foundation date less than 1 year
					result.setRejectReasonID(Constants.REJECT_STRATEGY_NEED_EXTERNAL_INFO);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				}
			}
			if (crefoBGScore == 0) {
				crefoBGScore = -1;
			}
		}
		
		BigDecimal paymentFactor = new BigDecimal(1);
		// Calculate Payment Factor
		if (new Payment().getCreditSettleFrequencyCd() == null) {
			if (Arrays.asList(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"})
					.contains(getCustomerCreditData().getRequestedPaymentTerms())) {
				paymentFactor = new BigDecimal(0.5);	
			} else if (Arrays.asList(new String[] {"16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", 
					"27", "28", "29", "30"}).contains(getCustomerCreditData().getRequestedPaymentTerms())) {
				paymentFactor = new BigDecimal(1); 
			} else if (Arrays.asList(new String[] {"31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", 
					"43", "44", "45"}).contains(getCustomerCreditData().getRequestedPaymentTerms())) {
				paymentFactor = new BigDecimal(1.5); 
			} else if (Arrays.asList(new String[] {"46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58",
					"59", "60"}).contains(getCustomerCreditData().getRequestedPaymentTerms())) {
				paymentFactor = new BigDecimal(2); 
			}
		} else if (new Payment().getCreditSettleFrequencyCd() == 1) {
			paymentFactor = new BigDecimal(1.5);  
		} else if (new Payment().getCreditSettleFrequencyCd() == 2) {
			paymentFactor = new BigDecimal(1);  
		} else {
			paymentFactor = new BigDecimal(1);  
		}
		
		long internalFactor = 0;
		long externalFactor = 0;
		long branchFactor = 0;
		//just for NewCustomer
		double baseCalculation4NewCustomer = 0;
		
		//if !isNewCustomer		
		if (branchGroup != BranchGroup.NEW_CUSTOMER) {
			//Turnover MissingExit
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_TURNOVER_MISSING);
				return result;
			} else {
				//Calculate Limit 4 OldCustomers
				//set internal Factor
				if (result.getScore() > 0  && result.getScore() <= 326) {
					internalFactor = (long) 1.5; 
				} else if (result.getScore() > 326 && result.getScore() <= 392) {
					internalFactor = (long) 1.25;
				} else if (result.getScore() > 392 && result.getScore() <= 493) {
					internalFactor = (long) 1;
				} else if (result.getScore() > 493) {
					internalFactor = (long) 0.5;
				} 
				
				//set external Factor
				if (crefoBGScore == -1) {
					externalFactor = (long) 0.5;
				} else if (crefoBGScore == -2) {
					externalFactor = (long) 1;
				} else if (crefoBGScore >= 100 && crefoBGScore <= 199) {
					externalFactor = (long) 2;
				} else if (crefoBGScore >= 200 && crefoBGScore <= 270) {
					externalFactor = (long) 1.5;
				} else if (crefoBGScore >= 271 && crefoBGScore <= 300) {
					externalFactor = (long) 1.25;
				} else if (crefoBGScore >= 301 && crefoBGScore <= 349) {
					externalFactor = (long) 1;
				} else if (crefoBGScore >= 350 && crefoBGScore <= 399) {
					externalFactor = (long) 0.75;
				} else if (crefoBGScore >= 400 && crefoBGScore <= 429) {
					externalFactor = (long) 0.25;
				} else if (crefoBGScore >= 430) {
					externalFactor = (long) 0;
				}
				
				//set branchFactor
				if (branchGroup == BranchGroup.HORECA) {
					branchFactor = (long) 1.5;
				} else if (branchGroup == BranchGroup.TRADER) {
					branchFactor = (long) 1.3;
				} else if (branchGroup == BranchGroup.SCO) {
					branchFactor = (long) 1.2;
				}
				
				// Calculate limit
				if (getCustomerCreditData().getSellValNspl12m() == null) {
					getCustomerCreditData().setSellValNspfl12m((long) 0);
				}
				double averageTurnover = 0;
				double double_sellValNspL12M = getCustomerCreditData().getSellValNspl12m();
				double tempLimit;
	
				averageTurnover = Math.round(double_sellValNspL12M / 12.00);
				tempLimit = averageTurnover * internalFactor * externalFactor * branchFactor * paymentFactor.doubleValue();
				Integer calculatedLimit = new Integer((int) tempLimit);
				
				if (calculatedLimit < 100) {
					calculatedLimit = 100;
				}
				result.setCalculatedLimit(calculatedLimit);
			}
		} else {
			//set internal Factor
			if (result.getScore() > 0  && result.getScore() <= 326) {
				internalFactor = (long) 1.5; 
			} else if (result.getScore() > 326 && result.getScore() <= 392) {
				internalFactor = (long) 1.25;
			} else if (result.getScore() > 392 && result.getScore() <= 493) {
				internalFactor = (long) 1;
			} else if (result.getScore() > 493) {
				internalFactor = (long) 0.5;
			} 
			
			//set external Factor
			if (crefoBGScore == -1) {
				externalFactor = (long) 0.5;
			} else if (crefoBGScore == -2) {
				externalFactor = (long) 1;
			} else if (crefoBGScore >= 100 && crefoBGScore <= 199) {
				externalFactor = (long) 2;
			} else if (crefoBGScore >= 200 && crefoBGScore <= 270) {
				externalFactor = (long) 1.5;
			} else if (crefoBGScore >= 271 && crefoBGScore <= 300) {
				externalFactor = (long) 1.25;
			} else if (crefoBGScore >= 301 && crefoBGScore <= 349) {
				externalFactor = (long) 1;
			} else if (crefoBGScore >= 350 && crefoBGScore <= 399) {
				externalFactor = (long) 0.75;
			} else if (crefoBGScore >= 400 && crefoBGScore <= 429) {
				externalFactor = (long) 0.25;
			} else if (crefoBGScore >= 430) {
				externalFactor = (long) 0;
			}
			
			//set branchFactor
			if (branchGroup == BranchGroup.HORECA) {
				branchFactor = (long) 1.5;
			} else if (branchGroup == BranchGroup.TRADER) {
				branchFactor = (long) 1.3;
			} else if (branchGroup == BranchGroup.SCO) {
				branchFactor = (long) 1.2;
			}
			
			//set BaseCalculationFactor
			if (crefoBGScore >= 100 && crefoBGScore <= 199) {
				if (recomendedLimit.equals(0)) {
					baseCalculation4NewCustomer = 5000;
				} else if (recomendedLimit.compareTo(new BigDecimal(0)) == 1) {
					baseCalculation4NewCustomer = recomendedLimit.doubleValue();
				}
			} else if (crefoBGScore >= 200 && crefoBGScore <= 270) {
				if (recomendedLimit.equals(0)) {
					baseCalculation4NewCustomer = 0;
				} else if (recomendedLimit.compareTo(new BigDecimal(0)) == 1) {
					baseCalculation4NewCustomer = recomendedLimit.doubleValue();
				}
			} else if (crefoBGScore >= 271 && crefoBGScore <= 300) {
				if (recomendedLimit.equals(0)) {
					baseCalculation4NewCustomer = 0;
				} else if (recomendedLimit.compareTo(new BigDecimal(0)) == 1) {
					baseCalculation4NewCustomer = recomendedLimit.doubleValue();
				}
			} else {
				if (recomendedLimit.equals(0)) {
					baseCalculation4NewCustomer = 5000;
				}
			}
			
			double tempLimit = baseCalculation4NewCustomer * internalFactor * externalFactor * branchFactor * paymentFactor.doubleValue();
			Integer calculatedLimit = new Integer((int) tempLimit);
			
			if (calculatedLimit < 100) {
				calculatedLimit = 100;
			}
			result.setCalculatedLimit(calculatedLimit);
		}
		
		//DetermineMissingExternalInformation
//		crefoBGScore=-1 means crefo sends blank score for company with foundation date 
				//less than 1 year
		if (crefoBGScore == -1) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setRejectReasonID(Constants.REJECT_STRATEGY_NEED_EXTERNAL_INFO);
			return result;
		}
		
		if (Arrays.asList(new String[] {"A", "AA", "AAA", "B", "BB", "BBB", "D", "MC3"})
				.contains(getCustomerMasterData().getSecurityIndicator())) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setRejectReasonID(Constants.REJECT_STRATEGY_RISK_CATEG_SKO);
			return result;
		}
		
		//Check public sector branch code
		if (getCustomerMasterData().getBranchFamilyId() == 83) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_PUBLIC_COMPANY);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);		
			return result; 			
		}		
		
		//Check attachments present
		if (this.getRequestHasAttachment()) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(0);
			return result; 
		}
		
		//Check attachments present
		if (getRequestHasComment()) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(0);
			return result; 
		}		
		
		//Check KO internal score
		if (result.getScore() >= 393) {
			result.setRejectReasonID(Constants.REJECT_KO_SCORECARD);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setCheckoutCheckCD(30);
			return result; 
		}
		// Check KO external score
		if (crefoBGScore > 430) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_CREFO_BG);
			result.setCheckoutCheckCD(30);
			return result;
		}
		
		//Check SKO external score for NewCustomer && Check GENERAL SKO external score 
		if (crefoBGScore > 100 && crefoBGScore < 200 || (crefoBGScore > 300 && crefoBGScore < 430)) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setRejectReasonID(Constants.REJECT_EXTERNAL_SEMI_KO_CREFO_BG);
			return result;
		}
		
		//Check credit blocking for SKO
		if (getCustomerMasterData().getCheckoutCheck() != null && getCustomerMasterData().getCheckoutCheck() == 30) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CREDIT_BLOCKING_FOR_SKO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result; 
		}
		
		//Check SKO internal score
		if (result.getScore() > 326) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_SCORECARD);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result; 
		}
		
		//Check requests in last 3 months
		if (getCustomerMasterData().getRequestCountL3M() != null && getCustomerMasterData().getRequestCountL3M() > 3) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result; 
		}
		
		//Check payment terms
		if (getCustomerCreditData().getRequestedPaymentTerms() != null  && getCustomerCreditData().getRequestedPaymentTerms().compareTo("15") > 0 && new Payment().getCreditSettleFrequencyCd() == null) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result; 
		}
		
		//Check payment types
		if (getPayment().getCreditSettleFrequencyCd() == 1 && Arrays.asList(new String[] {"1", "3","5", "7", "10", "15", "20", "25", "30", "31"}).contains(getCustomerCreditData().getRequestedPaymentTerms()) 
		||(getPayment().getCreditSettleFrequencyCd() == 2 && Arrays.asList(new String[] {"15", "20", "31"}).contains(getCustomerCreditData().getRequestedPaymentTerms()))) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_METHOD_QUESTIONABLE);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		
		//Check maximum limit
		if (getCreditRequest().getRequestedCustLimit() != null && getCreditRequest().getRequestedCustLimit() > 5000) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result; 
		}
		
		//Set score card traffic light
		if (result.getCalculatedLimit() != null && getCreditRequest().getCalculatedMaxLimit() < getCreditRequest().getRequestedCustLimit()
				&& result.getScore() <= 326) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT);
		} else {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
		}
		
		return result;
	}

	private BranchGroup setBranchFamily() {
		BranchGroup branchGroup=null;
		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.HORECA;
		} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.TRADER;
		} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.SCO;
		}
		return branchGroup;
	}

	@Override
	public List<String> extractMissingFields() {
		
		List<String> missingFields = new ArrayList<String>();
		
		if (getCustomerMasterData().getCompanyName() == null) {
			missingFields.add(Constants.FIELD_COMPANY_NAME);
		}
		
		if (getCustomerMasterData().getLegalForm() == null) {
			missingFields.add(Constants.FIELD_LEGAL_FORM_CODE);
		}

		if (getCustomerMasterData().getCompanyFoundationDate() == null) {
			missingFields.add(Constants.FIELD_COMPANY_FOUNDATION_DATE);
		}
		
		if (getCustomerMasterData().getRegistrationDate() == null) {
			missingFields.add(Constants.FIELD_REGISTRATION_DATE);
		}
	
		if (getCustomerMasterData().getStreet() == null) {
			missingFields.add(Constants.FIELD_STREET);
		}
		
		if (getCustomerMasterData().getCity() == null) {
			missingFields.add(Constants.FIELD_CITY);
		}			
		
		
		if (getCustomerMasterData().getZipCode() == null) {
			missingFields.add(Constants.FIELD_ZIP_CODE);
		}
		
		if (getCustomerMasterData().getVatNumber() == null) {
			missingFields.add(Constants.FIELD_VAT_SPEC_NO);
		}			

		//If this is a one man business company additional mandatory fields are needed
		if (isOneManBusiness) {
			
			if (getCustomerMasterData().getCompanyOwnerLastName() == null) {
				missingFields.add(Constants.FIELD_OWNER_LAST_NAME);
			}
			
			if (getCustomerMasterData().getBirthday() == null) {
				missingFields.add(Constants.FIELD_BIRTH_DATE);
			}
		}
		
		return missingFields;
	}
}
