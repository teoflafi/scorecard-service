package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorCZ extends ScoreCalculator {

	//private boolean isNewCustomer = false;
	private final String BEFORE_SIX_MONTH_SALES = "beforeSixMonthSales";
	private Integer branchMainGroupID;
	private Integer assortSectionID;
	private final String ASSORT_SECTION_ID = "assortSectionID";
	private final String BRANCH_MAIN_GROUP_ID = "branchMainGroupID";
	private final String CU_NO_INVOICES_L1M = "CuNoInvoicesL1M";
	private final String CU_NO_INVOICES_L6M = "CuNoInvoicesL6M";
	private final String CU_NO_VISIT_L1M = "CuNoVisitL1M";
	// private final String PERC_NON_FOOD = "percNonFood";
	private final String PERC_FOOD = "percFood";
	private int scorecardTrafficLightID = -1;

	private BranchGroup branchGroup;
	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays
			.asList(new Integer[] { 100, 101, 102, 110, 120, 130, 140, 141, 150, 151, 160, 170, 190 });
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] { 300, 301, 310, 311, 312, 313, 314,
			315, 316, 317, 318, 320, 330, 331, 332, 340, 341, 350, 351, 352, 353, 354, 370, 307, 308 });
	private final List<Integer> BRANCH_FAMILIY_SCO = Arrays
			.asList(new Integer[] { 500, 510, 520, 530, 540, 360, 700, 710, 720, 730, 810, 820 });
	private final List<Integer> REJECT_REASON_ID = Arrays.asList(new Integer[] { 7, 9, 62, 3, 35 });
	private final List<Integer> SKO_REJECT_REASON_ID = Arrays.asList(new Integer[] { 66, 32, 76, 4, 99, 71, 70, null });

	private BigDecimal CuNoInvoicesL1M = null;
	private BigDecimal CuNoInvoicesL6M = null;
	private BigDecimal CuNoVisitL1M = null;
	private long internalScoreIndex = 0;

	public ScoreCalculatorCZ(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);
	}

	@Override
	public ScoreResult calculateScore() {
		ScoreResult result = new ScoreResult();
		// InitCheckoutCheckCd
		result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());
		// CheckPublicSector
		if (getCustomerMasterData().getLegalForm() != null && getCustomerMasterData().getLegalForm().equals("8")) {

			if (getCreditRequest().getRequestedLimit() <= 150000 && getPayment() != null
					&& getPayment().getTermName() != null && Integer.parseInt(getPayment().getTermName()) <= 30) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				result.setCalculatedLimit(Integer.parseInt(getCreditRequest().getRequestedLimit() + ""));
				return result;
			} else if (getCreditRequest().getRequestedLimit() > 150000) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				return result;
			} else if (getPayment() != null && getPayment().getTermName() != null
					&& Integer.parseInt(getPayment().getTermName()) > 30) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
				return result;
			}
		} else {
			// Check request interval
			if (getCustomerMasterData().getLastRequestDate() != null
					&& Utils.getAgeInHours(getCustomerMasterData().getLastRequestDate()) < 1) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				;
				result.setRejectReasonID(Constants.REJECT_STRATEGY_REQUEST_INTEVRAL_TOO_SHORT);
				return result;
			}
			// TODO CheckMandatoryFields

			// CalculateBFSixMonthSale
			if (getCustomerCreditData().getSellValNspl12m() != null
					&& getCustomerCreditData().getSellValNspl6m() != null) {
				Long beforeSixMonthSales = getCustomerCreditData().getSellValNspl12m()
						- getCustomerCreditData().getSellValNspl6m();
				getCustomVariables().put(BEFORE_SIX_MONTH_SALES, beforeSixMonthSales);
			}
			// SetBranchMainGroupId
			if (Arrays.asList(new Integer[] { 100, 101, 102 }).contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 10;
			} else if (getCustomerMasterData().getBranchFamilyId() == 110) {
				branchMainGroupID = 11;
			} else if (getCustomerMasterData().getBranchFamilyId() == 120) {
				branchMainGroupID = 12;
			} else if (getCustomerMasterData().getBranchFamilyId() == 130) {
				branchMainGroupID = 13;
			} else if (Arrays.asList(new Integer[] { 140, 141 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 14;
			} else if (Arrays.asList(new Integer[] { 150, 151 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 15;
			} else if (getCustomerMasterData().getBranchFamilyId() == 160) {
				branchMainGroupID = 16;
			} else if (getCustomerMasterData().getBranchFamilyId() == 170) {
				branchMainGroupID = 17;
			} else if (getCustomerMasterData().getBranchFamilyId() == 190) {
				branchMainGroupID = 19;
			} else if (Arrays.asList(new Integer[] { 300, 301 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 30;
			} else if (Arrays.asList(new Integer[] { 310, 311, 312, 313, 314, 315, 316, 317, 318 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 31;
			} else if (getCustomerMasterData().getBranchFamilyId() == 320) {
				branchMainGroupID = 32;
			} else if (Arrays.asList(new Integer[] { 330, 331, 332 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 33;
			} else if (Arrays.asList(new Integer[] { 340, 341 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 34;
			} else if (Arrays.asList(new Integer[] { 350, 351, 352, 353, 354 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 35;
			} else if (getCustomerMasterData().getBranchFamilyId() == 360) {
				branchMainGroupID = 36;
			} else if (getCustomerMasterData().getBranchFamilyId() == 370) {
				branchMainGroupID = 38;
			} else if (getCustomerMasterData().getBranchFamilyId() == 500) {
				branchMainGroupID = 50;
			} else if (getCustomerMasterData().getBranchFamilyId() == 510) {
				branchMainGroupID = 51;
			} else if (getCustomerMasterData().getBranchFamilyId() == 520) {
				branchMainGroupID = 52;
			} else if (getCustomerMasterData().getBranchFamilyId() == 530) {
				branchMainGroupID = 53;
			} else if (getCustomerMasterData().getBranchFamilyId() == 540) {
				branchMainGroupID = 58;
			} else if (getCustomerMasterData().getBranchFamilyId() == 700) {
				branchMainGroupID = 70;
			} else if (getCustomerMasterData().getBranchFamilyId() == 710) {
				branchMainGroupID = 71;
			} else if (getCustomerMasterData().getBranchFamilyId() == 720) {
				branchMainGroupID = 72;
			} else if (getCustomerMasterData().getBranchFamilyId() == 730) {
				branchMainGroupID = 78;
			} else if (getCustomerMasterData().getBranchFamilyId() == 810) {
				branchMainGroupID = 81;
			} else if (getCustomerMasterData().getBranchFamilyId() == 820) {
				branchMainGroupID = 82;
			} else if (Arrays.asList(new Integer[] { 307, 308 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 37;
			}

			if (branchMainGroupID != null)
				getCustomVariables().put(BRANCH_MAIN_GROUP_ID, branchMainGroupID);

			// SetAssortSelectionId
			if (Arrays.asList(new Integer[] { 10, 11, 12, 13, 14, 15, 16, 17, 19, 29 }).contains(branchMainGroupID)) {
				assortSectionID = 1;
			} else if (Arrays.asList(new Integer[] { 38, 30, 31, 32, 33, 34, 35, 39, 37 })
					.contains(branchMainGroupID)) {
				assortSectionID = 3;
			} else if (Arrays.asList(new Integer[] { 50, 51, 52, 53, 58, 59 }).contains(branchMainGroupID)) {
				assortSectionID = 5;
			} else if (Arrays.asList(new Integer[] { 78, 36, 70, 71, 72, 78, 81, 82, 89 })
					.contains(branchMainGroupID)) {
				assortSectionID = 7;
			}

			if (assortSectionID != null)
				getCustomVariables().put(ASSORT_SECTION_ID, assortSectionID);
			if (getCustomerMasterData().getRegistrationDate() != null) {
				getCustomerMasterData().setCustomerRelation(
						Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()));
			}

			// SetScorecardBranchGroup
			if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchGroup = BranchGroup.HORECA;
			} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchGroup = BranchGroup.TRADER;
			} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchGroup = BranchGroup.SCO;
			} else {
				result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			}
		
//			if (getCustomerMasterData().getCustomerRelation() < 12) {
//				isNewCustomer = true;
//			}

			// CheckNewCustomerL3M
			if (getCustomerMasterData().getCustomerRelation() < 3) {
				if (getRequestHasAttachment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else if (getRequestHasComment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_NEW_CUSTOMER_3M);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
					return result;
				}

			}

			// CheckNewCustomerL6M
			if (getCustomerCreditData().getSellValNspl3m() == null) {
				getCustomerCreditData().setSellValNspl3m(0l);
			}
			if (getCustomerMasterData().getCustomerRelation() > 3 && getCustomerMasterData().getCustomerRelation() < 6
					&& getCustomerCreditData().getSellValNspl3m() < 60000) {
				if (getRequestHasAttachment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else if (getRequestHasComment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_NEW_CUSTOMER_3M);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
					return result;
				}
			}

			// initScore
			result.setScore(0);

			// FoodAndNonFoodPrecCalculation
			if (getCustomerCreditData().getSellValNspl12m() != null && getCustomerCreditData().getSellValNspl12m() != 0
					&& getCustomerCreditData().getSellValNspnfl12m() != null) {
				BigDecimal totalFoodSales = new BigDecimal(getCustomerCreditData().getSellValNspfl12m());
				BigDecimal totalSales = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
				Double percFood = totalFoodSales.divide(totalSales, 2, RoundingMode.HALF_UP).doubleValue() * 100;

				getCustomVariables().put(PERC_FOOD, percFood);
			}

			// if (getCustomerCreditData().getSellValNspl12m() != null &&
			// getCustomerCreditData().getSellValNspl12m() != 0
			// && getCustomerCreditData().getSellValNspnfl12m() != null) {
			// BigDecimal totalFoodSales = new
			// BigDecimal(getCustomerCreditData().getSellValNspnfl12m());
			// BigDecimal totalSales = new
			// BigDecimal(getCustomerCreditData().getSellValNspl12m());
			// Double percFood = totalFoodSales.divide(totalSales, 2,
			// RoundingMode.HALF_UP).doubleValue() * 100;
			//
			// getCustomVariables().put(PERC_FOOD, percFood);
			// }

			// CalculateScorecardAttributeMissingInMDW
			BigDecimal numInvoicesL3M = BigDecimal.ZERO;
			BigDecimal numPurchasesL3M = BigDecimal.ZERO;
			//BigDecimal sellValNspLYL3M = BigDecimal.ZERO;

			if (getCustomerCreditData().getNumInvoicesl3m() != null) {
				numInvoicesL3M = new BigDecimal(getCustomerCreditData().getNumInvoicesl3m());
				CuNoInvoicesL1M = numInvoicesL3M.divide(new BigDecimal("3"));
				CuNoInvoicesL6M = numInvoicesL3M.multiply(new BigDecimal(2));
				getCustomVariables().put(CU_NO_INVOICES_L1M, CuNoInvoicesL1M);
				getCustomVariables().put(CU_NO_INVOICES_L6M, CuNoInvoicesL6M);
			}

			if (getCustomerCreditData().getNumPurchasesfStorel3m() != null) {
				numPurchasesL3M = new BigDecimal(getCustomerCreditData().getNumPurchasesfStorel3m());
				CuNoVisitL1M = numPurchasesL3M.divide(new BigDecimal(3));
				getCustomVariables().put(CU_NO_VISIT_L1M, CuNoVisitL1M);
			}
//			if (getCustomerCreditData().getSellValNsplyl3m() != null) {
//				sellValNspLYL3M = new BigDecimal(getCustomerCreditData().getSellValNsplyl3m());
//			}

			// TODO EvaluateBranchGroup

			ScoreRuleEngine scoreRuleEngine = new ScoreRuleEngine(getScoreRuleRepository(), getCustomerMasterData(),
					getCustomerCreditData(), getPayment(), branchGroup, getCustomVariables());
			result.setScore(scoreRuleEngine.calculateScore());
			// UnifiedTrafficLight
			if (result.getScore() <= 326) {
				// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				scorecardTrafficLightID = Constants.TRAFFIC_LIGHT_GREEN;
			} else if (result.getScore() >= 327 || result.getScore() <= 392) {
				// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				scorecardTrafficLightID = Constants.TRAFFIC_LIGHT_YELLOW;
			} else if (result.getScore() > 392) {
				// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				scorecardTrafficLightID = Constants.TRAFFIC_LIGHT_RED;
			}
			// DetermineInternalScoreIndex
			if (result.getScore() >= 0 && result.getScore() <= 326) {
				internalScoreIndex = 1;
			} else if (result.getScore() >= 327 && result.getScore() <= 392) {
				internalScoreIndex = 2;
			} else if (result.getScore() >= 393 && result.getScore() <= 493) {
				internalScoreIndex = 3;
			} else if (result.getScore() > 493) {
				internalScoreIndex = 4;
			}

			// TurnoverL12M missing
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				// TurnoverMissingExit
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_TURNOVER_MISSING);
				return result;
			}

			// CalculateLimit
			BigDecimal additionalFactor = new BigDecimal(1);
			BigDecimal averageTurnover = new BigDecimal(0);
			BigDecimal bigDecimal_sellValNspL12M = new BigDecimal(0);
			BigDecimal internalFactor = new BigDecimal(1);
			int tempBigint = 0;
			BigDecimal tempLimit = new BigDecimal(0);
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				getCustomerCreditData().setSellValNspl12m(0l);
			}
			bigDecimal_sellValNspL12M = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			averageTurnover = bigDecimal_sellValNspL12M.divide(new BigDecimal(12), 2, RoundingMode.HALF_UP);
			if (internalScoreIndex == 1) {
				internalFactor = new BigDecimal(1.5 + "");
			} else if (internalScoreIndex == 2) {
				internalFactor = new BigDecimal(1.25 + "");
			} else if (internalScoreIndex == 3) {
				internalFactor = new BigDecimal(1 + "");
			} else if (internalScoreIndex == 4) {
				internalFactor = new BigDecimal(0.25 + "");
			}
			if (branchGroup == BranchGroup.HORECA) {
				additionalFactor = new BigDecimal(1.5 + "");
			} else if (branchGroup == BranchGroup.TRADER) {
				additionalFactor = new BigDecimal(1.3 + "");
			} else if (branchGroup == BranchGroup.SCO) {
				additionalFactor = new BigDecimal(1.2 + "");
			}

			tempLimit = averageTurnover.multiply(internalFactor).multiply(additionalFactor);

			tempBigint = BigInteger.valueOf(tempLimit.toBigInteger().longValue() / 100).intValue() * 100;

			if (tempBigint < 100) {
				result.setCalculatedLimit(100);
			} else {
				result.setCalculatedLimit(tempBigint);
			}
			if (getRequestHasAttachment()) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setCheckoutCheckCD(0);
				return result;
			}
			if (getRequestHasComment()) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setCheckoutCheckCD(0);
				return result;
			}

			// CheckInternalKOCriteria
			if (getCustomerCreditData().getMonitionLevelMaxl12m() >= 2) {
				result.setRejectReasonID(Constants.REJECT_KO_DUNNING_LEVEL_TOO_HIGH);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			} else if (scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_RED) {
				result.setRejectReasonID(Constants.REJECT_KO_SCORECARD);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}

			// CheckBlockingReasonCode
//			boolean blockingReasonCodeNull = false;
//			if (getCustomerMasterData().getCheckoutCheck() == null) {
//				blockingReasonCodeNull = true;
//			}

			if (getCustomerMasterData().getCheckoutCheck() != null && getCustomerMasterData().getCheckoutCheck() == 30
					&& REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_KO_PAY_CASH_ONLY);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}
			if (getCustomerMasterData().getCheckoutCheck() != null && getCustomerMasterData().getCheckoutCheck() == 35
					&& REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_KO_DELETED);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}
			if (getCustomerMasterData().getCheckoutCheck() == null
					&& REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_KO_ACTIVE_CUSTOMER);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			} else if (getCustomerMasterData().getCheckoutCheck() == null
					&& getCustomerMasterData().getBlockingReason() == null) {

				// CheckSKOBlockingReasonCode

				if (getCustomerMasterData().getCheckoutCheck() != null
						&& getCustomerMasterData().getCheckoutCheck() == 30
						&& SKO_REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_PAY_CASH_ONLY);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(30);
					return result;
				}
				if (getCustomerMasterData().getCheckoutCheck() != null
						&& getCustomerMasterData().getCheckoutCheck() == 35
						&& SKO_REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_DELETED);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(30);
					return result;
				}
				if (getCustomerMasterData().getCheckoutCheck() == null
						&& SKO_REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_ACTIVE_CUSTOMER);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(30);
					return result;
				}

				// CheckInternaSKOCriteria
				if (getCustomerMasterData().getRequestCountL3M() != null
						&& getCustomerMasterData().getRequestCountL3M() > 3) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				} else if (getPayment() != null && getPayment().getTermName() != null
						&& getPayment().getTermName().startsWith("Revolving")
						&& Integer.parseInt(getPayment().getTermName()) > 14) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				} else if (getPayment() != null && getPayment().getTermName() != null
						&& getPayment().getTermName().startsWith("Fix")
						&& Integer.parseInt(getPayment().getTermName()) > 7) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				} else if (scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_YELLOW) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_SCORECARD);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				}

				// CheckMaximumLimit
				if (getCreditRequest().getRequestedLimit() > 100000) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				}

				// SetscorecardTrafficLight
				if (result.getCalculatedLimit() < getCreditRequest().getRequestedLimit()
						&& scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_GREEN) {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT);
				} else {
					result.setTrafficLightID(scorecardTrafficLightID);

				}

			}

		}

		return result;
	}

	@Override
	public List<String> extractMissingFields() {

		return null;
	}

}
