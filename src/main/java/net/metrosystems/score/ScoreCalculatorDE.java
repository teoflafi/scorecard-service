package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.metrosystems.domain.ExternalScore;
import net.metrosystems.domain.ProcessTypeEnum;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorDE extends ScoreCalculator {

	private boolean isOneManBusiness = false;

	private boolean isQuickCheck = false;
	// boolean jobMode = false;
	private Integer branchMainGroupID;
	private Integer assortSectionID;
	private BranchGroup branchGroup;
	private int scorecardTrafficLightID = -1;
	private final String BEFORE_SIX_MONTH_SALES = "beforeSixMonthSales";
	// private final String PERC_FOOD = "percFood";
	private final String PERC_NON_FOOD = "percNonFood";
	private final String ASSORT_SECTION_ID = "assortSectionID";
	private final String BRANCH_MAIN_GROUP_ID = "branchMainGroupID";
	boolean isNewCustomer = false;

	// External Bureau
	private Integer EXTERNAL_BUREAU_ID_CEG = 2;
	private Integer EXTERNAL_BUREAU_ID_CREFO = 1;
	private Integer EXTERNAL_BUREAU_ID_SCHUFA_B2B = 17;
	private Integer EXTERNAL_BUREAU_ID_SCHUFA_B2C = 3;

	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays
			.asList(new Integer[] { 100, 101, 102, 103, 104, 110, 120, 121, 130, 131, 132, 140, 141, 150, 160, 161, 170,
					171, 190, 106, 107, 123, 133, 142, 151, 162, 191, 192, 193, 105, 108, 109, 111, 122, 172 });
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] { 300, 301, 310, 311, 320, 330, 340,
			341, 350, 380, 381, 382, 302, 312, 321, 331, 313, 303, 383, 351, 384, 385, 386 });
	private final List<Integer> BRANCH_FAMILIY_SCO = Arrays.asList(
			new Integer[] { 360, 500, 501, 510, 520, 521, 530, 580, 700, 701, 710, 711, 720, 721, 722, 810, 820 });
	private final List<Integer> INDUSTRY_SECTOR_CAFES = Arrays.asList(new Integer[] { 321, 322, 323, 324, 325, 326, 327,
			328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 518 });
	private final List<Integer> INDUSTRY_SECTOR_GASTRO_KITCHEN = Arrays.asList(
			new Integer[] { 340, 341, 342, 343, 345, 610, 612, 613, 614, 615, 616, 620, 650, 651, 803, 804, 808 });
	private final List<String> INDUSTRY_SECTOR_SONSTIGE = Arrays.asList(new String[] { "V11", "N21", "U12", "N23",
			"V31", "X52", "X51", "N27", "N28", "N29", "V36", "812", "813", "814" });
	private final List<Integer> INDUSTRY_SECTOR_RESTAURANT_GASTRO = Arrays.asList(new Integer[] { 310, 311, 312, 313,
			314, 315, 316, 317, 318, 319, 320, 512, 515, 514, 344, 510, 801, 802, 805, 806, 807, 810, 811 });
	private final List<String> INDUSTRY_SECTOR_FOOD_FACHHANDEL = Arrays.asList(new String[] { "113", "360", "361",
			"362", "114", "132", "363", "364", "365", "211", "131", "130", "133", "369", "370", "371", "372", "373",
			"387", "356", "405", "L11", "L10", "374", "631", "375", "376", "809" });
	private final List<Integer> INDUSTRY_SECTOR_FOOD_SONDERVERTRIEB = Arrays.asList(new Integer[] { 101, 102, 104, 380,
			381, 382, 100, 383, 412, 384, 385, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403 });
	private final List<Integer> INDUSTRY_SECTOR_FOOD_HANDEL_SORT = Arrays
			.asList(new Integer[] { 110, 108, 107, 350, 106, 351, 352, 353, 354, 355, 120, 121, 122, 357 });
	private final List<Integer> INDUSTRY_SECTOR_NONFOOD_TRADERS = Arrays.asList(new Integer[] { 346, 409 });

	private final List<String> INDUSTRY_SECTOR_ALLFINANZ_HANDELSVERTRETER = Arrays
			.asList(new String[] { "V26", "K16", "G12", "L72", "V22", "L77", "L78", "S11", "S10", "L81", "L82" });

	private final List<String> INDUSTRY_SECTOR_FREIE_BERUFE_BERATUNG = Arrays.asList(new String[] { "V21", "Z25", "Z26",
			"Z27", "V34", "V27", "V32", "Z31", "Z32", "V20", "X50", "Z35", "Z36", "Z37" });

	private final List<String> INDUSTRY_SECTOR_HANDWERKER = Arrays.asList(new String[] { "T12", "L18", "L20", "H16",
			"J16", "L21", "L24", "H11", "L25", "J10", "G11", "L29", "K10", "L31", "L32" });
	private final List<String> INDUSTRY_SECTOR_HEALTH_GESUNDHEIT_PFLEGE = Arrays.asList(new String[] { "Z38", "Z39",
			"Z40", "Z41", "Z42", "Z43", "Z44", "Z45", "Z46", "E11", "712", "Z49", "Z50", "Z51", "Z52" });
	private final List<String> INDUSTRY_SECTOR_MEDIA_MEDIEN_KULTUR_UNTERHALTUNG = Arrays
			.asList(new String[] { "Z53", "Z54", "Z55", "V13", "Z61", "Z56", "Z57", "Z60" });
	private final List<String> INDUSTRY_SECTOR_NONFOOD_HANDEL = Arrays
			.asList(new String[] { "L33", "L34", "L35", "B10", "J13", "L36", "L30", "P10", "P13", "P11", "L43", "L44",
					"L45", "H13", "F11", "C10", "A11", "M11", "T11", "116", "L53", "D10", "L55", "L56", "L57", "L58",
					"D12", "L60", "L61", "X13", "L96", "L64", "L65", "L66", "815", "816" });
	private final List<Integer> INDUSTRY_SECTOR_OTHERS = Arrays.asList(new Integer[] { 377, 378, 379 });
	private final List<String> INDUSTRY_SECTOR_PRODUZIERENDES_GEWERBE = Arrays
			.asList(new String[] { "L67", "L68", "X10", "L70", "L71" });
	private final List<String> INDUSTRY_SECTOR_VERKEHR_VERSORGUNG = Arrays.asList(new String[] { "Z10", "Z11", "Z12",
			"K12", "Z14", "Z15", "K17", "Z17", "Z16", "K13", "Z20", "Z21", "Z22", "V33" });

	public ScoreCalculatorDE(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);
	}

	@Override
	public ScoreResult calculateScore() {

		ScoreResult result = new ScoreResult();
		result.setScore(0);

		// Check processType from MERC_PROCESSTYPE table;
		// CustomerDetails class for the processType variable
		if (this.getProcessType() == ProcessTypeEnum.QUICK_CHECK_REQUEST_PROCESS) {
			isQuickCheck = true;
		}

		// Set checkout check code
		result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());

		// init JobMode
		// JobMode -> normal scoring
		// TODO

		// Check employee branch code
		if (Arrays.asList(new String[] { "990", "996", "997", "999", "99A", "99B", "99C" })
				.contains(getCustomerMasterData().getBranchId())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_EMPLOYEE);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// Check blocking reason code
		if (getCustomerMasterData().getBlockingReason() != null && getCustomerMasterData().getBlockingReason() == 9) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_INACTIVE_IN_CFM);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// Check affiliated company branch code
		if ((Arrays.asList(new String[] { "MINT", "INTN" }).contains(getCustomerMasterData().getTypeCode()))
				|| (!Arrays.asList(new String[] { "MINT", "INTN" }).contains(getCustomerMasterData().getTypeCode())
						&& getCustomerMasterData().getBranchId() != null
						&& getCustomerMasterData().getBranchId().equals("980"))) {

			// Check attachment and comments for affiliated company
			if (getRequestHasAttachment() == true || getRequestHasComment() == true) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				if (getRequestHasAttachment() == true) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
				} else {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
				}
			} else {
				// Grant limit up to upper bound
				if (getCreditRequest().getRequestedLimit() <= 10000 || isQuickCheck == true) {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
					if (isQuickCheck == false) {
						result.setCalculatedLimit(getCreditRequest().getRequestedLimit().intValue());
					}
				} else {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				}
			}

			return result;

		} else {
			// for Not internalCompany
			// TODO null pointer exception
			if (getCustomerMasterData().getLegalForm() != null && getCustomerMasterData().getLegalForm().equals("5")
					|| Arrays.asList(new String[] { "342", "349", "612", "613", "614", "615", "616", "L85", "L87",
							"L88", "L91", "L93", "V95" }).contains(getCustomerMasterData().getBranchId())) {

				// for public Sector - check schaper and attachment
				if (getRequestHasAttachment() == true && getCreditRequest().getRequestedLimit() <= 10000) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
				} else {
					if (getCreditRequest().getRequestedLimit() <= 10000 || isQuickCheck == true) {
						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
						if (isQuickCheck == false) {
							result.setCalculatedLimit(getCreditRequest().getRequestedLimit().intValue());
						}
					} else {
						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
						result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
					}
				}

				return result;

			}
		}

		// for Not public Sector - Check request interval
		if (getCustomerMasterData().getLastRequestDate() != null
				&& Utils.getAgeInHours(getCustomerMasterData().getLastRequestDate()) < 1 && isQuickCheck == false) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_REQUEST_INTEVRAL_TOO_SHORT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// Check one man business (for interval not too short)
		if (getCustomerMasterData().getLegalForm() != null && (getCustomerMasterData().getLegalForm().equals("1")
				|| getCustomerMasterData().getLegalForm().equals("3"))) {
			isOneManBusiness = true;
		}
		// Check mandatory fields
		// If one or more mandatory fields are missing reject the
		// scoring
		if (isQuickCheck == false) {
			List<String> missingFields = extractMissingFields();
			if (missingFields.size() > 0) {
				result.setMissingFields(missingFields);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_MANDATORY_FIELDS_MISSING);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			}
		}

		// Set scoreCard Branch Group
//		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchGroup = BranchGroup.HORECA;
//		} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchGroup = BranchGroup.TRADER;
//		} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchGroup = BranchGroup.SCO;
//		} else {
//			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
//			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
//			return result;
//		}

		branchGroup=setBranchFamily();
		if (branchGroup == null) {
			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		if (Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()) < 12) {

			branchGroup = BranchGroup.NEW_CUSTOMER;
			isNewCustomer = true;
		}

		// Set IndustrySector2
		// Constants declared at the start of this class

		result.setScore(0);

		// Calculate before six months sales
		if (getCustomerCreditData() != null && getCustomerCreditData().getSellValNspl12m() != null
				&& getCustomerCreditData().getSellValNspl6m() != null) {
			Long beforeSixMonthSales = getCustomerCreditData().getSellValNspl12m()
					- getCustomerCreditData().getSellValNspl6m();
			getCustomVariables().put(BEFORE_SIX_MONTH_SALES, beforeSixMonthSales);
		}

		// FoodAndNonFoodPrecCalculation
		if (getCustomerCreditData() != null && getCustomerCreditData().getSellValNspl12m() != null
				&& getCustomerCreditData().getSellValNspl12m() != 0
				&& getCustomerCreditData().getSellValNspnfl12m() != null) {
			BigDecimal totalNonFoodSales = new BigDecimal(getCustomerCreditData().getSellValNspnfl12m());
			BigDecimal totalSales = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			Double percNonFood = totalNonFoodSales.divide(totalSales, 2, RoundingMode.HALF_UP).doubleValue() * 100;

			getCustomVariables().put(PERC_NON_FOOD, percNonFood);
		}

		// Set branch main group
		if (Arrays.asList(new Integer[] { 105, 106, 107, 108, 109 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 10;
		} else if (getCustomerMasterData().getBranchFamilyId() == 111) {
			branchMainGroupID = 11;
		} else if (Arrays.asList(new Integer[] { 121, 122, 123 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 12;
		} else if (getCustomerMasterData().getBranchFamilyId() == 133) {
			branchMainGroupID = 13;
		} else if (getCustomerMasterData().getBranchFamilyId() == 142) {
			branchMainGroupID = 14;
		} else if (getCustomerMasterData().getBranchFamilyId() == 151) {
			branchMainGroupID = 15;
		} else if (getCustomerMasterData().getBranchFamilyId() == 162) {
			branchMainGroupID = 16;
		} else if (getCustomerMasterData().getBranchFamilyId() == 172) {
			branchMainGroupID = 17;
		} else if (Arrays.asList(new Integer[] { 191, 192, 193 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 19;
		} else if (Arrays.asList(new Integer[] { 302, 303 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 30;
		} else if (Arrays.asList(new Integer[] { 312, 313 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 31;
		} else if (getCustomerMasterData().getBranchFamilyId() == 321) {
			branchMainGroupID = 32;
		} else if (getCustomerMasterData().getBranchFamilyId() == 331) {
			branchMainGroupID = 33;
		} else if (getCustomerMasterData().getBranchFamilyId() == 351) {
			branchMainGroupID = 35;
		} else if (getCustomerMasterData().getBranchFamilyId() == 360) {
			branchMainGroupID = 36;
		} else if (Arrays.asList(new Integer[] { 383, 384, 385, 386 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 38;
		} else if (Arrays.asList(new Integer[] { 500, 501 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 50;
		} else if (getCustomerMasterData().getBranchFamilyId() == 510) {
			branchMainGroupID = 51;
		} else if (Arrays.asList(new Integer[] { 520, 521 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 52;
		} else if (getCustomerMasterData().getBranchFamilyId() == 530) {
			branchMainGroupID = 53;
		} else if (getCustomerMasterData().getBranchFamilyId() == 580) {
			branchMainGroupID = 58;
		} else if (Arrays.asList(new Integer[] { 700, 701 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 70;
		} else if (Arrays.asList(new Integer[] { 710, 711 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 71;
		} else if (Arrays.asList(new Integer[] { 720, 721, 722 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 72;
		} else if (getCustomerMasterData().getBranchFamilyId() == 999) {
			branchMainGroupID = 78;
		} else if (getCustomerMasterData().getBranchFamilyId() == 810) {
			branchMainGroupID = 81;
		} else if (getCustomerMasterData().getBranchFamilyId() == 820) {
			branchMainGroupID = 82;
		} else {
			throw new IllegalStateException("branchMainGroup not found");
		}
		if (branchMainGroupID != null)
			getCustomVariables().put(BRANCH_MAIN_GROUP_ID, branchMainGroupID);

		// set assort sectionID
		if (Arrays.asList(new Integer[] { 10, 11, 12, 13, 14, 15, 16, 17, 19 }).contains(branchMainGroupID)) {
			assortSectionID = 1;
		} else if (Arrays.asList(new Integer[] { 30, 31, 32, 33, 35, 38 }).contains(branchMainGroupID)) {
			assortSectionID = 3;
		} else if (Arrays.asList(new Integer[] { 50, 51, 52, 53, 58 }).contains(branchMainGroupID)) {
			assortSectionID = 5;
		} else if (Arrays.asList(new Integer[] { 36, 70, 71, 72, 78, 81, 82 }).contains(branchMainGroupID)) {
			assortSectionID = 7;
		}

		if (assortSectionID != null)
			getCustomVariables().put(ASSORT_SECTION_ID, assortSectionID);

		// calculate ageOfCompany based on foundationDate
		if (getCustomerMasterData().getCompanyFoundationDate() != null)
			getCustomerMasterData()
					.setAgeOfCompany(Utils.numberOfMonthsSinceDate(getCustomerMasterData().getCompanyFoundationDate()));
		// calculate customerRelation based on registrationDate
		if (getCustomerMasterData().getRegistrationDate() != null)
			getCustomerMasterData()
					.setCustomerRelation(Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()));
		// calculate custAge based on birthday
		if (getCustomerMasterData().getBirthday() != null)
			getCustomerMasterData()
					.setCustomerAge(Utils.numberOfMonthsSinceDate(getCustomerMasterData().getBirthday()));

		// Unified traffic light & Unified traffic light_QCR
		ScoreRuleEngine scoreRuleEngine = new ScoreRuleEngine(getScoreRuleRepository(), getCustomerMasterData(),
				getCustomerCreditData(), getPayment(), branchGroup, getCustomVariables());
		result.setScore(scoreRuleEngine.calculateScore());
		if (isQuickCheck) {
			// Unified traffic light_QCR
			// TODO Change
			if (result.getScore() <= 200) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				// scorecardTrafficLightID=Constants.TRAFFIC_LIGHT_GREEN;
			} else if (result.getScore() >= 201 || result.getScore() <= 492) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				// scorecardTrafficLightID=Constants.TRAFFIC_LIGHT_YELLOW;
			} else if (result.getScore() > 492) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				// scorecardTrafficLightID=Constants.TRAFFIC_LIGHT_RED;
			}
		} else {
			// Unified traffic light
			if (result.getScore() <= 326) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				// scorecardTrafficLightID=Constants.TRAFFIC_LIGHT_GREEN;
			} else if (result.getScore() >= 327 || result.getScore() <= 492) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				// scorecardTrafficLightID=Constants.TRAFFIC_LIGHT_YELLOW;
			} else if (result.getScore() > 492) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				// scorecardTrafficLightID=Constants.TRAFFIC_LIGHT_RED;
			}
		}

		// Check Age & Dunning Level

		if (Utils.numberOfMonthsSinceDate(getCustomerMasterData().getBirthday()) < 216 && isOneManBusiness == true) {
			result.setRejectReasonID(Constants.REJECT_KO_AGE_UNDER_18);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setCheckoutCheckCD(30);
			return result;
		} else if ((getCustomerCreditData().getMonitionLevelMaxl12m() >= 3 && isQuickCheck == false)
				|| (getCustomerCreditData().getMonitionLevelMaxl12m() >= 2 && isQuickCheck == true)) {
			result.setRejectReasonID(Constants.REJECT_KO_DUNNING_LEVEL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// Check attachment and commentsQuick

		if (!this.getRequestHasAttachment() && !this.getRequestHasComment()) {
			// Check Internal KO Criteria
			if (result.getCheckoutCheckCD() != null && result.getCheckoutCheckCD() == 30) {
				result.setRejectReasonID(Constants.REJECT_KO_BLOCK_IN_CFM);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			} else if (result.getTrafficLightID() == 1) {
				result.setCheckoutCheckCD(30);
				result.setRejectReasonID(Constants.REJECT_KO_SCORECARD);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			} else if ((getCustomerCreditData().getNumDebitEntriesl12m() != null
					&& getCustomerCreditData().getNumDebitEntriesl12m() > 3 && isQuickCheck)
					|| (getCustomerCreditData().getNumDebitEntriesl12m() != null
							&& getCustomerCreditData().getNumDebitEntriesl12m() >= 10 && !isQuickCheck)) {
				result.setCheckoutCheckCD(30);
				result.setRejectReasonID(Constants.REJECT_KO_TOO_MANY_BAD_DEBTS);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			}
		}



		int countExternalBureauLoop = 0;

		List<Integer> externalBureauIdsToBeUsed = new ArrayList<Integer>();

		long externalCreditreformScoreIndex = 0;
		long externalScoreCeg = -1;
		long externalScoreCreditreform = -1;
		long externalScoreSchufa = -1;
		long internalScoreIndex = 0;

		// TODO
		// TODO

		if (!isJobMode()) {
			if (isQuickCheck) {
				int securityCount = 0;
				// QuickCheckDetermineExternalBureauIDs
				while (countExternalBureauLoop >= 0 && securityCount < 2) {
					if (branchGroup == BranchGroup.HORECA) {
						if (isOneManBusiness && countExternalBureauLoop == 0) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);
							countExternalBureauLoop = -1;
						} else if (!isOneManBusiness && countExternalBureauLoop == 0) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
							countExternalBureauLoop = -1;
						}
					}

					if (branchGroup == BranchGroup.SCO) {
						if (isOneManBusiness && countExternalBureauLoop == 0) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
							countExternalBureauLoop = -1;
						} else if (!isOneManBusiness && countExternalBureauLoop == 0) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
							countExternalBureauLoop = -1;
						}
					}

					if (branchGroup == BranchGroup.TRADER) {
						if (isOneManBusiness && countExternalBureauLoop == 0) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
							countExternalBureauLoop = -1;
						} else if (!isOneManBusiness && countExternalBureauLoop == 0) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
							countExternalBureauLoop = -1;
						}
					}

				}

			} else {
				// Determine external bureau to be used
				if (branchGroup == BranchGroup.HORECA && isNewCustomer == false) {
					if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN) && isOneManBusiness
							&& (getCreditRequest().getRequestedLimit() > 750)) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& (getCreditRequest().getRequestedLimit() > 4000)) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() <= 2500 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() > 2500) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);
					}
				}

				else if (branchGroup == BranchGroup.TRADER && isNewCustomer == false) {
					if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 750
							&& getCreditRequest().getRequestedLimit() <= 5000 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 4500 && isOneManBusiness == false) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 5000 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() <= 2500 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() > 2500) {
						if (isOneManBusiness) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
						} else {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
						}

					}

				} else if (branchGroup == BranchGroup.SCO && isNewCustomer == false) {
					if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 1000
							&& getCreditRequest().getRequestedLimit() <= 4500 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 4500) {
						if (isOneManBusiness) {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
						} else {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
						}

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() <= 3000 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() > 3000
							&& getCreditRequest().getRequestedLimit() <= 4500) {
						if (isOneManBusiness) {

							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
						} else {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
						}

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() > 4500) {
						if (isOneManBusiness) {

							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2C);
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);

						} else {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_SCHUFA_B2B);
						}

					}
				} else if (isNewCustomer == true) {
					if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 750
							&& getCreditRequest().getRequestedLimit() <= 4500 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 4500
							&& getCreditRequest().getRequestedLimit() < 10000) {
						if (isOneManBusiness) {

							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);

						} else {
							externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);
						}

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_GREEN)
							&& getCreditRequest().getRequestedLimit() > 10000) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() <= 2500 && isOneManBusiness) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);

					} else if (result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)
							&& getCreditRequest().getRequestedLimit() > 2500) {
						externalBureauIdsToBeUsed.add(EXTERNAL_BUREAU_ID_CEG);

					}
				}
			}

			// DetermineMissingExternalBureaus
			boolean requiredBureauInformationFound = false;
			int currentId = -1;
			long tempBigInteger = -1;
			long currentAvailableId = -1;

			if (isQuickCheck && !(result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW))) {

			} else {
				for (Integer i : externalBureauIdsToBeUsed) {
					requiredBureauInformationFound = false;
					currentId = i;

					for (ExternalScore e : getExternalScores()) {

						// For each ::
						// customerCreditData/externalInformation.getXmlElement
						// In customerCreditData.getXmlElement
						// Assign :: tempBigInteger =
						// customerCreditData/externalInformation/@bureauID

						tempBigInteger = e.getInfoBureauID();
						currentAvailableId = tempBigInteger;

						if (currentAvailableId == currentId) {
							if ((currentAvailableId == Long.parseLong(EXTERNAL_BUREAU_ID_CEG.toString()))
									&& Utils.numberOfMonthsSinceDate((e.getRequestedAt())) <= 0) {

								requiredBureauInformationFound = true;
							} else if ((currentAvailableId == Long.parseLong(EXTERNAL_BUREAU_ID_SCHUFA_B2B.toString()))
									&& Utils.numberOfMonthsSinceDate((e.getRequestedAt())) <= 90) {
								requiredBureauInformationFound = true;
							} else if ((currentAvailableId == Long.parseLong(EXTERNAL_BUREAU_ID_SCHUFA_B2C.toString()))
									&& Utils.numberOfMonthsSinceDate((e.getRequestedAt())) <= 0) {
								requiredBureauInformationFound = true;
							}
						} else if (branchGroup == BranchGroup.TRADER && currentId == EXTERNAL_BUREAU_ID_CEG
								&& currentAvailableId == Long.parseLong(EXTERNAL_BUREAU_ID_SCHUFA_B2C.toString())
								&& Utils.numberOfMonthsSinceDate((e.getRequestedAt())) <= 0) {
							requiredBureauInformationFound = true;
							// break

						}

						if (!requiredBureauInformationFound) {
							// Assign :: continueFlow = false
							// Execute ::
							// creditResult.addChildElement(neededExternalInformation)
							// Execute ::
							// creditResult/neededExternalInformation/@informationBureauID
							// = BigInteger
							// Execute :: creditResult/@rejectReasonID =
							// REJECT_STRATEGY_NEED_EXTERNAL_INFO
							//
							result.setRejectReasonID(Constants.REJECT_STRATEGY_NEED_EXTERNAL_INFO);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							return result;
						}

					}

				}
			}

			// TODO Checking Missing Info

			// ConvertAvailableExternalScores
			for (ExternalScore e : getExternalScores()) {
				if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_CEG)) {
					externalScoreCeg = Long.parseLong(e.getScore());
				} else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2C)) {
					externalScoreSchufa = Long.parseLong(e.getScore());
				} else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2B)) {
					externalScoreCreditreform = Long.parseLong(e.getScore());
				}
			}

			// CheckAttachementsAndComments

			if (getRequestHasAttachment()) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setCheckoutCheckCD(0);
				return result;

			} else if (getRequestHasComment()) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setCheckoutCheckCD(0);
				return result;
			}

			// Quick Check
			// TODO creditResult/@checkoutCheckCD = 30
			if (isQuickCheck) {
				// CheckExternalKOCriteria_QuickCheck
				for (ExternalScore e : getExternalScores()) {
					if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_CEG)
							&& result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)) {

						if (externalScoreCeg == 93000 || externalScoreCeg == 94000 || externalScoreCeg == 95000
								|| externalScoreCeg == 96000 || externalScoreCeg == 97000 || externalScoreCeg == 98000
								|| externalScoreCeg == 99000) {
							result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_CEG);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							result.setCheckoutCheckCD(30);
							return result;

						} else if (externalScoreCeg < 93000) {
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
						}

					} else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2B)
							&& result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)) {
						if (externalScoreCreditreform >= 351 && externalScoreCreditreform <= 600) {
							result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_SCHUFA_B2B);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							return result;
						} else if (externalScoreCreditreform < 351) {
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
						}
					}

					else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2C)
							&& result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)) {
						if (externalScoreSchufa >= 334 && externalScoreSchufa <= 600) {
							result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_SCHUFA_B2C);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							result.setCheckoutCheckCD(30);
							return result;
						} else if (externalScoreCreditreform < 334) {
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
						}
					}

					else if (!(e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_CREFO))
							&& result.getTrafficLightID().equals(Constants.TRAFFIC_LIGHT_YELLOW)) {

						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);

					}

				}
			} else {
				// CheckEternalKOCriteria

				for (ExternalScore e : getExternalScores()) {
					if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_CEG)) {

						if (externalScoreCeg == 93000 || externalScoreCeg == 94000 || externalScoreCeg == 95000
								|| externalScoreCeg == 96000) {
							result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_CEG);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							result.setCheckoutCheckCD(30);
							return result;
						}

					} else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2B)) {
						if (externalScoreCreditreform >= 500 && externalScoreCreditreform <= 600) {
							result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_SCHUFA_B2B);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							result.setCheckoutCheckCD(30);
							return result;
						}

					}

					else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2C)) {
						if (externalScoreSchufa >= 500 && externalScoreSchufa <= 600) {
							result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_SCHUFA_B2C);
							result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
							result.setCheckoutCheckCD(30);
							return result;
						}

					}

				}

			}

		}

		// TODO Set CheckoutCDBlock , CheckExternalKOCriteria
		// TODO scorecardTrafficLightID

		if (isQuickCheck) {
			if (scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_YELLOW) {
				result.setTrafficLightID(scorecardTrafficLightID);
				return result;
			}
		}

		// DetermineCreditreformScoreIndex
		if (externalScoreCreditreform > 100 && externalScoreCreditreform < 199) {
			externalCreditreformScoreIndex = 1;
		} else if (externalScoreCreditreform > 200 && externalScoreCreditreform < 299) {
			externalCreditreformScoreIndex = 2;
		} else if (externalScoreCreditreform > 300 && externalScoreCreditreform < 399) {
			externalCreditreformScoreIndex = 3;
		} else if (externalScoreCreditreform > 400 && externalScoreCreditreform < 499) {
			externalCreditreformScoreIndex = 4;
		} else if (externalScoreCreditreform == 500) {
			externalCreditreformScoreIndex = 5;
		} else if (externalScoreCreditreform == 600) {
			externalCreditreformScoreIndex = 6;
		} else if (isJobMode()) {
			externalCreditreformScoreIndex = 7;
		}

		// DetermineInternalScoreIndex
		if (result.getScore() >= 0 && result.getScore() <= 326) {
			internalScoreIndex = 1;
		} else if (result.getScore() >= 327 && result.getScore() <= 392) {
			internalScoreIndex = 2;
		} else if (result.getScore() >= 393 && result.getScore() <= 493) {
			internalScoreIndex = 3;
		} else if (result.getScore() > 493) {
			internalScoreIndex = 4;
		}
		// (age == 18) ? do_this()
		// : do_that()
		// BranchOrNewCustomer
		if (isNewCustomer) {

			// FIX ME

			
			// NewCustomerCalculateLimit
			BranchGroup branchGroupForNewCustomer=null;
			branchGroupForNewCustomer=setBranchFamily();
			if (branchGroupForNewCustomer == BranchGroup.HORECA) {
				if (internalScoreIndex == 1) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(3000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(2000);
					}

				} else if (internalScoreIndex == 2) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(2500);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(2000);
					}
				} else if (internalScoreIndex == 3) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(1000);
					}
				} else if (internalScoreIndex == 4) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(250);
					}
				} else if (internalScoreIndex == 5) {

					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(0);
					}
				} else if (internalScoreIndex == 6) {

					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(0);
					}
				}

			} else if (branchGroupForNewCustomer == BranchGroup.TRADER) {
				if (internalScoreIndex == 1) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(3000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(2000);
					}

				} else if (internalScoreIndex == 2) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(2000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(1500);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(1500);
					}
				} else if (internalScoreIndex == 3) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(750);
					}
				} else if (internalScoreIndex == 4) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(250);
					}
				} else if (internalScoreIndex == 5) {

					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(0);
					}
				} else if (internalScoreIndex == 6) {

					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(0);
					}
				}

			}

			if (branchGroupForNewCustomer == BranchGroup.SCO) {
				if (internalScoreIndex == 1) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1500);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(1000);
					}

				} else if (internalScoreIndex == 2) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(750);
					}
				} else if (internalScoreIndex == 3) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(500);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(250);
					}
				} else if (internalScoreIndex == 4) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(250);
					}
				} else if (internalScoreIndex == 5) {

					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(0);
					}
				} else if (internalScoreIndex == 6) {

					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 5) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 6) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 7 || externalCreditreformScoreIndex == 0) {
						result.setCalculatedLimit(0);
					}
				}

			}
		} else {
			// is not a new Customer
			// Check Turnover
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				// TurnoverMissingExit
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_TURNOVER_MISSING);
				return result;
			}

			// CalculateLimit
			long additionalFactor = 1;
			BigDecimal averageTurnover = null;
			long long_sellValNspL12M = getCustomerCreditData().getSellValNspl12m();
			long externalFactor = 1;
			long internalFactor = 1;

			Integer tempBigInt;
			BigDecimal tempLimit;

			averageTurnover = new BigDecimal(long_sellValNspL12M);
			averageTurnover = averageTurnover.divide(new BigDecimal(12), 2, RoundingMode.HALF_UP);

			if (internalScoreIndex == 1) {
				internalFactor = (long) 1.5;
			} else if (internalScoreIndex == 2) {
				internalFactor = (long) 1.25;
			} else if (internalScoreIndex == 3) {
				internalFactor = (long) 1;
			} else if (internalScoreIndex == 4) {
				internalFactor = (long) 0.25;
			} else if (internalScoreIndex == 7) {
				internalFactor = (long) 1;
			}

			if (externalCreditreformScoreIndex == 1) {
				externalFactor = (long) 1.3;
			} else if (externalCreditreformScoreIndex == 2) {
				externalFactor = (long) 1.2;
			} else if (externalCreditreformScoreIndex == 3) {
				externalFactor = (long) 1;
			} else if (externalCreditreformScoreIndex == 4) {
				externalFactor = (long) 0.5;
			} else {
				externalFactor = (long) 1;
			}

			if (branchGroup == BranchGroup.HORECA) {
				additionalFactor = (long) 1.5;
			} else if (branchGroup == BranchGroup.TRADER) {
				additionalFactor = (long) 1.3;
			} else if (branchGroup == BranchGroup.SCO) {
				additionalFactor = (long) 1.2;
			}

			tempLimit = averageTurnover.multiply(new BigDecimal(internalFactor * externalFactor * additionalFactor));
			tempBigInt = tempLimit.toBigInteger().intValue();

			if (tempBigInt < 100) {
				result.setCalculatedLimit(100);
			} else {
				result.setCalculatedLimit(tempBigInt);
			}
		}

		// CheckExternalSKOCriteria
		// CheckExternaSKOCriteria gateway
		if (getExternalScores() != null) {
			for (ExternalScore e : getExternalScores()) {
				if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_CEG)) {

					if (externalScoreCeg == 97000 || externalScoreCeg == 98000 || externalScoreCeg == 95000
							|| externalScoreCeg == 99000) {
						result.setRejectReasonID(Constants.REJECT_EXTERNAL_SEMI_KO_CEG);
						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
						return result;
					}

				} else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2B)) {
					if (externalScoreCreditreform >= 350 && externalScoreCreditreform <= 499) {
						result.setRejectReasonID(Constants.REJECT_EXTERNAL_SEMI_KO_SCHUFA_B2B);
						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
						return result;
					}

				}

				else if (e.getInfoBureauID().equals(EXTERNAL_BUREAU_ID_SCHUFA_B2C)) {
					if (externalScoreSchufa >= 345 && externalScoreSchufa <= 499) {
						result.setRejectReasonID(Constants.REJECT_EXTERNAL_SEMI_KO_SCHUFA_B2C);
						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
						return result;
					}

				}

			}
		}

		// CheckInternalSKOCriteria
		// CheckInternaSKOCriteria (gateway)
		if (getCustomerCreditData().getMonitionLevelMaxl12m() != null
				&& getCustomerCreditData().getMonitionLevelMaxl12m() == 2) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_DUNNING_LEVEL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;

		} else if (getCustomerCreditData().getNumDebitEntriesl12m() != null
				&& getCustomerCreditData().getNumDebitEntriesl12m() > 2) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_BAD_DEBTS);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;

		}

		else if (getCustomerMasterData().getGrossProfitMargin() != null
				&& Float.parseFloat(getCustomerMasterData().getGrossProfitMargin()) < 2.62) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_GROSS_PROFIT_MARGIN_TOO_LOW);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;

		}

		else if (getCustomerMasterData().getRequestCountL3M() != null
				&& getCustomerMasterData().getRequestCountL3M() > 3) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;

		}

		else if (getPayment() != null && getPayment().getTermName() != null
				&& Integer.parseInt(getPayment().getTermName()) > 14) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;

		}

		// BranchGroupMaxLimit
		if (branchGroup == BranchGroup.HORECA) {
			// HorecaCheckMaximumLimit

			if (getCreditRequest() != null && getCreditRequest().getRequestedLimit() > 10000) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				return result;

			}
		} else if (branchGroup == BranchGroup.TRADER) {
			// TraderCheckMaximumLimit

			if (getCreditRequest() != null && getCreditRequest().getRequestedLimit() > 10000) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				return result;
			}
		} else if (branchGroup == BranchGroup.SCO) {
			// SCOCheckMaximumLimit
			if (getCreditRequest() != null && getCreditRequest().getRequestedLimit() > 10000) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				return result;
			}
		}

		// TODO ??? scorecardTraficLightId not used
		// SetScorecardTrafficLight
		if (getCreditRequest() != null && result.getCalculatedLimit() < getCreditRequest().getRequestedLimit()
				&& scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_GREEN) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT);
		} else {

			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
		}

		return result;

	}

	private BranchGroup setBranchFamily() {
		BranchGroup branchGroup=null;
		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.HORECA;
		} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.TRADER;
		} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.SCO;
		}
		return branchGroup;
	}

	@Override
	public List<String> extractMissingFields() {

		List<String> missingFields = new ArrayList<String>();

		if (getCustomerMasterData().getCompanyName() == null) {
			missingFields.add(Constants.FIELD_COMPANY_NAME);
		}

		if (getCustomerMasterData().getLegalForm() == null) {
			missingFields.add(Constants.FIELD_LEGAL_FORM_CODE);
		}

		if (getCustomerMasterData().getCompanyFoundationDate() == null) {
			missingFields.add(Constants.FIELD_COMPANY_FOUNDATION_DATE);
		}

		if (getCustomerMasterData().getRegistrationDate() == null) {
			missingFields.add(Constants.FIELD_REGISTRATION_DATE);
		}

		if (getCustomerMasterData().getStreet() == null) {
			missingFields.add(Constants.FIELD_STREET);
		}

		if (getCustomerMasterData().getCity() == null) {
			missingFields.add(Constants.FIELD_CITY);
		}

		if (getCustomerMasterData().getHouseNumber() == null) {
			missingFields.add(Constants.FIELD_HOUSE_NO);
		}

		if (getCustomerMasterData().getZipCode() == null) {
			missingFields.add(Constants.FIELD_ZIP_CODE);
		}

		Date currentDate = new Date();
		if (getCustomerMasterData().getCompanyFoundationDate() != null
				&& Utils.numberOfMonthsSinceDate(getCustomerMasterData().getCompanyFoundationDate()) + 2 < Utils
						.numberOfMonthsSinceDate(currentDate)
				&& getCustomerMasterData().getVatNumber() == null) {
			missingFields.add(Constants.FIELD_VAT_SPEC_NO);
		}

		// If this is a one man business company additional mandatory fields are
		// needed

		if (isOneManBusiness == true) {

			if (getCustomerMasterData().getFirstName() == null) {
				missingFields.add(Constants.FIELD_OWNER_FIRST_NAME);
			}

			if (getCustomerMasterData().getLastName() == null) {
				missingFields.add(Constants.FIELD_OWNER_LAST_NAME);
			}

			if (getCustomerMasterData().getBirthday() == null) {
				missingFields.add(Constants.FIELD_BIRTH_DATE);
			}

		}

		return missingFields;
	}

}
