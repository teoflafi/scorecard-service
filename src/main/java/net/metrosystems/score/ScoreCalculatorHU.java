package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.metrosystems.domain.ExternalScore;
import net.metrosystems.domain.NeedExternalInformation;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorHU extends ScoreCalculator {

	private final List<Integer> EMPLOYEE_BRANCH_ID = Arrays.asList(new Integer[] { 66, 32, 76, 4, 99, 71, 70, null });
	private boolean isNewCustomer = false;
	private boolean isOneManBusiness = false;
	// private boolean isPublicCompany = false;
	private Integer branchMainGroupID;
	private Integer assortSectionID;
	private BranchGroup branchGroup;
	private String assortmentCode;
	private final String ASSORTMENT_CODE = "assortmentCode";
	private final String BRANCH_MAIN_GROUP_ID = "branchMainGroupID";
	// private final int EXTERNAL_BUREAU_COFACE_HU = 10;
	private final String PERC_NON_FOOD = "percNonFood";
	private final String PERC_FOOD = "percFood";
	private final String SCORECARD_BRANCH_SUBGROUP_COMPANIES_OFFICES = "CompaniesOffices";
	private final String SCORECARD_BRANCH_SUBGROUP_SERVICES = "Services";
	private final String ASSORT_SECTION_ID = "assortSectionID";
	private boolean cofaceHUFound = false;
	private long cofaceHuScore = 99;
	private final List<Integer> BLOCKING_REASON = Arrays
			.asList(new Integer[] { 9, 45, 46, 60, 61, 63, 64, 65, 75, 89, 37, 38 });
	private final List<Integer> ONE_MAN_BUSINESS_LEGALFORM = Arrays
			.asList(new Integer[] { 66, 32, 76, 4, 99, 71, 70, null, 401, 402, 404, 405, 406 });
	private final List<Integer> PUBLIC_COMPANY_BRANCH_FAMILY_ID = Arrays
			.asList(new Integer[] { 502, 701, 702, 704, 705, 706, 707, 708, 710 });
	private final List<Integer> BLOCKING_REASON_CD = Arrays
			.asList(new Integer[] { 3, 7, 8, 2, 4, 6, 31, 32, 33, 35, 40, 70, 71 });
	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays.asList(new Integer[] { 101, 102, 103, 105, 111, 121, 122,
			131, 132, 133, 134, 141, 143, 151, 152, 153, 161, 162, 163, 171, 172, 173, 190, 191, 192, });
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] { 301, 302, 303, 304, 310, 311, 312,
			313, 314, 315, 316, 317, 318, 321, 322, 331, 332, 334, 335, 341, 351, 353, 380, 389, });
	private final List<Integer> BRANCH_FAMILIY_SCO_ASSORT_ID_7 = Arrays.asList(new Integer[] { 360, 361, 362, 363, 364,
			365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 391, 392, 393, 394, 395, 396,
			397, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 721, 722,
			723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 739, 780, 781, 782, 783, 784, 785, 786, 787, 789,
			811, 812, 813, 814, 815, 816, 817, 819, 821, 822, 921, 922, 942, 952 });

	private final List<Integer> BRANCH_FAMILIY_SCO_ASSORT_ID_5 = Arrays.asList(
			new Integer[] { 501, 502, 503, 504, 505, 506, 511, 512, 513, 514, 521, 522, 523, 524, 525, 526, 527, 531,
					532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 580 });

	public ScoreCalculatorHU(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ScoreResult calculateScore() {
		ScoreResult result = new ScoreResult();
		// InitCheckoutCheckCd
		if (result.getCheckoutCheckCD() == null) {
			result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());
		}
		// InitJobMode
		// TODO

		// CheckEmployee
		if (getCustomerCreditData().getClientCd() != null && getCustomerCreditData().getClientCd().equals("MARB")
				&& EMPLOYEE_BRANCH_ID.contains(getCustomerMasterData().getBranchId())) {

			result.setRejectReasonID(Constants.REJECT_STRATEGY_EMPLOYEE);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		// CheckOneManBusiness
		if (ONE_MAN_BUSINESS_LEGALFORM.contains(getCustomerMasterData().getLegalForm())) {
			isOneManBusiness = true;
		}

		// Check missing fields
		List<String> missingFields = extractMissingFields();
		if (missingFields.size() > 0) {
			result.setMissingFields(missingFields);
			result.setRejectReasonID(Constants.REJECT_STRATEGY_MANDATORY_FIELDS_MISSING);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		// CheckCancelBlockingCodes
		if (getCustomerMasterData().getBlockingReason() != null
				&& BLOCKING_REASON_CD.contains(getCustomerMasterData().getBlockingReason())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_INACTIVE_IN_CFM);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		// CheckCheckoutCode
		if (getCustomerMasterData().getCheckoutCheck() == 30 || getCustomerMasterData().getCheckoutCheck() == 32) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_CANCEL);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		// CheckCustomerAge
		if (Utils.numberOfMonthsSinceDate(getCustomerMasterData().getBirthday()) < 216 && isOneManBusiness == false) {
			result.setRejectReasonID(Constants.REJECT_KO_AGE_UNDER_18);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setCheckoutCheckCD(30);
			return result;
		}

		// CheckAffiliatedCompany
		if (getCustomerMasterData().getTypeCode().equals("MINT")) {
			// GrantLimitToAffiliatedCompany
			if (getCreditRequest().getRequestedLimit() <= 3000000) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				result.setCalculatedLimit(Integer.parseInt(getCreditRequest().getRequestedLimit() + ""));
				return result;
			} else {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				return result;
			}
		}
		// CheckPublicCompany
		if (PUBLIC_COMPANY_BRANCH_FAMILY_ID.contains(getCustomerMasterData().getBranchFamilyId())) {
			// GrantLimitToPublicCompany
			if (getCreditRequest().getRequestedLimit() <= 300000) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				result.setCalculatedLimit(Integer.parseInt(getCreditRequest().getRequestedLimit() + ""));
				return result;
			} else {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				return result;
			}
		}

		// InitScore
		result.setScore(0);

		// SetScorecardBranchGroup
		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.HORECA;
			assortmentCode = BranchGroup.HORECA.toString();
			assortSectionID = 1;
		} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.TRADER;
			assortmentCode = BranchGroup.TRADER.toString();
			assortSectionID = 3;
		} else if (BRANCH_FAMILIY_SCO_ASSORT_ID_5.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.SCO;
			assortmentCode = SCORECARD_BRANCH_SUBGROUP_SERVICES;
			assortSectionID = 5;

		} else if (BRANCH_FAMILIY_SCO_ASSORT_ID_7.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.SCO;
			assortmentCode = SCORECARD_BRANCH_SUBGROUP_COMPANIES_OFFICES;
			assortSectionID = 7;

		}

		else {
			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		getCustomVariables().put(ASSORT_SECTION_ID, assortSectionID);
		getCustomVariables().put(ASSORTMENT_CODE, assortmentCode);

		if (getCustomerMasterData().getRegistrationDate() != null) {
			getCustomerMasterData()
					.setCustomerRelation(Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()));
		}

		if (getCustomerMasterData().getCustomerRelation() < 12) {
			// branchGroup = BranchGroup.NEW_CUSTOMER;
			isNewCustomer = true;
		}

		// SetBranchMainGroupID
		if (Arrays.asList(new Integer[] { 101, 102, 103, 105 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 10;
		} else if (getCustomerMasterData().getBranchFamilyId() == 111) {
			branchMainGroupID = 11;
		} else if (Arrays.asList(new Integer[] { 121, 122 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 12;
		} else if (Arrays.asList(new Integer[] { 131, 132, 133, 134 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 13;
		} else if (Arrays.asList(new Integer[] { 141, 143 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 14;
		} else if (Arrays.asList(new Integer[] { 151, 152, 153 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 15;
		} else if (Arrays.asList(new Integer[] { 161, 162, 163 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 16;
		} else if (Arrays.asList(new Integer[] { 171, 172, 173 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 17;
		} else if (Arrays.asList(new Integer[] { 190, 191, 192, 193 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 19;
		} else if (Arrays.asList(new Integer[] { 301, 302, 303, 304 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 30;
		} else if (Arrays.asList(new Integer[] { 310, 311, 312, 313, 314, 315, 316, 317, 318 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 31;
		} else if (Arrays.asList(new Integer[] { 321, 322 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 32;
		} else if (Arrays.asList(new Integer[] { 331, 332, 333, 334, 335 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 33;
		} else if (getCustomerMasterData().getBranchFamilyId() == 341) {
			branchMainGroupID = 34;
		} else if (Arrays.asList(new Integer[] { 351, 353 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 35;
		} else if (Arrays
				.asList(new Integer[] { 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375,
						376, 377, 378, 379, 391, 392, 393, 394, 395, 396, 397, 921, 922, 942, 952 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 36;
		} else if (Arrays.asList(new Integer[] { 389, 380, 381 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 38;
		} else if (Arrays.asList(new Integer[] { 501, 502, 503, 504, 505, 506 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 50;
		} else if (Arrays.asList(new Integer[] { 511, 512, 513, 514 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 51;
		} else if (Arrays.asList(new Integer[] { 521, 522, 523, 524, 525, 526, 527 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 52;
		} else if (Arrays.asList(new Integer[] { 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544,
				545, 546, 547, 548, 549 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 53;
		} else if (Arrays.asList(new Integer[] { 580, 581 }).contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 58;
		} else if (Arrays.asList(new Integer[] { 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 70;
		} else if (Arrays.asList(new Integer[] { 711, 712, 713, 714, 715, 716, 717 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 71;
		} else if (Arrays.asList(new Integer[] { 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 739 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 72;
		} else if (Arrays.asList(new Integer[] { 787, 780, 781, 782, 783, 784, 785, 786, 789, 788 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 78;
		} else if (Arrays.asList(new Integer[] { 811, 812, 813, 814, 815, 816, 817, 819, 821, 822 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupID = 81;
		} else {
			throw new IllegalStateException("branchMainGroup not found");
		}
		if (branchMainGroupID != null)
			getCustomVariables().put(BRANCH_MAIN_GROUP_ID, branchMainGroupID);

		//FoodAndNonFoodPRecCalculation

		if (getCustomerCreditData().getSellValNspl12m() != null && getCustomerCreditData().getSellValNspl12m() != 0
				&& getCustomerCreditData().getSellValNspnfl12m() != null
				&& getCustomerCreditData().getSellValNspfl12m() != null) {

			BigDecimal totalNonFoodSales = new BigDecimal(getCustomerCreditData().getSellValNspnfl12m());
			BigDecimal totalSales = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			Double percNonFood = totalNonFoodSales.divide(totalSales, 2, RoundingMode.HALF_UP).doubleValue() * 100;

			BigDecimal totalFoodSales = new BigDecimal(getCustomerCreditData().getSellValNspfl12m());

			Double percFood = totalFoodSales.divide(totalSales, 2, RoundingMode.HALF_UP).doubleValue() * 100;

			getCustomVariables().put(PERC_FOOD, percFood);
			getCustomVariables().put(PERC_NON_FOOD, percNonFood);
		}

		// TODO Check Branch
		// TODO ScoreCards

		ScoreRuleEngine scoreRuleEngine = new ScoreRuleEngine(getScoreRuleRepository(), getCustomerMasterData(),
				getCustomerCreditData(), getPayment(), branchGroup, getCustomVariables());
		result.setScore(scoreRuleEngine.calculateScore());

		// CheckKOBlockingCodes
		if (result.getCheckoutCheckCD() != null && result.getCheckoutCheckCD() == 30) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_KO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// CheckKODunningLevel
		if ((getCustomerCreditData().getMonitionLevelMaxl12m() >= 3)
				|| (getCustomerCreditData().getMonitionLevelMaxl12m() <= 4)) {
			result.setRejectReasonID(Constants.REJECT_KO_DUNNING_LEVEL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setCheckoutCheckCD(30);
			return result;
		}

		// CheckKOBadDebts

		if (getCustomerCreditData().getNumDebitEntriesl12m() != null
				&& getCustomerCreditData().getNumDebitEntriesl12m() > 10) {
			result.setRejectReasonID(Constants.REJECT_KO_TOO_MANY_BAD_DEBTS);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// SetCofaceHuScore

		for (ExternalScore exScore : getExternalScores()) {
			if (Utils.getAgeInHours(exScore.getRequestedAt()) < 360 && exScore.getInfoBureauID() == 10) {
				cofaceHUFound = true;
				cofaceHuScore = Long.parseLong(exScore.getScore());
			} else {
				cofaceHUFound = false;
			}
		}

		// NewCustomerCalculateLimit
		if (isNewCustomer) {
			if (branchGroup == BranchGroup.HORECA) {
				if (cofaceHuScore == 0 || cofaceHuScore == 1) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 2) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 3 || cofaceHuScore == 4) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(75000);
					}
				}
				if (cofaceHuScore == 5 || cofaceHuScore == 6) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(600000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(600000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(75000);
					}
				}
				if (cofaceHuScore == 7 || cofaceHuScore == 8 || cofaceHuScore == 9 || cofaceHuScore == 10) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(900000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(750000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(150000);
					}
				}

				if (cofaceHuScore == 99) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(75000);
					}
				}
			}

			if (branchGroup == BranchGroup.TRADER) {
				if (cofaceHuScore == 0 || cofaceHuScore == 1) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 2) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(7500);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 3 || cofaceHuScore == 4) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(225000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 5 || cofaceHuScore == 6) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(600000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(450000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(225000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(75000);
					}
				}
				if (cofaceHuScore == 7 || cofaceHuScore == 8 || cofaceHuScore == 9 || cofaceHuScore == 10) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(900000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(600000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(150000);
					}
				}

				if (cofaceHuScore == 99) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(225000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
			}

			if (branchGroup == BranchGroup.SCO) {
				if (cofaceHuScore == 0 || cofaceHuScore == 1) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 2) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(0);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 3 || cofaceHuScore == 4) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(225000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
				if (cofaceHuScore == 5 || cofaceHuScore == 6) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(225000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(75000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(75000);
					}
				}
				if (cofaceHuScore == 7 || cofaceHuScore == 8 || cofaceHuScore == 9 || cofaceHuScore == 10) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(450000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(300000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(75000);
					}
				}

				if (cofaceHuScore == 99) {
					if (result.getScore() <= 326) {
						result.setCalculatedLimit(225000);
					} else if (result.getScore() <= 392) {
						result.setCalculatedLimit(150000);
					} else if (result.getScore() <= 493) {
						result.setCalculatedLimit(750000);
					} else if (result.getScore() > 493) {
						result.setCalculatedLimit(0);
					}
				}
			}

		} else {
			// CalculateLimit

			BigDecimal additionalFactor = new BigDecimal(1);
			BigDecimal averageTurnover = new BigDecimal(0);
			BigDecimal bigDecimal_sellValNspL12M = new BigDecimal(0);
			BigDecimal internalFactor = new BigDecimal(1);
			// BigDecimal paymentFactor = new BigDecimal(1);
			int tempBigint = 0;
			BigDecimal tempLimit = new BigDecimal(0);

			// long long_sellValNspL12M =
			// getCustomerCreditData().getSellValNspl12m();
			// long externalFactor = 1;
			// long paymentTerm = 0;

			// Integer tempBigInt;

			if (getCustomerCreditData().getSellValNspl12m() == null) {
				getCustomerCreditData().setSellValNspl12m(0l);
			}
			bigDecimal_sellValNspL12M = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			averageTurnover = bigDecimal_sellValNspL12M.divide(new BigDecimal(12), 2, RoundingMode.HALF_UP);
			// paymentTerm =
			// Long.parseLong(getCustomerCreditData().getRequestedPaymentTerms());

			if (result.getScore() >= 0 && result.getScore() <= 326) {
				internalFactor = new BigDecimal(1.5 + "");
			} else if (result.getScore() > 327 && result.getScore() <= 392) {
				internalFactor = new BigDecimal(1.25 + "");
			} else if (result.getScore() > 393 && result.getScore() <= 493) {
				internalFactor = new BigDecimal(1 + "");
			} else if (result.getScore() > 493) {
				internalFactor = new BigDecimal(0.25 + "");
			}
			// if (Arrays.asList(new Integer[] { 7, 8, 9, 10
			// }).contains(cofaceHuScore)) {
			// externalFactor = (long) 1.3;
			// } else if (Arrays.asList(new Integer[] { 5, 6
			// }).contains(cofaceHuScore)) {
			// externalFactor = (long) 1.2;
			// } else if (Arrays.asList(new Integer[] { 3, 4
			// }).contains(cofaceHuScore)) {
			// externalFactor = (long) 1;
			// } else if (cofaceHuScore == 2l) {
			// externalFactor = (long) 0.5;
			// } else if (cofaceHuScore == 1l) {
			// externalFactor = (long) 0.25;
			// }

			if (branchGroup.equals(BranchGroup.HORECA)) {
				additionalFactor = new BigDecimal(1.5 + "");
			} else if (branchGroup == BranchGroup.TRADER) {
				additionalFactor = new BigDecimal(1.3 + "");
			} else if (branchGroup == BranchGroup.SCO) {
				additionalFactor = new BigDecimal(1.2 + "");
			}

			// if (paymentTerm >= 0 && paymentTerm <= 15) {
			// paymentFactor = new BigDecimal(0.5 + "");
			// } else if (paymentTerm >= 16 && paymentTerm <= 30) {
			// paymentFactor = new BigDecimal(1 + "");
			// } else if (paymentTerm >= 16 && paymentTerm <= 30) {
			// paymentFactor = new BigDecimal(1 + "");
			// } else if (paymentTerm >= 31 && paymentTerm <= 45) {
			// paymentFactor = new BigDecimal(1.5 + "");
			// } else if (paymentTerm >= 46) {
			// paymentFactor = new BigDecimal(2 + "");
			// }

			tempLimit = averageTurnover.multiply(internalFactor).multiply(additionalFactor);

			tempBigint = BigInteger.valueOf(tempLimit.toBigInteger().longValue() / 100).intValue() * 100;

			if (tempBigint < 100) {
				result.setCalculatedLimit(100);
			} else {
				result.setCalculatedLimit(tempBigint);
			}

		}

		if (!isJobMode()) {
			// CheckCofaceInformationAvailable
			if (!cofaceHUFound) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_NEED_EXTERNAL_INFO);
				NeedExternalInformation cofaceExternalInformation = new NeedExternalInformation();
				cofaceExternalInformation.setinformationBureauId(10);
				List<NeedExternalInformation> neededInformation = new ArrayList<>();
				neededInformation.add(cofaceExternalInformation);
				result.setNeededExternalInformation(neededInformation);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			}
			// CheckKOExternalScore
			if (cofaceHuScore < 3) {
				result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_COFACE_HU);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}
		}
		// CheckSKOBlockingCodes
		if (BLOCKING_REASON.contains(getCustomerMasterData().getBlockingReason())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_SKO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}

		// CheckAttachementsPresent
		if (getRequestHasAttachment()) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(0);
			return result;
		}

		// CheckCommentsPresent
		if (getRequestHasComment()) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(0);
			return result;
		}

		// CheckSKOExternalScore
		if (cofaceHuScore >= 3 && cofaceHuScore <= 5) {
			result.setRejectReasonID(Constants.REJECT_EXTERNAL_SEMI_KO_COFACE_HU);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		// CheckSKOInternalScore
		if (result.getScore() > 326) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_SCORECARD);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}

		// CheckSKOBadDebts
		if (getCustomerCreditData().getNumDebitEntriesl12m() != null
				&& getCustomerCreditData().getNumDebitEntriesl12m() >= 5) {
			result.setRejectReasonID(Constants.REJECT_KO_TOO_MANY_BAD_DEBTS);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}

		// CheckSKODunningLevel
		if (getCustomerCreditData().getMonitionLevelMaxl12m() != null
				&& getCustomerCreditData().getMonitionLevelMaxl12m() == 2) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_DUNNING_LEVEL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		// CheckRequestsInLast3Months
		if (getCustomerMasterData().getRequestCountL3M() != null && getCustomerMasterData().getRequestCountL3M() > 3) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}

		// CheckPaymentTerms
		if (getPayment() != null && getPayment().getTermName() != null
				&& Integer.parseInt(getPayment().getTermName()) > 14 && getPayment().getCreditSettleFrequencyCd()==null) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}

		// CheckMaximumLimit
		if (getCreditRequest() != null && getCreditRequest().getRequestedLimit() >= 500000) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		if (getCreditRequest() != null && result.getCalculatedLimit() < getCreditRequest().getRequestedLimit()
				&& result.getScore() <= 326) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT);
		} else {

			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
		}

		return result;

	}

	@Override
	public List<String> extractMissingFields() {

		List<String> missingFields = new ArrayList<String>();

		if (getCustomerMasterData().getCompanyName() == null) {
			missingFields.add(Constants.FIELD_COMPANY_NAME);
		}

		if (getCustomerMasterData().getLegalForm() == null) {
			missingFields.add(Constants.FIELD_LEGAL_FORM_CODE);
		}

		if (getCustomerMasterData().getCompanyFoundationDate() == null) {
			missingFields.add(Constants.FIELD_COMPANY_FOUNDATION_DATE);
		}

		if (getCustomerMasterData().getRegistrationDate() == null) {
			missingFields.add(Constants.FIELD_REGISTRATION_DATE);
		}

		if (getCustomerMasterData().getStreet() == null) {
			missingFields.add(Constants.FIELD_STREET);
		}

		if (getCustomerMasterData().getCity() == null) {
			missingFields.add(Constants.FIELD_CITY);
		}

		if (getCustomerMasterData().getHouseNumber() == null) {
			missingFields.add(Constants.FIELD_HOUSE_NO);
		}

		if (getCustomerMasterData().getZipCode() == null) {
			missingFields.add(Constants.FIELD_ZIP_CODE);
		}

		Date currentDate = new Date();
		if (getCustomerMasterData().getCompanyFoundationDate() != null
				&& Utils.numberOfMonthsSinceDate(getCustomerMasterData().getCompanyFoundationDate()) + 2 < Utils
						.numberOfMonthsSinceDate(currentDate)
				&& getCustomerMasterData().getVatNumber() == null) {
			missingFields.add(Constants.FIELD_VAT_SPEC_NO);
		}

		// If this is a one man business company additional mandatory fields are
		// needed

		if (isOneManBusiness == true) {

			if (getCustomerMasterData().getFirstName() == null) {
				missingFields.add(Constants.FIELD_OWNER_FIRST_NAME);
			}

			if (getCustomerMasterData().getLastName() == null) {
				missingFields.add(Constants.FIELD_OWNER_LAST_NAME);
			}

			if (getCustomerMasterData().getBirthday() == null) {
				missingFields.add(Constants.FIELD_BIRTH_DATE);
			}

		}

		return missingFields;
	}

}
