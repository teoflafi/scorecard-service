package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.metrosystems.domain.ExternalScore;
import net.metrosystems.domain.NeedExternalInformation;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorPL extends ScoreCalculator {

	private Boolean isOneManBusiness = false;
	// private int branchMainGroupId = 0;
	private BranchGroup branchGroup;
	private final String BEFORE_SIX_MONTH_SALES = "beforeSixMonthSales";

	// External Bureau
	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays.asList(new Integer[] { 100, 101, 102, 103, 104, 105, 106,
			125, 126, 150, 151, 152, 153, 154, 175, 176, 177, 178, 179, 180, 181, 182, 183, 200, 201, 202, 203, 225,
			226, 227, 228, 250, 251, 252, 275, 276, 277, 278, 300 });
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] { 325, 326, 327, 328, 350, 351, 352,
			353, 354, 355, 356, 357, 358, 359, 371, 372, 373, 375, 376, 400, 425, 426, 450, 451, 452, 453, 500 });
	private final List<Integer> BRANCH_FAMILIY_SCO = Arrays.asList(new Integer[] { 525, 526, 527, 528, 529, 550, 551,
			575, 576, 577, 578, 600, 601, 602, 625, 650, 651, 652, 653, 654, 655, 656, 675, 676, 677, 678, 700, 701,
			702, 725, 726, 750, 775, 776, 777, 778, 779, 800 });

	public ScoreCalculatorPL(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);
	}

	@Override
	public ScoreResult calculateScore() {
		
		ScoreResult result = new ScoreResult();
		
		// Set checkout check code
		if (getCustomerMasterData().getCheckoutCheck() >= 0 || getCustomerMasterData().getCheckoutCheck() < 0) {
			result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());
		}
		
		ScoreRequest scoreRequest = new ScoreRequest();
		if (!scoreRequest.getJobMode()) {
			scoreRequest.setJobMode(false);
		}
		
		boolean isPublicSectorCompany = false;
		
		// Check Public Sector BranchCode
		if (Arrays.asList(new String[] {"260", "270", "280", "290", "5000", "5025", "7175", "7176", "7177", "7200", "7202", "7225", "7226", "7227", "7228"}).contains(getCustomerMasterData().getBranchId())) {
			 isPublicSectorCompany = true; 
							
			if ((getCustomerMasterData().getBranchId().equals("260") && Arrays.asList(new String[] {"9", "28", "29", "30", "31", "50"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("270") 
					&& Arrays.asList(new String[] {"9", "24", "28", "29", "30", "31"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("280") 
					&& Arrays.asList(new String[] {"80", "99"}).contains(getCustomerMasterData().getLegalForm())) || (Arrays.asList(new String[] {"290", "5000", "5025"}).contains(getCustomerMasterData().getBranchId()) 
					&& Arrays.asList(new String[] {"9", "24", "28", "29", "30", "31"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("7175") 
					&& Arrays.asList(new String[] {"80", "99"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("7176") 
					&& Arrays.asList(new String[] {"2", "6", "9", "24", "28", "29", "30", "31", "1"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("7177") 
					&& Arrays.asList(new String[] {"2", "6", "24", "28", "29", "30", "31", "1"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("7200") 
					&& Arrays.asList(new String[] {"2", "9", "24", "28", "29", "30", "31", "50", "1"}).contains(getCustomerMasterData().getLegalForm())) || (getCustomerMasterData().getBranchId().equals("7202") 
					&& getCustomerMasterData().getLegalForm().equals("50")) || (Arrays.asList(new String[] {"7225", "7226"}).contains(getCustomerMasterData().getBranchId()) 
					&& Arrays.asList(new String[] {"9", "28", "29", "30", "31", "50"}).contains(getCustomerMasterData().getLegalForm())) || (Arrays.asList(new String[] {"7227", "7228"}).contains(getCustomerMasterData().getBranchId()) 
					&& Arrays.asList(new String[] {"81", "9", "28", "29", "30", "31", "50"}).contains(getCustomerMasterData().getLegalForm()))) {
				
				// Grant Limit Up To Upper Bound for PublicCompany for legalFormCDCheck = true 
				int totalRequestedPaymentTerms = Integer.valueOf((getCustomerCreditData().getRequestedPaymentTerms()));
				
				if (getCreditRequest().getRequestedLimit() <= 5000  && getCustomerCreditData().getRequestedPaymentTerms() != null && getCustomerCreditData().getRequestedPaymentTerms().length() > 0 
				 	&& (totalRequestedPaymentTerms <= 14)) {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
					result.setCalculatedLimit(getCreditRequest().getRequestedLimit().intValue());
				} else {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					if (getCreditRequest().getRequestedLimit() > 5000 || totalRequestedPaymentTerms > 14) {
						result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
						return result;
					}
				}
			
			}
			
		}
		
		// CheckInternalCompanyBranchCode
		int totalRequestedPaymentTerms = Integer.valueOf(getCustomerCreditData().getRequestedPaymentTerms());
		
		if (Arrays.asList(new String[] {"7425", "7426"}).contains(getCustomerMasterData().getBranchId()) || (getCustomerMasterData().getTypeCode().equals("MINT"))) {
			// Grant Limit Up To Upper Bound for PublicCompany
			if (getCreditRequest().getRequestedLimit() <= 5000  && getCustomerCreditData().getRequestedPaymentTerms() != null && getCustomerCreditData().getRequestedPaymentTerms().length() > 0  
				 	&& (totalRequestedPaymentTerms <= 14)) {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
					result.setCalculatedLimit(getCreditRequest().getRequestedLimit().intValue());
			} else {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				if (getCreditRequest().getRequestedLimit() > 5000 || totalRequestedPaymentTerms > 14) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
					return result;
				}
			}
		}
		
		// Check Request Interval
		if (Utils.getAgeInHours(getCustomerMasterData().getLastRequestDate()) < 1) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_REQUEST_INTEVRAL_TOO_SHORT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		
		// Check OneManBusiness
		if (getCustomerMasterData().getLegalForm().equals("99")) {
			isOneManBusiness = true;
		}	
		
		//Check mandatory fields
		//If one or more mandatory fields are missing reject the scoring
		List<String> missingFields = extractMissingFields();
		if (missingFields.size() > 0) {
			result.setMissingFields(missingFields);
			result.setRejectReasonID(Constants.REJECT_STRATEGY_MANDATORY_FIELDS_MISSING);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);		
			return result; 			
		}
		
		// Calculate before six months sale
		if (getCustomerCreditData().getSellValNspl12m() != null && getCustomerCreditData().getSellValNspl6m() != null) {
			Long beforeSixMonthSales = getCustomerCreditData().getSellValNspl12m() - getCustomerCreditData().getSellValNspl6m();
			getCustomVariables().put(BEFORE_SIX_MONTH_SALES, beforeSixMonthSales);
		}
		
		// Set Branch Main GroupId
//		if (Arrays.asList(new Integer[] {100 , 101 , 102 , 103 , 104 , 105 , 106}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 10;
//		} else if (Arrays.asList(new Integer[] {125 , 126 }).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 11;
//		} else if (Arrays.asList(new Integer[] {150 , 151 , 152 , 153 , 154}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 12;
//		} else if (Arrays.asList(new Integer[] {175 , 176 , 177 , 178 , 179 , 180 , 181 , 182 , 183}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 13;
//		} else if (Arrays.asList(new Integer[] {200 , 201 , 202 , 203}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 14;
//		} else if (Arrays.asList(new Integer[] {225 , 226 , 227 , 228}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 15;
//		} else if (Arrays.asList(new Integer[] {250 , 251 , 252}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 16;
//		} else if (Arrays.asList(new Integer[] {275 , 276 , 277 , 278}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 17;
//		} else if (Arrays.asList(new Integer[] {525 , 526 , 527 , 528 , 529}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 50;
//		} else if (Arrays.asList(new Integer[] {550 , 551}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 51;
//		} else if (Arrays.asList(new Integer[] {575 , 576 , 577 , 578}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 52;
//		} else if (Arrays.asList(new Integer[] {600 , 601 , 602}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 53;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 625) {
//			branchMainGroupId = 58;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 800) {
//			branchMainGroupId = 82;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 324) {
//			branchMainGroupId = 19;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 524) {
//			branchMainGroupId = 38;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 649) {
//			branchMainGroupId = 58;
//		} else if (Arrays.asList(new Integer[] {325 , 326 , 327 , 328}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 30;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 300) {
//			branchMainGroupId = 19;
//		} else if (Arrays.asList(new Integer[] {779 , 775 , 777 , 776 , 778}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 81;
//		} else if (Arrays.asList(new Integer[] {350 , 351 , 352 , 353 , 354 , 355 , 356 , 357 , 358 , 359}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 31;
//		} else if (Arrays.asList(new Integer[] {375 , 376}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 32;
//		} else if (Arrays.asList(new Integer[] {425 , 426}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 34;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 400) {
//			branchMainGroupId = 33;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 500) {
//			branchMainGroupId = 38;
//		} else if (Arrays.asList(new Integer[] {652 , 653 , 654 , 655 , 656}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 36;
//		} else if (Arrays.asList(new Integer[] {675 , 676 , 677 , 678}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 70;
//		} else if (Arrays.asList(new Integer[] {700 , 701 , 702}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 71;
//		} else if (Arrays.asList(new Integer[] {725 , 726}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 72;
//		} else if (Arrays.asList(new Integer[] {651 , 650}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 36;
//		} else if (getCustomerMasterData().getBranchFamilyId() == 750) {
//			branchMainGroupId = 78;
//		} else if (Arrays.asList(new Integer[] {371 , 372 , 373}).contains(getCustomerMasterData().getBranchFamilyId())) {
//			branchMainGroupId = 37;
//		}
		
//		 int assortSectionId = 0;
		
		//Set assort sectionID
//		if (Arrays.asList(new Integer[] {50 , 51 , 52 , 53 , 58}).contains(branchMainGroupId)) {
//			assortSectionId = 5;
//		} else if (Arrays.asList(new Integer[] {36 , 70 , 71 , 72 , 78 , 81 , 82}).contains(branchMainGroupId)) {
//			assortSectionId = 7;
//		} else if (Arrays.asList(new Integer[] {10 , 11 , 12 , 13 , 14 , 15 , 16 , 17 , 19}).contains(branchMainGroupId)) {
//			assortSectionId = 1;
//		} else if (Arrays.asList(new Integer[] {37 , 30 , 31 , 32 , 33 , 34 , 35 , 38}).contains(branchMainGroupId)) {
//			assortSectionId = 3;
//		}

		//Set score card BranchGroup
		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.HORECA;
		} else if(BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.TRADER;
		} else if(BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.SCO;
		} else {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
			return result;
		}
		
		if (Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()) <= 12) {
			branchGroup = BranchGroup.NEW_CUSTOMER;
		}

		//Init score:
		result.setScore(0);
		
		boolean isNewCustomer = false;
		String krdScr = "0"; 
		boolean requiredBureauInformationCrefoPlFound = false; 
		boolean requiredBureauInformationKrdFound = false;
		
		String externalScoreCreditreform = null;
		long externalCreditreformScoreIndex = 0;
		boolean isDeliveryCustomer = false;
		
//		boolean dunningLevelMissing = false;
//		boolean rec_perc_09_missing = false; 
//		boolean rec_perc_12_missing = false; 
//		boolean rec_perc_16_missing = false; 
//		
//		boolean numInvoicesL3Mmissing = false; 
//		boolean numPurchaseL3Mmissing = false;
//		boolean sellValNspL12M_missing = false;
//		
//		boolean historyTurnoverMissing = false;
//		boolean historyTurnoverMissing_5 = false;
//		boolean historyTurnoverMissing_10 = false;
//		boolean historyTurnoverMissing_15 = false;
//		boolean historyTurnoverMissing_16 = false;
//		boolean historyTurnoverMissing_34 = false;
		
//		if (branchGroup == BranchGroup.HORECA && !isNewCustomer) {
//			// HorecaCheckMissingValues
//			rec_perc_09_missing = true; 
//			historyTurnoverMissing = true;
//			
//			if (!(getCustomerCreditData().getNumPurchasesl3m() >= 0) || !(getCustomerCreditData().getNumPurchasesl3m() < 0)) {
//				numPurchaseL3Mmissing = true;
//			}
//
//			if (!(getCustomerCreditData().getNumInvoicesl3m() >= 0) || !(getCustomerCreditData().getNumInvoicesl3m() < 0)) {
//				numInvoicesL3Mmissing = true; 
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("sc_sell_val_nsp_l6m") && d.getKey() == 4) {
//					rec_perc_09_missing = false;
//				}
//			}
//			
//			if (!(getCustomerCreditData().getSellValNspl12m() <= 0) || !(getCustomerCreditData().getSellValNspl12m() > 0)) {
//				sellValNspL12M_missing = true;
//			}
//	
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey() == 5) {
//					historyTurnoverMissing = false;
//				}
//			}
//		
//		} else if (branchGroup == BranchGroup.TRADER && !isNewCustomer) {
//			// TraderCheckMissingValues
//			historyTurnoverMissing_15 = true;
//			historyTurnoverMissing_34 = true;
//			if (!(getCustomerCreditData().getSellValNspl12m() <= 0) || !(getCustomerCreditData().getSellValNspl12m() > 0)) {
//				sellValNspL12M_missing = true;
//			}
//
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey()== 7) {
//					historyTurnoverMissing_15 = false;
//				}
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey()== 10) {
//					historyTurnoverMissing_34 = false;
//				}
//			}
//			
//		} else if (branchGroup == BranchGroup.SCO && !isNewCustomer) {
//			// ScoCheckMissingValues
//			historyTurnoverMissing_10 = true;
//			historyTurnoverMissing_5 = true;
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey() == 3) {
//					historyTurnoverMissing_5 = false;
//				}
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey() == 5) {
//					historyTurnoverMissing_10 = false;
//				}
//			}
//			
//			if (!(getCustomerCreditData().getSellValNspl3m() <= 0) || !(getCustomerCreditData().getSellValNspl3m() > 0)) {
//				historyTurnoverMissing = true;
//			}
//			
//		} else if (isNewCustomer) {
//			// NewCustomerCheckMissingValues
//			if (getCustomerCreditData().getMonitionLevelMaxl12m() == null) {
//				dunningLevelMissing = true;
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("sc_sell_val_nsp_l6m") && d.getKey() == 6) {
//					rec_perc_12_missing = false;
//					return result;
//				}
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("sc_sell_val_nsp_l6m") && d.getKey() == 8) {
//					rec_perc_16_missing = false;
//					return result;
//				}
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey() == 7) {
//					historyTurnoverMissing_15 = false;
//					return result;
//				}
//			}
//			
//			for (DynamicIndicator d : getCustomerCreditData().getDynamicIndicatorList()) {
//				if (d.getIndicator().equals("behv_sell_val_nsp_l12m") && d.getKey() == 8) {
//					historyTurnoverMissing_16 = false;
//					return result;
//				}
//			}
//			
//		}
		
		//UnifiedTrafficLight
		if (result.getScore() <= 326) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
		} else if (result.getScore() >= 327 && result.getScore() <= 492) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
		} else if (result.getScore() > 492 ) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
		}
		
		long internalScoreIndex = 0;
		
		if (!isJobMode()) {
		//Determine missing external bureau
			for (ExternalScore e : getExternalScores()) {
				if ((e.getInfoBureauID()== Constants.EXTERNAL_BUREAU_KRD) && Utils.numberOfMonthsSinceDate((e.getRequestedAt())) <= 30) {
					requiredBureauInformationKrdFound = true;
					krdScr = e.getScore();
				}
				
				if (e.getInfoBureauID().equals(Constants.EXTERNAL_BUREAU_CREDITREFORM) && Utils.numberOfMonthsSinceDate((e.getRequestedAt())) <= 360) {
					requiredBureauInformationCrefoPlFound = true ;
					externalScoreCreditreform = e.getScore();	
				}
			}
			
			NeedExternalInformation needExtInfo = new NeedExternalInformation();
			
			if (!requiredBureauInformationKrdFound && !isPublicSectorCompany) {
				needExtInfo.setinformationBureauId(Constants.EXTERNAL_BUREAU_KRD);
				result.getNeededExternalInformation().add(needExtInfo);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_NEED_EXTERNAL_INFO);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			}
			
			if (!requiredBureauInformationCrefoPlFound && krdScr.equals("0")) {
				needExtInfo.setinformationBureauId(Constants.EXTERNAL_BUREAU_CREDITREFORM);
				result.getNeededExternalInformation().add(needExtInfo);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_NEED_EXTERNAL_INFO);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			}
				
			// Check External KO Criteria
			for (ExternalScore e : getExternalScores()) {
				if (e.getInfoBureauID() == Long.valueOf(Constants.EXTERNAL_BUREAU_KRD) && !isPublicSectorCompany) {
					if (result.getScore() > 0) {
						result.setRejectReasonID(Constants.REJECT_EXTERNAL_KO_KRD);
						result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
						result.setCheckoutCheckCD(30);
						return result;
					}
				}
			}
		}	
		
		if (externalScoreCreditreform.equals("A")) {
			externalCreditreformScoreIndex = 1;
		} else if (externalScoreCreditreform.equals("B")) {
			externalCreditreformScoreIndex = 2;
		} else if (externalScoreCreditreform.equals("C")) {
			externalCreditreformScoreIndex = 3;
		} else if (isJobMode()) {
			externalCreditreformScoreIndex = 4;
		}
		
		//DetermineInternalScoreIndex
		if (result.getScore() >= 0 && result.getScore() <= 326) {
			internalScoreIndex = 1;
		} else if (result.getScore() >= 327 && result.getScore() <= 392) {
			internalScoreIndex = 2;
		} else if (result.getScore() >= 393 && result.getScore() <= 493) {
			internalScoreIndex = 3;
		} else if (result.getScore() > 493) {
			internalScoreIndex = 4;
		}
			
		if (isNewCustomer) {
			//NewCustomer calculate limit
			if (branchGroup == BranchGroup.HORECA) {
				if (internalScoreIndex == 1) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(8250);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(5500);
					}
				}
				
				if (internalScoreIndex == 2) {	
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(7000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(5500);
					}
				}
				
				if (internalScoreIndex == 3) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(2750);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(1250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(2750);
					}
				}
				
				if (internalScoreIndex == 4) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1250);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(750);
					}
				}
				
				if (internalScoreIndex == 5) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(0);
					}
				}
				
				if (internalScoreIndex == 6) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(0);
					}
				}
				
			} else if (branchGroup == BranchGroup.TRADER) {
				if (internalScoreIndex == 1) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(8250);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(5500);
					}
				}
				
				if (internalScoreIndex == 2) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(5500);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(4000);
					}
				}
				
				if (internalScoreIndex == 3) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(2750);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(1250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(2000);
					}
				}
				
				if (internalScoreIndex == 4) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1250);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(750);
					}
				}
				
				if (internalScoreIndex == 5) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(0);
					}
				}
				
				if (internalScoreIndex == 6) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(0);
					}
				}
				
			} else if (branchGroup == BranchGroup.SCO) {
				if (internalScoreIndex == 1) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(4000);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(2000);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(2700);
					}
				}
				
				if (internalScoreIndex == 2) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(2750);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(1250);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(2000);
					}
				}
				
				if (internalScoreIndex == 3) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(1250);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(750);
					}
				}
				
				if (internalScoreIndex == 4) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(750);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(750);
					}
				}
				
				if (internalScoreIndex == 5) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(0);
					}
				}
				
				if (internalScoreIndex == 6) {
					if (externalCreditreformScoreIndex == 1) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 2) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 3) {
						result.setCalculatedLimit(0);
					} else if (externalCreditreformScoreIndex == 4 || externalCreditreformScoreIndex == 0 ) {
						result.setCalculatedLimit(0);
					}
				}
			}
			
		} else {
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				//TurnoverMissingExit
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_TURNOVER_MISSING);
				return result;
			} else {
				//Calculate limit
				double additionalFactor = 1;
				double averageTurnover = 0;
				double sellValNspL12M = getCustomerCreditData().getSellValNspl12m()!= null ? getCustomerCreditData().getSellValNspl12m() : 0; 
				double externalFactor = 1;
				double internalFactor =  1; 
				int tempInt = 0; 
				double tempLimit = 0;
				
				BigDecimal bigDecimal_sellValNspL12M = new BigDecimal(sellValNspL12M);
				averageTurnover = bigDecimal_sellValNspL12M.divide(new BigDecimal(12), 2, RoundingMode.HALF_UP).doubleValue(); 
				
				if (internalScoreIndex == 1) {
					internalFactor = 1.5;
				} else if (internalScoreIndex == 2) {
					internalFactor = 1.25;
				} else if (internalScoreIndex == 3) {
					internalFactor = 1;
				} else if (internalScoreIndex == 4) {
					internalFactor = 0.25;
				}
				
				if (externalCreditreformScoreIndex == 1) {
					externalFactor = 1.3;
				} else if (externalCreditreformScoreIndex == 2) {
					externalFactor = 1;
				} else {
					externalFactor = 1;
				} 
				
				if (branchGroup == BranchGroup.HORECA) {
					additionalFactor = 1.5;
				} else if (branchGroup == BranchGroup.TRADER) {
					additionalFactor = 1.2;
				} else if (branchGroup == BranchGroup.SCO) {
					additionalFactor = 1.2;
				}
				
				tempLimit = averageTurnover * internalFactor *externalFactor * additionalFactor;
				tempInt = (int) (tempLimit);
				tempInt = tempInt / 100;
				
				int calculatedLimit = 0;
				calculatedLimit = tempInt * 100;
				
				if (calculatedLimit < 100) {
					calculatedLimit = 100;
				}
				result.setCalculatedLimit(calculatedLimit);
			}
		}
		
		//Determine Delivery Customer
		//compare the getCreditProgramId with the id from MERC_CREDITREQUEST / MERC_CREDITPROGRAM table 
		if (getCreditRequest().getCreditProgramId() == 29) {
			isDeliveryCustomer = true;
		}
		
		if (isDeliveryCustomer) {
		// ApprovalCriterionForDeliveryCustomer
			boolean bureauInfoKRDFound = false;
			String crefoScore = null; 
			String krdScore = null; 
			
			for (ExternalScore e : getExternalScores()) {
				if (e.getInfoBureauID() == 6 && Utils.numberOfMonthsSinceDate(e.getRequestedAt()) <= 30) {
					bureauInfoKRDFound = true;
					krdScore = String.valueOf(result.getScore());
				} 
				if (e.getInfoBureauID() == 7 && Utils.numberOfMonthsSinceDate(e.getRequestedAt()) <= 360) {
					crefoScore = String.valueOf(result.getScore());
				}
			}
			
			int convertedRequestedPaymentTerms =Integer.valueOf((getCustomerCreditData().getRequestedPaymentTerms()));
//			If{ customerCreditData/@requestedLimit Less Than Equals 3000 
//				and customerCreditData/@companyAge Greater Than Equals 12 
//				and bureauInfoKRDFound Equals true 
//				and customerCreditData/@requestedPaymentTerms Not Equals null 
//				and Length of customerCreditData/@requestedPaymentTerms  Greater Than Equals 0 
//				and new BigInteger ( customerCreditData/@requestedPaymentTerms ) Less Than Equals 14 
//				and customerCreditData/@monitionLevelMaxL12M Less Than 3 
//				and customerCreditData/@blockingReasonCD Not In 32 
//				and krdScore Equals 0 
//				and crefoScore In A , B , null }
			
			if (getCreditRequest().getRequestedLimit() <= 3000 && getCustomerMasterData().getAgeOfCompany() > 12 
					&& bureauInfoKRDFound && getCustomerCreditData().getRequestedPaymentTerms() != null 
					&& (getCustomerCreditData().getRequestedPaymentTerms()).length() > 0
					&& (convertedRequestedPaymentTerms <= 14) 
					&& getCustomerCreditData().getMonitionLevelMaxl12m() < 3 
					&& getCustomerMasterData().getBlockingReason() != 32 && krdScore.equals("0") 
					&& (crefoScore.equals("A") || crefoScore.equals("B") || crefoScore.equals(null))) {
				result.setTrafficLightID(3);
				return result;
			}
		} 
		
		// Check Attachments And Comments
		if (getRequestHasAttachment()) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(0);
			return result;
		} else if (getRequestHasComment()) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(0);
			return result;
		}
		
		BigInteger scorecardTrafficLightID = new BigInteger("-1"); 
		//CheckKOInternalScore
		if (scorecardTrafficLightID.compareTo(BigInteger.valueOf(Constants.TRAFFIC_LIGHT_RED)) == 0) {
			result.setRejectReasonID(Constants.REJECT_KO_SCORECARD);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setCheckoutCheckCD(30);
			return result;
		}
		
		//CheckKOFromCREFOPL
		for (ExternalScore e : getExternalScores()) {
			if (e.getInfoBureauID() == Long.valueOf(Constants.EXTERNAL_BUREAU_CREDITREFORM)) {
				if (e.getScore().equals("C")) {
					result.setRejectReasonID(Constants. REJECT_EXTERNAL_KO_CEG);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
					result.setCheckoutCheckCD(30);
					return result;
				}
			}
		}
		
		//CheckSKOInternalScore
		if (scorecardTrafficLightID.compareTo(BigInteger.valueOf(Constants.TRAFFIC_LIGHT_YELLOW)) == 0) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_SCORECARD);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}		
		
		//CheckExternalSKOCriteria
		for (ExternalScore e : getExternalScores()) {
			if (e.getInfoBureauID() == Long.valueOf(Constants.EXTERNAL_BUREAU_CREDITREFORM)) {
				if (e.getScore().equals("B")) {
					result.setRejectReasonID(Constants.REJECT_EXTERNAL_SEMI_KO_CREDITREFORM_PL);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				}
			}
		}
		
		//CheckBlockingCode
		if (Arrays.asList(new Integer[] {30, 32}).contains(result.getCheckoutCheckCD())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_SKO);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());
			return result; 
		}
		
		//CheckSKODunningLevel
		if (getCustomerCreditData().getMonitionLevelMaxl12m() >= 2) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_DUNNING_LEVEL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		
		//CheckRequestsInLast3Months
		if (getCustomerMasterData().getRequestCountL3M() >= 3) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		
		//CheckPaymentTerms
		if (getCustomerCreditData().getRequestedPaymentTerms() != null && getCustomerCreditData().getRequestedPaymentTerms().length() > 0
				&& Integer.valueOf(getCustomerCreditData().getRequestedPaymentTerms()) > 14) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		
		//CheckMaximumLimit
		if (getCreditRequest().getRequestedLimit() > 10000) {
			result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			return result;
		}
		
		//SetScorecardTrafficLight
		if (result.getCalculatedLimit() < getCreditRequest().getRequestedLimit() && scorecardTrafficLightID.compareTo(BigInteger.valueOf(Constants.TRAFFIC_LIGHT_GREEN)) == 0) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT);
		} else {
			result.setTrafficLightID(scorecardTrafficLightID.intValue());
		}
		
		return result;
	}

	@Override
	public List<String> extractMissingFields() {

		List<String> missingFields = new ArrayList<String>();

		if (getCustomerMasterData().getCompanyName() == null) {
			missingFields.add(Constants.FIELD_COMPANY_NAME);
		}

		if (getCustomerMasterData().getLegalForm() == null) {
			missingFields.add(Constants.FIELD_LEGAL_FORM_CODE);
		}

		if (getCustomerMasterData().getCompanyFoundationDate() == null) {
			missingFields.add(Constants.FIELD_COMPANY_FOUNDATION_DATE);
		}

		if (getCustomerMasterData().getRegistrationDate() == null) {
			missingFields.add(Constants.FIELD_REGISTRATION_DATE);
		}

		if (getCustomerMasterData().getStreet() == null) {
			missingFields.add(Constants.FIELD_STREET);
		}

		if (getCustomerMasterData().getHouseNumber() == null) {
			missingFields.add(Constants.FIELD_HOUSE_NO);
		}

		if (getCustomerMasterData().getCity() == null) {
			missingFields.add(Constants.FIELD_CITY);
		}

		if (getCustomerMasterData().getZipCode() == null) {
			missingFields.add(Constants.FIELD_ZIP_CODE);
		}

		Date currentDate = new Date();
		if (getCustomerMasterData().getCompanyFoundationDate() != null
				&& Utils.numberOfMonthsSinceDate(getCustomerMasterData().getCompanyFoundationDate()) + 2 < Utils
						.numberOfMonthsSinceDate(currentDate)
				&& getCustomerMasterData().getVatNumber() == null) {
			missingFields.add(Constants.FIELD_VAT_SPEC_NO);
		}

		// If this is a one man business company additional mandatory fields are
		// needed
		if (isOneManBusiness == true) {

			if (getCustomerMasterData().getFirstName() == null) {
				missingFields.add(Constants.FIELD_OWNER_FIRST_NAME);
			}

			if (getCustomerMasterData().getLastName() == null) {
				missingFields.add(Constants.FIELD_OWNER_LAST_NAME);
			}

			if (getCustomerMasterData().getBirthday() == null) {
				missingFields.add(Constants.FIELD_BIRTH_DATE);
			}

		}

		return missingFields;
	}

}
