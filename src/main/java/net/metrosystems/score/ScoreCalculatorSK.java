package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.metrosystems.domain.DynamicIndicator;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorSK extends ScoreCalculator {

	private boolean isOneManBusiness = false;
	// private boolean isNewCustomer = false;
	private float percFood = 0;
	private float percNonFood = 0;
	private long internalScoreIndex = 0;
	private Integer branchMainGroupID;
	private Integer assortSectionID;
	private BigDecimal CuNoInvoicesL1M = null;
	private BigDecimal CuNoInvoicesL6M = null;
	private BigDecimal CuNoVisitL1M = null;
	private int scorecardTrafficLightID = -1;

	private final String BEFORE_SIX_MONTH_SALES = "beforeSixMonthSales";
	private final String BRANCH_MAIN_GROUP_ID = "branchMainGroupID";
	private final String CU_NO_INVOICES_L1M = "CuNoInvoicesL1M";
	private final String CU_NO_INVOICES_L6M = "CuNoInvoicesL6M";
	private final String CU_NO_VISIT_L1M = "CuNoVisitL1M";
	// private final String INTERNAL_SCORE_INDEX = "internalScoreIndex";
	private final String PERC_NON_FOOD = "percNonFood";
	private final String PERC_FOOD = "percFood";
	private final String ASSORT_SECTION_ID = "assortSectionID";
	private BranchGroup branchGroup;
	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays
			.asList(new Integer[] { 100, 101, 102, 110, 120, 130, 140, 141, 150, 151, 160, 170, 190 });
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] { 300, 301, 310, 311, 312, 313, 314,
			315, 316, 317, 318, 320, 330, 331, 332, 340, 341, 350, 351, 352, 353, 354, 370, 307, 308 });
	private final List<Integer> BRANCH_FAMILIY_SCO = Arrays
			.asList(new Integer[] { 500, 510, 520, 530, 540, 360, 700, 710, 720, 730, 810, 820 });

	private final List<Integer> FOOD_DYNAMIC_INDICATORS_KEYS = Arrays.asList(new Integer[] { 1, 3, 4, 5, 8, 10, 14 });
	private final List<Integer> NONFOOD_DYNAMIC_INDICATORS_KEYS = Arrays
			.asList(new Integer[] { 1, 3, 4, 5, 8, 10, 14 });
	private final List<Integer> REJECT_REASON_ID = Arrays.asList(new Integer[] { 7, 9, 62, 3, 35 });
	private final List<Integer> SKO_REJECT_REASON_ID = Arrays.asList(new Integer[] { 66, 32, 76, 4, 99, 71, 70, null });

	public ScoreCalculatorSK(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);

	}

	@Override
	public ScoreResult calculateScore() {
		ScoreResult result = new ScoreResult();
		// InitCheckoutCheckCd
		result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());
		if (getCustomerMasterData().getLegalForm() != null && getCustomerMasterData().getLegalForm().equals("8")) {

			if (getCreditRequest().getRequestedLimit() <= 6000 && getPayment() != null
					&& getPayment().getTermName() != null && Integer.parseInt(getPayment().getTermName()) <= 30) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				result.setCalculatedLimit(Integer.parseInt(getCreditRequest().getRequestedLimit() + ""));
				return result;
			} else if (getCreditRequest().getRequestedLimit() > 6000) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				return result;
			} else if (getPayment() != null && getPayment().getTermName() != null
					&& Integer.parseInt(getPayment().getTermName()) > 30) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
				return result;
			}
		} else {
			// Check request interval
			if (getCustomerMasterData().getLastRequestDate() != null
					&& Utils.getAgeInHours(getCustomerMasterData().getLastRequestDate()) < 1) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);

				result.setRejectReasonID(Constants.REJECT_STRATEGY_REQUEST_INTEVRAL_TOO_SHORT);
				return result;
			}

			// CheckOneManBusiness
			if (getCustomerMasterData().getLegalForm() != null
					&& (getCustomerMasterData().getLegalForm().equals("1"))) {
				isOneManBusiness = true;
				// CheckMandatoryFieldsOneManBusiness
			}
			// List<String> missingFields = extractMissingFields();
			// if (missingFields.size() > 0) {
			// result.setMissingFields(missingFields);
			// result.setRejectReasonID(Constants.REJECT_STRATEGY_MANDATORY_FIELDS_MISSING);
			// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			// return result;
			// }
			// CalculateBFSixMonthSale
			if (getCustomerCreditData().getSellValNspl12m() != null
					&& getCustomerCreditData().getSellValNspl6m() != null) {
				Long beforeSixMonthSales = getCustomerCreditData().getSellValNspl12m()
						- getCustomerCreditData().getSellValNspl6m();
				getCustomVariables().put(BEFORE_SIX_MONTH_SALES, beforeSixMonthSales);
			}
			// SetBranchMainGroupID
			if (Arrays.asList(new Integer[] { 100, 101, 102 }).contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 10;
			} else if (getCustomerMasterData().getBranchFamilyId() == 110) {
				branchMainGroupID = 11;
			} else if (getCustomerMasterData().getBranchFamilyId() == 120) {
				branchMainGroupID = 12;
			} else if (getCustomerMasterData().getBranchFamilyId() == 130) {
				branchMainGroupID = 13;
			} else if (Arrays.asList(new Integer[] { 140, 141 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 14;
			} else if (Arrays.asList(new Integer[] { 150, 151 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 14;
			} else if (getCustomerMasterData().getBranchFamilyId() == 150) {
				branchMainGroupID = 15;
			} else if (getCustomerMasterData().getBranchFamilyId() == 160) {
				branchMainGroupID = 16;
			} else if (getCustomerMasterData().getBranchFamilyId() == 170) {
				branchMainGroupID = 17;
			} else if (getCustomerMasterData().getBranchFamilyId() == 190) {
				branchMainGroupID = 19;
			} else if (Arrays.asList(new Integer[] { 300, 301 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 30;
			} else if (Arrays.asList(new Integer[] { 310, 311, 312, 313, 314, 315, 316, 317, 318 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 31;
			} else if (getCustomerMasterData().getBranchFamilyId() == 320) {
				branchMainGroupID = 32;
			} else if (Arrays.asList(new Integer[] { 330, 331, 332 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 33;
			} else if (Arrays.asList(new Integer[] { 340, 341 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 34;
			} else if (Arrays.asList(new Integer[] { 350, 351, 352, 353, 354 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 35;
			} else if (getCustomerMasterData().getBranchFamilyId() == 360) {
				branchMainGroupID = 36;
			} else if (getCustomerMasterData().getBranchFamilyId() == 370) {
				branchMainGroupID = 38;
			} else if (getCustomerMasterData().getBranchFamilyId() == 500) {
				branchMainGroupID = 50;
			} else if (getCustomerMasterData().getBranchFamilyId() == 510) {
				branchMainGroupID = 51;
			} else if (getCustomerMasterData().getBranchFamilyId() == 520) {
				branchMainGroupID = 52;
			} else if (getCustomerMasterData().getBranchFamilyId() == 530) {
				branchMainGroupID = 53;
			} else if (getCustomerMasterData().getBranchFamilyId() == 540) {
				branchMainGroupID = 58;
			} else if (getCustomerMasterData().getBranchFamilyId() == 700) {
				branchMainGroupID = 70;
			} else if (getCustomerMasterData().getBranchFamilyId() == 710) {
				branchMainGroupID = 71;
			} else if (getCustomerMasterData().getBranchFamilyId() == 720) {
				branchMainGroupID = 72;
			} else if (getCustomerMasterData().getBranchFamilyId() == 730) {
				branchMainGroupID = 78;
			} else if (getCustomerMasterData().getBranchFamilyId() == 810) {
				branchMainGroupID = 81;
			} else if (getCustomerMasterData().getBranchFamilyId() == 820) {
				branchMainGroupID = 82;
			} else if (Arrays.asList(new Integer[] { 307, 308 })
					.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchMainGroupID = 37;
			}

			if (branchMainGroupID != null)
				getCustomVariables().put(BRANCH_MAIN_GROUP_ID, branchMainGroupID);
			// SetAssortSelectionId
			if (Arrays.asList(new Integer[] { 10, 11, 12, 13, 14, 15, 16, 17, 19, 29 }).contains(branchMainGroupID)) {
				assortSectionID = 1;
			} else if (Arrays.asList(new Integer[] { 38, 30, 31, 32, 33, 34, 35, 39, 37 })
					.contains(branchMainGroupID)) {
				assortSectionID = 3;
			} else if (Arrays.asList(new Integer[] { 50, 51, 52, 53, 58, 59 }).contains(branchMainGroupID)) {
				assortSectionID = 5;
			} else if (Arrays.asList(new Integer[] { 78, 36, 70, 71, 72, 78, 81, 82, 89 })
					.contains(branchMainGroupID)) {
				assortSectionID = 7;
			}

			if (assortSectionID != null)
				getCustomVariables().put(ASSORT_SECTION_ID, assortSectionID);

			if (getCustomerMasterData().getRegistrationDate() != null) {
				getCustomerMasterData().setCustomerRelation(
						Utils.numberOfMonthsSinceDate(getCustomerMasterData().getRegistrationDate()));
			}

			// SetScorecardBranchGroup
			if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchGroup = BranchGroup.HORECA;
			} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchGroup = BranchGroup.TRADER;
			} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
				branchGroup = BranchGroup.SCO;
			} else {
				result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			}
			// if (getCustomerMasterData().getCustomerRelation() < 12) {
			// isNewCustomer = true;
			// }

			// CheckNewCustomerL3M
			if (getCustomerMasterData().getCustomerRelation() < 3) {
				if (getRequestHasAttachment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else if (getRequestHasComment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_NEW_CUSTOMER_3M);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
					return result;
				}
			}

			// CheckNewCustomerL6M
			if (getCustomerCreditData().getSellValNspl3m() == null) {
				getCustomerCreditData().setSellValNspl3m(0l);
			}
			if (getCustomerMasterData().getCustomerRelation() > 3 && getCustomerMasterData().getCustomerRelation() < 6
					&& getCustomerCreditData().getSellValNspl3m() < 2400) {
				if (getRequestHasAttachment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else if (getRequestHasComment()) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(0);
					return result;
				} else {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_NEW_CUSTOMER_3M);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
					return result;
				}
			}

			// initScore
			result.setScore(0);

			float foodTotal = 0;
			float allTotal = 0;
			float nonFoodTotal = 0;
			// FoodAndNonFoodPercCalculation
			for (DynamicIndicator indicator : getCustomerCreditData().getDynamicIndicatorList()) {
				if (indicator.getIndicator().matches("sc_sell_val_nsp_l6m")) {
					if (FOOD_DYNAMIC_INDICATORS_KEYS.contains(indicator.getKey())) {
						foodTotal += indicator.getValue();
					} else if (NONFOOD_DYNAMIC_INDICATORS_KEYS.contains(indicator.getKey())) {
						nonFoodTotal += indicator.getValue();
					}
				}

			}
			allTotal = foodTotal + nonFoodTotal;
			percFood = foodTotal / allTotal * 100;
			percNonFood = nonFoodTotal / allTotal * 100;

			getCustomVariables().put(PERC_FOOD, percFood);
			getCustomVariables().put(PERC_NON_FOOD, percNonFood);

			// CalculateScorecardAttributeMissingInMDW
			BigDecimal numInvoicesL3M = BigDecimal.ZERO;
			BigDecimal numPurchasesL3M = BigDecimal.ZERO;
			///BigDecimal sellValNspLYL3M = BigDecimal.ZERO;

			if (getCustomerCreditData().getNumInvoicesl3m() != null) {
				numInvoicesL3M = new BigDecimal(getCustomerCreditData().getNumInvoicesl3m());
				CuNoInvoicesL1M = numInvoicesL3M.divide(new BigDecimal("3"));
				CuNoInvoicesL6M = numInvoicesL3M.multiply(new BigDecimal(2));
				getCustomVariables().put(CU_NO_INVOICES_L1M, CuNoInvoicesL1M);
				getCustomVariables().put(CU_NO_INVOICES_L6M, CuNoInvoicesL6M);
			}

			if (getCustomerCreditData().getNumPurchasesfStorel3m() != null) {
				numPurchasesL3M = new BigDecimal(getCustomerCreditData().getNumPurchasesfStorel3m());
				CuNoVisitL1M = numPurchasesL3M.divide(new BigDecimal(3));
				getCustomVariables().put(CU_NO_VISIT_L1M, CuNoVisitL1M);
			}
//			if (getCustomerCreditData().getSellValNsplyl3m() != null) {
//				sellValNspLYL3M = new BigDecimal(getCustomerCreditData().getSellValNsplyl3m());
//			}

			// TODO EvaluateBranchGroup

			ScoreRuleEngine scoreRuleEngine = new ScoreRuleEngine(getScoreRuleRepository(), getCustomerMasterData(),
					getCustomerCreditData(), getPayment(), branchGroup, getCustomVariables());
			result.setScore(scoreRuleEngine.calculateScore());
			// UnifiedTrafficLight
			if (result.getScore() <= 326) {
				// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				scorecardTrafficLightID = Constants.TRAFFIC_LIGHT_GREEN;
			} else if (result.getScore() >= 327 || result.getScore() <= 392) {
				// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				scorecardTrafficLightID = Constants.TRAFFIC_LIGHT_YELLOW;
			} else if (result.getScore() > 392) {
				// result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				scorecardTrafficLightID = Constants.TRAFFIC_LIGHT_RED;
			}
			// DetermineInternalScoreIndex
			if (result.getScore() >= 0 && result.getScore() <= 326) {
				internalScoreIndex = 1;
			} else if (result.getScore() >= 327 && result.getScore() <= 392) {
				internalScoreIndex = 2;
			} else if (result.getScore() >= 393 && result.getScore() <= 493) {
				internalScoreIndex = 3;
			} else if (result.getScore() > 493) {
				internalScoreIndex = 4;
			}

			// getCustomVariables().put(CU_NO_VISIT_L1M, CuNoVisitL1M);
			// TurnoverL12M missing
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				// TurnoverMissingExit
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_TURNOVER_MISSING);
				return result;
			}

			// CalculateLimit
			BigDecimal additionalFactor = new BigDecimal(1);
			BigDecimal averageTurnover = new BigDecimal(0);
			BigDecimal bigDecimal_sellValNspL12M = new BigDecimal(0);
			BigDecimal internalFactor = new BigDecimal(1);
			int tempBigint = 0;
			BigDecimal tempLimit = new BigDecimal(0);
			if (getCustomerCreditData().getSellValNspl12m() == null) {
				getCustomerCreditData().setSellValNspl12m(0l);
			}
			bigDecimal_sellValNspL12M = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			averageTurnover = bigDecimal_sellValNspL12M.divide(new BigDecimal(12), 2, RoundingMode.HALF_UP);
			if (internalScoreIndex == 1) {
				internalFactor = new BigDecimal(1.5 + "");
			} else if (internalScoreIndex == 2) {
				internalFactor = new BigDecimal(1.25 + "");
			} else if (internalScoreIndex == 3) {
				internalFactor = new BigDecimal(1 + "");
			} else if (internalScoreIndex == 4) {
				internalFactor = new BigDecimal(0.25 + "");
			}
			if (branchGroup == BranchGroup.HORECA) {
				additionalFactor = new BigDecimal(1.5 + "");
			} else if (branchGroup == BranchGroup.TRADER) {
				additionalFactor = new BigDecimal(1.3 + "");
			} else if (branchGroup == BranchGroup.SCO) {
				additionalFactor = new BigDecimal(1.2 + "");
			}

			tempLimit = averageTurnover.multiply(internalFactor).multiply(additionalFactor);

			tempBigint = BigInteger.valueOf(tempLimit.toBigInteger().longValue() / 100).intValue() * 100;

			if (tempBigint < 100) {
				result.setCalculatedLimit(100);
			} else {
				result.setCalculatedLimit(tempBigint);
			}

			if (getRequestHasAttachment()) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setCheckoutCheckCD(0);
				return result;
			}
			if (getRequestHasComment()) {
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setCheckoutCheckCD(0);
				return result;
			}

			// CheckInternalKOCriteria
			if (getCustomerCreditData().getMonitionLevelMaxl12m() >= 2) {
				result.setRejectReasonID(Constants.REJECT_KO_DUNNING_LEVEL_TOO_HIGH);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				return result;
			} else if (scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_RED) {
				result.setRejectReasonID(Constants.REJECT_KO_SCORECARD);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}

			// CheckBlockingReasonCode
			// boolean blockingReasonCodeNull = false;
			// if (getCustomerMasterData().getCheckoutCheck() == null) {
			// blockingReasonCodeNull = true;
			// }

			if (getCustomerMasterData().getCheckoutCheck() != null && getCustomerMasterData().getCheckoutCheck() == 30
					&& REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_KO_PAY_CASH_ONLY);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}
			if (getCustomerMasterData().getCheckoutCheck() != null && getCustomerMasterData().getCheckoutCheck() == 35
					&& REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_KO_DELETED);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			}
			if (getCustomerMasterData().getCheckoutCheck() == null
					&& REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
				result.setRejectReasonID(Constants.REJECT_STRATEGY_KO_ACTIVE_CUSTOMER);
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setCheckoutCheckCD(30);
				return result;
			} else if (getCustomerMasterData().getCheckoutCheck() == null
					&& getCustomerMasterData().getBlockingReason() == null) {

				// CheckSKOBlockingReasonCode

				if (getCustomerMasterData().getCheckoutCheck() != null
						&& getCustomerMasterData().getCheckoutCheck() == 30
						&& SKO_REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_PAY_CASH_ONLY);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(30);
					return result;
				}
				if (getCustomerMasterData().getCheckoutCheck() != null
						&& getCustomerMasterData().getCheckoutCheck() == 35
						&& SKO_REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_DELETED);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(30);
					return result;
				}
				if (getCustomerMasterData().getCheckoutCheck() == null
						&& SKO_REJECT_REASON_ID.contains(getCustomerMasterData().getBlockingReason())) {
					result.setRejectReasonID(Constants.REJECT_STRATEGY_SKO_ACTIVE_CUSTOMER);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					result.setCheckoutCheckCD(30);
					return result;
				}

				// CheckInternaSKOCriteria
				if (getCustomerMasterData().getRequestCountL3M() != null
						&& getCustomerMasterData().getRequestCountL3M() > 3) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_TOO_MANY_REQUESTS_L3M);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				} else if (getPayment() != null && getPayment().getTermName() != null
						&& getPayment().getTermName().startsWith("Revolving")
						&& Integer.parseInt(getPayment().getTermName()) > 14) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				} else if (getPayment() != null && getPayment().getTermName() != null
						&& getPayment().getTermName().startsWith("Fix")
						&& Integer.parseInt(getPayment().getTermName()) > 7) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_PAYMENT_INTERVAL_TOO_HIGH);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				} else if (scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_YELLOW) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_SCORECARD);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				}

				// CheckMaximumLimit
				if (getCreditRequest().getRequestedLimit() > 4000) {
					result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
					return result;
				}

				// SetscorecardTrafficLight
				if (result.getCalculatedLimit() < getCreditRequest().getRequestedLimit()
						&& scorecardTrafficLightID == Constants.TRAFFIC_LIGHT_GREEN) {
					result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN_REDUCED_LIMIT);
				} else {
					result.setTrafficLightID(scorecardTrafficLightID);

				}
			}

		}

		return result;
	}

	@Override
	public List<String> extractMissingFields() {
		List<String> missingFields = new ArrayList<String>();

		if (getCustomerMasterData().getCompanyName() == null) {
			missingFields.add(Constants.FIELD_COMPANY_NAME);
		}

		if (getCustomerMasterData().getLegalForm() == null) {
			missingFields.add(Constants.FIELD_LEGAL_FORM_CODE);
		}

		if (getCustomerMasterData().getCompanyFoundationDate() == null) {
			missingFields.add(Constants.FIELD_COMPANY_FOUNDATION_DATE);
		}

		if (getCustomerMasterData().getRegistrationDate() == null) {
			missingFields.add(Constants.FIELD_REGISTRATION_DATE);
		}

		if (getCustomerMasterData().getStreet() == null) {
			missingFields.add(Constants.FIELD_STREET);
		}

		if (getCustomerMasterData().getCity() == null) {
			missingFields.add(Constants.FIELD_CITY);
		}

		if (getCustomerMasterData().getHouseNumber() == null) {
			missingFields.add(Constants.FIELD_HOUSE_NO);
		}

		if (getCustomerMasterData().getZipCode() == null) {
			missingFields.add(Constants.FIELD_ZIP_CODE);
		}

		Date currentDate = new Date();
		if (getCustomerMasterData().getCompanyFoundationDate() != null
				&& Utils.numberOfMonthsSinceDate(getCustomerMasterData().getCompanyFoundationDate()) + 2 < Utils
						.numberOfMonthsSinceDate(currentDate)
				&& getCustomerMasterData().getVatNumber() == null) {
			missingFields.add(Constants.FIELD_VAT_SPEC_NO);
		}

		// If this is a one man business company additional mandatory fields are
		// needed

		if (isOneManBusiness == true) {

			if (getCustomerMasterData().getFirstName() == null) {
				missingFields.add(Constants.FIELD_OWNER_FIRST_NAME);
			}

			if (getCustomerMasterData().getLastName() == null) {
				missingFields.add(Constants.FIELD_OWNER_LAST_NAME);
			}

			if (getCustomerMasterData().getBirthday() == null) {
				missingFields.add(Constants.FIELD_BIRTH_DATE);
			}

		}

		return missingFields;
	}

}
