package net.metrosystems.score;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;

public class ScoreCalculatorTR extends ScoreCalculator {
	
	private boolean jobMode = false;
//	private boolean isOneManBusiness = false;
	private int branchMainGroupId = 0;
	private BranchGroup branchGroup;
	private boolean  isNewCustomer = false;
	private final String PERC_NON_FOOD = "percNonFood";
	private final String PERC_FOOD = "percFood";
	
	private String custAssortSection = "";
	
	private final List<Integer> BRANCH_FAMILIY_HORECA = Arrays
			.asList(new Integer[] { 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125,
					126, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 207 });
	private final List<Integer> BRANCH_FAMILIY_TRADER = Arrays.asList(new Integer[] { 150, 151, 152, 153, 154, 174, 161,
			162, 163, 164, 165, 166, 167, 168, 169, 159, 160, 155, 156, 157, 158, 170, 171, 172, 173 });
	private final List<Integer> BRANCH_FAMILIY_SCO = Arrays.asList(new Integer[] { 177, 178, 182, 179, 180, 181, 175,
			176, 195, 196, 197, 198, 187, 188, 189, 183, 200, 201, 202, 203, 204, 205, 194, 199, 190, 191, 192, 193 });
	
	public ScoreCalculatorTR(ScoreRequest scoreRequest, ScoreRuleRepository scoreRuleRepository) {
		super(scoreRequest, scoreRuleRepository);
	}

	@Override
	public ScoreResult calculateScore() {
		ScoreResult result = new ScoreResult();

		// InitCheckoutCheckCD
		result.setCheckoutCheckCD(getCustomerMasterData().getCheckoutCheck());

		// InitJobMode
		if (!jobMode) {
			jobMode = false;
		}

		// Check request interval
		if (getCustomerMasterData().getLastRequestDate() != null
				&& Utils.numberOfMonthsSinceDate(getCustomerMasterData().getLastRequestDate()) < 15) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_REQUEST_INTEVRAL_TOO_SHORT);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}

		// check one man business
//		if (getCustomerMasterData().getLegalForm().equals("S")) {
//			isOneManBusiness = true;
			//then chek mandatory fields, but you'll check them anyway
//		}

		// check mandatory fields
		List<String> missingFields = extractMissingFields();
		if (missingFields.size() > 0) {
			result.setMissingFields(missingFields);
			result.setRejectReasonID(Constants.REJECT_STRATEGY_MANDATORY_FIELDS_MISSING);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
		}
		
		//Check blocked customer
		if (Arrays.asList(new Integer[] {3, 9, 7, 8, 5, 2, 4, 30, 31, 32, 46, 35, 68, 70, 111})
				.contains(getCustomerMasterData().getBlockingReason())) {
			result.setRejectReasonID(Constants.REJECT_STRATEGY_CUSTOMER_BLOCKED_FOR_CANCEL);
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			return result;
		}
		
		// Check affiliated company BranchCode
		if (getCustomerMasterData().getLegalForm().contains("M")) {
			//GrantLimitUpToUpperBoundForAffiliatedComp
			if (getCreditRequest().getRequestedLimit() >= 25000) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				result.setCalculatedLimit(getCreditRequest().getRequestedLimit().intValue());
			} else {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
			}
		}
		
		//Check public sector company.
		if (getCustomerMasterData().getLegalForm().equals("K")) {
			//GrantLimitUpToUpperBoundForPublicCompany
			if (getCreditRequest().getRequestedLimit() <= 10000) {
				result.setCalculatedLimit(getCreditRequest().getRequestedLimit().intValue());
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
				
			} else {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_REQUESTED_LIMIT_TOO_HIGH);
				return result;
			}
		}
		
		//Check KO customer already blocked
		if (getCustomerMasterData().getBlockingReason() == 6) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
			result.setRejectReasonID(Constants.REJECT_KO_BLOCK_IN_CFM);
			result.setCheckoutCheckCD(30);
			return result;
		}
		
		//Check dunning level KO
		if (getCustomerCreditData().getNoDunningL2l12m() != null && getCustomerCreditData().getNoDunningL3l12m() != null) {
			if (getCustomerCreditData().getNoDunningL2l12m() + getCustomerCreditData().getNoDunningL3l12m() > 6L) {
				result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
				result.setRejectReasonID(Constants.REJECT_SEMI_KO_DUNNING_LEVEL_TOO_HIGH);
				return result;
			}
		}
		
		 //Set Scorecard BranchGroup
		branchGroup = setBranchFamily();
		if (branchGroup == null) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setRejectReasonID(Constants.REJECT_ERROR_BRANCH_FAMILY_ID_UNKNOWN);
			return result;
		}
		
		if (getCustomerMasterData().getCustomerRelation() <= 6 && getCustomerCreditData().getSellValNspl1m() < 3000) {
			branchGroup = BranchGroup.NEW_CUSTOMER;
			 isNewCustomer = true; 
		}

		// Set BranchMain GroupId
		if (Arrays.asList(new Integer[] {133, 134, 135, 136, 137, 138, 139, 140, 141, 142})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 10;
		} else if (getCustomerMasterData().getBranchFamilyId() == 126) {
			branchMainGroupId = 11;
		} else if (Arrays.asList(new Integer[] {120, 121, 122, 123, 124, 125})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 12;
		} else if (Arrays.asList(new Integer[] {128, 129, 130})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 13;
		} else if (Arrays.asList(new Integer[] {113 , 114 , 115 , 116 , 207})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 14;
		} else if (Arrays.asList(new Integer[] {110 , 111 , 112})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 15;
		} else if (Arrays.asList(new Integer[] {131 , 132})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 16;
		} else if (Arrays.asList(new Integer[] {117 , 118 , 119})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 17;
		} else if (Arrays.asList(new Integer[] {150 , 151 , 152 , 153 , 154 , 174})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 30;
		} else if (Arrays.asList(new Integer[] {161 , 162 , 163 , 164 , 165 , 166 , 167 , 168 , 169})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 31;
		} else if (Arrays.asList(new Integer[] {159 , 160})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 32;
		} else if (Arrays.asList(new Integer[] {155 , 156 , 157 , 158 })
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 33;
		} else if (getCustomerMasterData().getBranchFamilyId() == 170) {
			branchMainGroupId = 34;
		} else if (Arrays.asList(new Integer[] {171 , 172 , 173})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 35;
		} else if (Arrays.asList(new Integer[] {177, 178})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 50;
		} else if (getCustomerMasterData().getBranchFamilyId() == 182) {
			branchMainGroupId = 51;
		} else if (Arrays.asList(new Integer[] {179 , 180 , 181})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 52;
		} else if (Arrays.asList(new Integer[] {175 , 176})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 53;
		} else if (Arrays.asList(new Integer[] {195 , 196 , 197 , 198})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 36;
		} else if (Arrays.asList(new Integer[] {187 , 188 , 189})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 70;
		} else if (getCustomerMasterData().getBranchFamilyId() == 183) {
			branchMainGroupId = 71;
		} else if (Arrays.asList(new Integer[] {200 , 201 , 202 , 203 , 204 , 205})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 72;
		} else if (Arrays.asList(new Integer[] {194 , 199})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 81;
		} else if (Arrays.asList(new Integer[] {190 , 191 , 192 , 193})
				.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchMainGroupId = 82;
		}
		
		//PROBLEM 1
//		//set customer assort section
		if (branchGroup.equals(BranchGroup.HORECA)) {
			custAssortSection = BranchGroup.HORECA.toString();
		} else if (branchGroup.equals(BranchGroup.TRADER)) {
			custAssortSection = BranchGroup.TRADER.toString();
		} else if (branchGroup.equals(BranchGroup.SCO)) {
//			custAssortSection = ???;
		
		
//			Else if
//			scorecardBranchGroup Equals SCORECARD_BRANCH_GROUP_SCO 
//			+
//			Then
//				Assign :: custAssortSection = Other 
//				+
//		<Add Else-if> <Add Else>
		
		}
		
		// FoodAndNonFoodPercCalculation
		if (getCustomerCreditData().getSellValNspl12m() != null && getCustomerCreditData().getSellValNspfl12m() != null
				&& getCustomerCreditData().getSellValNspnfl12m() != null
				&& getCustomerCreditData().getSellValNspl12m() != 0L) {
			
			BigDecimal turnoverNonFood = new BigDecimal(getCustomerCreditData().getSellValNspfl12m());
			BigDecimal turnoverFood = new BigDecimal(getCustomerCreditData().getSellValNspfl12m());
			BigDecimal bigDecimal_SellValNspL12M = new BigDecimal(getCustomerCreditData().getSellValNspl12m());
			
			Double percFood = turnoverFood.divide(bigDecimal_SellValNspL12M, 2,RoundingMode.HALF_UP).doubleValue() * 100;
			Double percNonFood = turnoverNonFood.divide(bigDecimal_SellValNspL12M, 2, RoundingMode.HALF_UP).doubleValue() * 100;

			getCustomVariables().put(PERC_FOOD, percFood);
			getCustomVariables().put(PERC_NON_FOOD, percNonFood);
		}
		
		// init score
		result.setScore(0);

		//Unified TrafficLight
		if (result.getScore() <= 326) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_GREEN);
		} else if (result.getScore() >= 327 && result.getScore() <= 492) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_YELLOW);
		} else if (result.getScore() > 492) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
		}
		
		// Internal score KO
		if (result.getScore() >= 493) {
			result.setTrafficLightID(Constants.TRAFFIC_LIGHT_RED);
			result.setRejectReasonID(Constants.REJECT_KO_SCORECARD);
			result.setCheckoutCheckCD(30);
			return result;
		}
		
		
		
		
		return result;
	}

	private BranchGroup setBranchFamily() {
		BranchGroup branchGroup = null;
		if (BRANCH_FAMILIY_HORECA.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.HORECA;
		} else if (BRANCH_FAMILIY_TRADER.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.TRADER;
		} else if (BRANCH_FAMILIY_SCO.contains(getCustomerMasterData().getBranchFamilyId())) {
			branchGroup = BranchGroup.SCO;
		}
		return branchGroup;
	}
	
	@Override
	public List<String> extractMissingFields() {
		List<String> missingFields = new ArrayList<String>();

		// CheckMandatoryFieldsLegalEntity && CheckMandatoryFieldsOneManBusiness
		if (getCustomerMasterData().getCompanyOwnerLastName() == null
				|| Integer.parseInt(getCustomerMasterData().getCompanyOwnerLastName()) == 0) {
			missingFields.add(Constants.FIELD_OWNER_LAST_NAME);
		}

		if (getCustomerMasterData().getCity() == null || Integer.parseInt(getCustomerMasterData().getCity()) == 0) {
			missingFields.add(Constants.FIELD_CITY);
		}

		if (getCustomerMasterData().getZipCode() == null
				|| Integer.parseInt(getCustomerMasterData().getZipCode()) == 0) {
			missingFields.add(Constants.FIELD_ZIP_CODE);
		}

		if (getCustomerMasterData().getStreet() == null || Integer.parseInt(getCustomerMasterData().getStreet()) == 0) {
			missingFields.add(Constants.FIELD_STREET);
		}

		if (getCustomerMasterData().getVatNumber() == null
				|| Integer.parseInt(getCustomerMasterData().getVatNumber()) == 0) {
			missingFields.add(Constants.FIELD_VAT_SPEC_NO);
		}

		if (getCustomerMasterData().getBranchId() == null
				|| Integer.parseInt(getCustomerMasterData().getBranchId()) == 0) {
			missingFields.add(Constants.FIELD_BRANGH_ID);
		}

		return missingFields;

	}

}
