package net.metrosystems.score;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import net.metrosystems.domain.CustomerCreditData;
import net.metrosystems.domain.CustomerMasterData;
import net.metrosystems.domain.DynamicIndicator;
import net.metrosystems.domain.Payment;
import net.metrosystems.domain.ScoreRule;
import net.metrosystems.domain.ScoreRuleRepository;

/**
 * Calculates the score of a customer based on the information from the
 * ScoreRule entity and on it's companyId and branchGroup
 */
public class ScoreRuleEngine {

	private final Logger log = LoggerFactory.getLogger(ScoreRuleEngine.class);

	@Autowired
	private ScoreRuleRepository scoreRuleRepository;
	private CustomerMasterData customerMasterData;
	private CustomerCreditData customerCreditData;
	private Payment payment;
	private BranchGroup branchGroup;
	private Map<String, Object> customVariables;
	private List<Long> alreadyVisitedGroups;

	public ScoreRuleEngine(ScoreRuleRepository scoreRuleRepository, CustomerMasterData customerMasterData,
			CustomerCreditData customerCreditData, Payment payment, BranchGroup branchGroup,
			Map<String, Object> customVariables) {
		this.scoreRuleRepository = scoreRuleRepository;
		this.customerMasterData = customerMasterData;
		this.customerCreditData = customerCreditData;
		this.payment = payment;
		this.branchGroup = branchGroup;
		this.customVariables = customVariables;
	}

	public int calculateScore() {
		int score = 0;
		alreadyVisitedGroups = new ArrayList<Long>();

		// Find all scoreRules from the database based on companyId and
		// branchGroup
		List<ScoreRule> scoreRules = scoreRuleRepository
				.findByCompanyIdAndBranchGroup(customerMasterData.getCompanyId(), branchGroup);

		log.info("Start calculating the score for storeNumber=" + customerMasterData.getStoreNumber()
				+ ", customerNumber=" + customerMasterData.getCustomerNumber() + ", countryCd="
				+ customerMasterData.getCountryCode() + ", companyId=" + customerMasterData.getCompanyId()
				+ ", branchGroup=" + branchGroup);

		for (ScoreRule scoreRule : scoreRules) {

			String targetValue = null;
			try {

				// If the scoreRule attribute contains a dot this means that the
				// targetValue refers to
				// an object with a particular property in the form
				// ClassName.property
				if (scoreRule.getAttribute().contains(".")) {
					String[] split = scoreRule.getAttribute().split("\\.");
					String targetClass = split[0];
					String targetAttribute = split[1];

					Object targetObject = null;
					if (targetClass.equals(CustomerMasterData.class.getSimpleName())) {
						targetObject = customerMasterData;
					} else if (targetClass.equals(CustomerCreditData.class.getSimpleName())) {
						targetObject = customerCreditData;
					} else if (targetClass.equals(Payment.class.getSimpleName())) {
						targetObject = payment;
					} else {
						throw new IllegalStateException("Unknown class provided for target object");
					}

					Object attributeValue = PropertyUtils.getSimpleProperty(targetObject, targetAttribute);
					targetValue = (attributeValue != null) ? attributeValue.toString() : null;

					// If the scoreRule attribute does not contain a dot but
					// instead has a dynamicIndicatorKey
					// this means that targetValue refers to a dynamicIndicator
				} else if (scoreRule.getDynamicIndicatorKey() != null) {
					for (DynamicIndicator dynamicIndicator : customerCreditData.getDynamicIndicatorList()) {
						if (dynamicIndicator.getIndicator().equals(scoreRule.getAttribute())
								&& dynamicIndicator.getKey().toString().equals(scoreRule.getDynamicIndicatorKey().toString())) {
							targetValue = dynamicIndicator.getValue().toString();
						}
					}

					// Else targetValue refers to a custom variable which was
					// calculated in the implementation
					// of the ScoreCalculator class and can be found in the Map
					// customVariables
				} else {
					targetValue = (customVariables.get(scoreRule.getAttribute()) != null)
							? customVariables.get(scoreRule.getAttribute()).toString() : null;
				}

				if (targetValue != null) {

					// Create String withDynamicKey for logging purposes if the
					// scoreRule has a dynamic indicator
					String withDynamicKey = (scoreRule.getDynamicIndicatorKey() != null)
							? " with dynamicIndicatorKey=" + scoreRule.getDynamicIndicatorKey() : "";

					// If the scoreRule is not a member of a group then it is
					// the only rule that needs to be checked
					if (scoreRule.getGroupId() == null) {
						if (scoreRuleMatchesTarget(targetValue, scoreRule)) {
							score += scoreRule.getScore();

							log.info("Added " + scoreRule.getScore() + " to score because " + targetValue + " "
									+ scoreRule.getOperation() + " " + scoreRule.getValue() + " for "
									+ scoreRule.getAttribute() + withDynamicKey);
						}
						// If the scoreRule is a member of a group then all of
						// the group members need to be checked for relevance
					} else if (scoreRule.getGroupId() != null
							&& !alreadyVisitedGroups.contains(scoreRule.getGroupId())) {

						alreadyVisitedGroups.add(scoreRule.getGroupId()); // Only
																			// add
																			// the
																			// score
																			// from 
																			// the
																			// group
																			// once
						boolean allScoresMatch = true;
						List<ScoreRule> groupRules = scoreRuleRepository
								.findByGroupIdAndCompanyId(scoreRule.getGroupId(), customerMasterData.getCompanyId());

						for (ScoreRule groupScoreRule : groupRules) {
							if (!scoreRuleMatchesTarget(targetValue, groupScoreRule)) {
								allScoresMatch = false;
								break; // break the for loop because one score
										// that doesn't match is enough
							}
						}

						if (allScoresMatch) {
							score += scoreRule.getScore();
							log.info("Added " + scoreRule.getScore() + " to score because " + targetValue
									+ " was in score rule group with groupId=" + scoreRule.getGroupId() + " for "
									+ scoreRule.getAttribute() + withDynamicKey);
						}
					}
				}

			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}

		log.info("Total score: " + score);
		return score;
	}

	/**
	 * Returns true if targetValue matches the given attribute value of
	 * scoreRule
	 * 
	 * @param targetValue
	 * @param scoreRule
	 * @return
	 */
	private boolean scoreRuleMatchesTarget(String targetValue, ScoreRule scoreRule) {
		boolean scoreRuleMatchesTarget = false;

		if (scoreRule.getOperation().equals(ScoreRuleOperation.EQ)) {
			if (targetValue.equals(scoreRule.getValue())) {
				scoreRuleMatchesTarget = true;
			}
 		} else if (scoreRule.getOperation().equals(ScoreRuleOperation.NE)) {
			if (!targetValue.equals(scoreRule.getValue())) {
				scoreRuleMatchesTarget = true;
			}
		} else if (scoreRule.getOperation().equals(ScoreRuleOperation.IN)) {
			if (evaluateRangeOperator(targetValue, scoreRule.getValue().split(","))) {
				scoreRuleMatchesTarget = true;
			}
		} else if (scoreRule.getOperation().equals(ScoreRuleOperation.NOT_IN)) {
			if (!evaluateRangeOperator(targetValue, scoreRule.getValue().split(","))) {
				scoreRuleMatchesTarget = true;
			}
		} else {
			if (evaluateMathOperator(Double.valueOf(targetValue), Double.valueOf(scoreRule.getValue()),
					scoreRule.getOperation())) {
				scoreRuleMatchesTarget = true;
			}
		}

		return scoreRuleMatchesTarget;
	}

	/**
	 * Returns true is x and y evaluate to true given the math operation
	 * 
	 * @param x
	 * @param y
	 * @param operation
	 * @return
	 */
	private boolean evaluateMathOperator(Number x, Number y, ScoreRuleOperation operation) {
		Boolean evaluate = false;

		if (x != null && y != null) {
			switch (operation) {
			case LT:
				evaluate = x.doubleValue() < y.doubleValue();
				break;

			case GT:
				evaluate = x.doubleValue() > y.doubleValue();
				break;

			case LE:
				evaluate = x.doubleValue() <= y.doubleValue();
				break;

			case GE:
				evaluate = x.doubleValue() >= y.doubleValue();
				break;

			default:
				break;
			}
		}

		return evaluate;
	}

	/**
	 * Returns true is needle is found in hay stack
	 * 
	 * @param needle
	 * @param hayStack
	 * @return
	 */
	private boolean evaluateRangeOperator(String needle, String[] hayStack) {
		return Arrays.asList(hayStack).contains(needle);
	}
}
