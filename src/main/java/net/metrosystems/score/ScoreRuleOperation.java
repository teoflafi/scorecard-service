package net.metrosystems.score;

public enum ScoreRuleOperation {
    /**
     * <
     */
    LT,
    /**
     * >
     */
    GT,
    /**
     * ==
     */
    EQ,
    /**
     * <=
     */
    LE,
    /**
     * >=
     */
    GE,
    /**
     * !=
     */
    NE,
    /**
     * Value is an interval with values separated by comma, example: 1,2,3
     */
    IN,
    /**
     * Value is an interval with values separated by comma, example: 1,2,3
     */
    NOT_IN,
}
