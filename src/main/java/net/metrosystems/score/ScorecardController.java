package net.metrosystems.score;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import net.metrosystems.domain.ScoreResult;
import net.metrosystems.domain.ScoreRuleRepository;
import net.metrosystems.domain.ScoreRequest;

@RestController
public class ScorecardController {

	@Autowired
	private ScoreRuleRepository scoreRuleRepository;
	
	@RequestMapping(value = "/score", method = RequestMethod.POST)
	public ScoreResult score(@RequestBody ScoreRequest scoreRequest) {

		//Needed null checks
		if (scoreRequest == null || scoreRequest.getCustomerMasterData() == null 
				|| scoreRequest.getCustomerMasterData().getCountryCode() == null 
				|| scoreRequest.getCustomerMasterData().getLegalForm() == null) {
			throw new IllegalStateException("Unable to start scoring process due to missing mandatory parameters");
		}
		
		ScoreCalculator scoreCalculator = null; //This object is going to be instantiated below depending on the company country code

		//Calculate the scoring depending on the country code 
		switch (scoreRequest.getCustomerMasterData().getCountryCode()) {
		case "BG":
			scoreCalculator = new ScoreCalculatorBG(scoreRequest, scoreRuleRepository);
			break;
		case "DE":
			scoreCalculator = new ScoreCalculatorDE(scoreRequest, scoreRuleRepository);
			break;
		case "PL":
			scoreCalculator = new ScoreCalculatorPL(scoreRequest, scoreRuleRepository);
		case "CZ":
			scoreCalculator = new ScoreCalculatorPL(scoreRequest, scoreRuleRepository);
		case "SK":
			scoreCalculator = new ScoreCalculatorPL(scoreRequest, scoreRuleRepository);
		case "HU":
			scoreCalculator = new ScoreCalculatorPL(scoreRequest, scoreRuleRepository);
		case "TK":
			scoreCalculator = new ScoreCalculatorTR(scoreRequest, scoreRuleRepository);
		default:
			throw new IllegalStateException("Unable to start scoring because an unknown country code was provided");
		}
		
		return scoreCalculator.calculateScore();
	}

	public ScoreResult calculateScore() {
		// TODO Auto-generated method stub
		return null;
	}

}
