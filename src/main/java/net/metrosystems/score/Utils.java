package net.metrosystems.score;

import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {

	/**
	 * Returns the number of months since date
	 * @param date
	 * @return number of months since date
	 */
	public static int numberOfMonthsSinceDate(Date date) {
		
		GregorianCalendar calToday = new GregorianCalendar();
		calToday.setTime(new Date());
		GregorianCalendar calBefore = new GregorianCalendar();
		calBefore.setTime(date);

		int diffMonths = calToday.get(GregorianCalendar.MONTH) - calBefore.get(GregorianCalendar.MONTH);
		diffMonths += (calToday.get(GregorianCalendar.YEAR) - calBefore.get(GregorianCalendar.YEAR)) * 12;

		return diffMonths;
	}
	
	/**
	 * Returns the number of years since date
	 * @param date
	 * @return number of years since date
	 */
	public static int numberOfYearsSinceDate(Date date) {
		
		GregorianCalendar calToday = new GregorianCalendar();
		calToday.setTime(new Date());
		GregorianCalendar calBefore = new GregorianCalendar();
		calBefore.setTime(date);

		int diffMonths = calToday.get(GregorianCalendar.YEAR) - calBefore.get(GregorianCalendar.YEAR);

		return diffMonths;
	}
	/**
	 * Returns the number of minutes since date
	 * @param date
	 * @return number of minutes since date
	 */
	public static long getAgeInHours(Date value) {
		GregorianCalendar calToday = new GregorianCalendar();
		calToday.setTime(new Date());
		GregorianCalendar calBefore = new GregorianCalendar();
		calBefore.setTime(value);

		return (calToday.getTimeInMillis() - calBefore.getTimeInMillis()) / (1000 * 60 * 60L);
	}
}
