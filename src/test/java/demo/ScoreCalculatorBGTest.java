package demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.metrosystems.ScoreCardApplication;
import net.metrosystems.domain.CreditRequest;
import net.metrosystems.domain.CustomerCreditData;
import net.metrosystems.domain.CustomerMasterData;
import net.metrosystems.domain.ExternalScore;
import net.metrosystems.domain.Payment;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreRuleRepository;
import net.metrosystems.score.ScoreCalculator;
import net.metrosystems.score.ScoreCalculatorBG;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ScoreCardApplication.class)
@WebAppConfiguration
public class ScoreCalculatorBGTest {

	private ScoreRequest request;
	private CustomerMasterData customerMD;
	private CustomerCreditData customerCD;
	private CreditRequest creditRequest;
	private Payment payment;
	private ScoreCalculator scoreCalculatorBG;

	@Autowired
	private ScoreRuleRepository srRepo;
	
	@Before
	public void setUpBeforeClass() throws Exception {
		request = mock(ScoreRequest.class);
		customerMD = mock(CustomerMasterData.class);
		customerCD = mock(CustomerCreditData.class);
		creditRequest = mock(CreditRequest.class);
		payment = mock(Payment.class);
		
		//check mandatory fields
		when(request.getCustomerMasterData()).thenReturn(customerMD);
		when(request.getCustomerCreditData()).thenReturn(customerCD);
		when(request.getCreditRequest()).thenReturn(creditRequest);
		when(request.getPayment()).thenReturn(payment);
		
		//general values
		when(customerMD.getCompanyName()).thenReturn("One Comp");
		when(customerMD.getCompanyFoundationDate()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("01.01.2014"));
		when(customerMD.getRegistrationDate()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("15.01.2012"));
		when(customerMD.getCustomerAge()).thenReturn(250);
		when(customerMD.getHouseNumber()).thenReturn("11");
		when(customerMD.getCompanyOwnerFirstName()).thenReturn("Roxana");
		when(customerMD.getCompanyOwnerLastName()).thenReturn("Valis");
		when(customerMD.getStreet()).thenReturn("St.Petersburg");
		when(customerMD.getCity()).thenReturn("Oslo");
		when(customerMD.getZipCode()).thenReturn("25");
		when(customerMD.getVatNumber()).thenReturn("10");
		
		//under 18 years
		when(customerMD.getBirthday()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("17.09.1990"));
		
		when(customerMD.getStoreNumber()).thenReturn("10");
		when(customerMD.getCustomerNumber()).thenReturn("100100");
		when(customerMD.getCountryCode()).thenReturn("BG");
		when(customerMD.getCompanyId()).thenReturn((long) 5);
//		when(creditRequest.getRequestedLimit()).thenReturn((long) 770);
		
		//listExternalScore
		List<ExternalScore> listExternalScore = new ArrayList<ExternalScore>();
		ExternalScore e = mock(ExternalScore.class);
		when(e.getInfoBureauID()).thenReturn((long) 9);
		when(e.getRequestedAt()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("17.09.2010"));
		when(e.getScore()).thenReturn("200");
		listExternalScore.add(e);
		when(request.getExternalScores()).thenReturn(listExternalScore);
		scoreCalculatorBG = new ScoreCalculatorBG(request, srRepo);
	}
	
	//01 Check employee branch code
	@Test
	public void checkEmployeeBranchCode() {
		when(customerMD.getBranchId()).thenReturn("900");
		assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("400", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
	}
	
	//02 Check mandatory fields
	@Test
	public void checkMandatoryField() throws ParseException {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getCompanyName()).thenReturn("fvdfsgds");
		when(customerMD.getVatNumber()).thenReturn(null);
		when(customerMD.getBranchId()).thenReturn("777");
		assertEquals("1" , scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("402" , scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
	}
	
	//03 Check customer age
	@Test
	public void checkCustomerAgeLE18() throws ParseException {
		//if is isOneManBusiness
		when(customerMD.getLegalForm()).thenReturn("1");
		try {
		SimpleDateFormat birthd = new SimpleDateFormat("DD.MM.YYYY");	
		Date customerBirth = birthd.parse("12.03.2000");
		when(customerMD.getBirthday()).thenReturn(customerBirth);
		assertEquals("2", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	//04 Check cancel blocking code
	@Test
	public void checkCancelBlockingCode() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBlockingReason()).thenReturn(62);
		assertEquals("426", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//05 Check KO blocking code
	@Test
	public void checkKOBlockingCode() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBlockingReason()).thenReturn(99);
		when(customerMD.getCheckoutCheck()).thenReturn(35);
		
		assertEquals("424", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("30", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
//		assertEquals("0", scoreCalculatorBG.calculateScore().getCalculatedLimit().toString());
	}

	//06 Check credit blocking code for KO
	@Test
	public void checkCreditBlokingCodeForKO() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBlockingReason()).thenReturn(0);
		when(customerMD.getCheckoutCheck()).thenReturn(35);
		assertEquals("425", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("35", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//07 Check risk category KO conditions
	@Test
	public void checkRiskCategoryKOConditions() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getSecurityIndicator()).thenReturn("MC1");
		assertEquals("427", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//08 Check customer risk category B limit under 1000 - NC (STRATEGY HAS CHANGED)
//	@Test
//	public void checkRiskCategBLimitUnder1000() {
//		when(customerMD.getSecurityIndicator()).thenReturn("B");
//		when(creditRequest.getRequestedLimit()).thenReturn((long) 900);
//		assertEquals("0", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
//		assertEquals("3", scoreCalculatorBG.calculateScore().getTrafficz
	
	//09 Check affiliated company
	@Test
	public void checkAffiliatedCompany() {
		when(customerMD.getLegalForm()).thenReturn("1");
		when(customerMD.getBranchId()).thenReturn("898");
		when(creditRequest.getRequestedLimit()).thenReturn(900L);
		assertEquals("430", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	//10 Horeca Green
	
	//11 Check risk category SKO, verify both situations/Arrays({"A", "AA", "AAA", "B", "BB", "BBB", "D", "MC3"}
			// & {"MCC", "MCO", "MDP", "MG1", "MG1", "MG2", "MG3", "MGF", "MGL", "MPC"})
	@Test
	public void checkRiskCategorySKO() {
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerMD.getLegalForm()).thenReturn("1");
		when(customerMD.getSecurityIndicator()).thenReturn("A");
		assertEquals("428", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//12 Check attachments present	
	@Test
	public void checkAttachmentsPresent() {
		when(customerMD.getLegalForm()).thenReturn("1");
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(request.getRequestHasAttachment()).thenReturn(true);
		assertTrue(request.getRequestHasAttachment());
		ScoreCalculatorBG newSCalc = new ScoreCalculatorBG(request, srRepo);
		assertEquals("104", newSCalc.calculateScore().getRejectReasonID().toString());
		assertEquals("2", newSCalc.calculateScore().getTrafficLightID().toString());
		assertEquals("0", newSCalc.calculateScore().getCheckoutCheckCD().toString());
		
	}
	
	//13 Check comments present	
	@Test
	public void checkCommentsPresent() {
		when(customerMD.getLegalForm()).thenReturn("1");
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(request.getRequestHasAttachment()).thenReturn(false);
		when(request.getRequestHasComment()).thenReturn(true);
		assertTrue(request.getRequestHasComment());
		ScoreCalculatorBG newSCalc = new ScoreCalculatorBG(request, srRepo);
		assertEquals("106", newSCalc.calculateScore().getRejectReasonID().toString());
		assertEquals("2", newSCalc.calculateScore().getTrafficLightID().toString());
		assertEquals("0", newSCalc.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//14 Check credit blocking for SKO
	@Test
	public void checkCreditBlockingForSKO() {
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getCheckoutCheck()).thenReturn(30);
		assertEquals("429", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("30", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//15 Check requests in last 3 months
	@Test
	public void checkRequestsInLast3Months() {
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getRequestCountL3M()).thenReturn(4);
		assertEquals("105", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//16 Check payment terms
	@Test
	public void checkPaymentTerms() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("17");
		assertEquals("107", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//18 Check payment types
	@Test
	public void checkPaymentTypes() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("15");
		when(payment.getCreditSettleFrequencyCd()).thenReturn(2);
		assertEquals("108", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
	}
	
	//20 Check Crefo Information Available (if !isJobMode)
	@Test
	public void checkCrefoInformationAvailable() throws ParseException {
		try{
			when(customerMD.getLegalForm()).thenReturn("1");
			when(customerMD.getBranchFamilyId()).thenReturn(150);
			//overwrite list of externalScores
			List<ExternalScore> listExternalScore = new ArrayList<ExternalScore>();
			ExternalScore extScore = mock(ExternalScore.class);
			when(extScore.getInfoBureauID()).thenReturn((long) 10);
			SimpleDateFormat requestedAtDate = new SimpleDateFormat("DD.MM.YYYY");	
			Date reqAtDate = requestedAtDate.parse("17.09.2010");
			when(extScore.getRequestedAt()).thenReturn(reqAtDate);
//			when(extScore.getScore()).thenReturn("200");
			listExternalScore.add(extScore);
			when(request.getExternalScores()).thenReturn(listExternalScore);
			scoreCalculatorBG = new ScoreCalculatorBG(request, srRepo);
			
			when(creditRequest.getRequestedLimit()).thenReturn((long) 1001);
			assertEquals("403", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
			assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
			assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	//100T Check Crefo Information Available (if isNewCustomer)
	@Test
	public void checkCrefoInformationAvailable2() throws ParseException{
		try {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		List<ExternalScore> listExternalScore = new ArrayList<ExternalScore>();
		ExternalScore extScore = mock(ExternalScore.class);
		when(extScore.getInfoBureauID()).thenReturn((long) 10);
		SimpleDateFormat requestedAtDate = new SimpleDateFormat("DD.MM.YYYY");	
		Date reqAtDate = requestedAtDate.parse("17.09.2010");
		when(extScore.getRequestedAt()).thenReturn(reqAtDate);
		when(extScore.getScore()).thenReturn("200");
		listExternalScore.add(extScore);
		when(request.getExternalScores()).thenReturn(listExternalScore);
		scoreCalculatorBG = new ScoreCalculatorBG(request, srRepo);
		
		SimpleDateFormat registrationDate = new SimpleDateFormat("DD.MM.YYYY");	
		Date regDate = registrationDate.parse("12.05.2015");
		when(customerMD.getRegistrationDate()).thenReturn(regDate);
		assertEquals("403", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	//101T Turnover MissingExit, cond: !newCustomer, getCustomerCreditData().getSellValNspl12m() == null
	@Test
	public void checkTurnoverMissingExit() {
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerCD.getSellValNspl12m()).thenReturn(null);
		assertEquals("436", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	//102T //Check public sector branch code
	@Test
	public void checkPublicSectorBranchCode() {
		when(customerMD.getLegalForm()).thenReturn("2");
		when(customerMD.getBranchFamilyId()).thenReturn(83);
		assertEquals("423", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	//103T Check KO internal score  //null at L502 (Check payment terms), cond: if (result.getScore() >= 393) {
//	@Test
//	public void checkKOInternalScore() {
//		when(customerMD.getLegalForm()).thenReturn("2");
//		when(scoreCalculatorBG.calculateScore().getScore()).thenReturn(393);
//		assertEquals("1", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
//		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
//		assertEquals("30", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
//	}	

	//104T // Check KO external score cond : if (crefoBGScore > 430)
//	@Test
//	public void checkKOExternalScore() {
//		when(customerMD.getLegalForm()).thenReturn("2");
//		when(scoreCalculatorBG.calculateScore().getScore()).thenReturn(393);
//		assertEquals("206", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
//		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
//		assertEquals("30", scoreCalculatorBG.calculateScore().getCheckoutCheckCD().toString());
//	}	
	
	//105T Check SKO external score for NewCustomer && Check GENERAL SKO external score 
			//cond: if (crefoBGScore > 100 && crefoBGScore < 200 || (crefoBGScore > 300 && crefoBGScore < 430)) {
//	@Test
//	public void checkKOExternalScore4NewCustomerAndOthers() {
//		when(customerMD.getLegalForm()).thenReturn("2");
//		when(scoreCalculatorBG.calculateScore().getScore()).thenReturn(393);
//		assertEquals("305", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
//		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
//	}
	
	//106T Check SKO internal score, cond: if (result.getScore() > 326) {
//	@Test
//	public void checkSKOInternalScore() {
//		when(customerMD.getLegalForm()).thenReturn("2");
//		when(scoreCalculatorBG.calculateScore().getScore()).thenReturn(393);
//		assertEquals("100", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
//		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
//		assertEquals("30", scoreCalculatorBG.calculateScore().getCheckoutC heckCD().toString());
//	}
	
	//107T //Check maximum limit
	@Test
	public void checkMaximumLimit() {
		when(customerMD.getBranchFamilyId()).thenReturn(150);
		when(customerMD.getLegalForm()).thenReturn("2");
		when(creditRequest.getRequestedCustLimit()).thenReturn((long) 5001);
		assertEquals("109", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
		assertEquals("2", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	// 35 Check ScoreCard branch group, if the customer is not included in any group like Horeca, 
				//Trader or SCO group, and if is not a NewCostumer too.
	@Test
	public void  scoreCardBranchGroup() throws ParseException {
		try {
			when(customerMD.getLegalForm()).thenReturn("2");
			when(customerMD.getBranchFamilyId()).thenReturn(3);
			SimpleDateFormat compDay = new SimpleDateFormat("DD.MM.YYYY");	
			Date companyFoundationDate = compDay.parse("12.03.2016");
			when(customerMD.getCompanyFoundationDate()).thenReturn(companyFoundationDate);
			
			assertEquals("900", scoreCalculatorBG.calculateScore().getRejectReasonID().toString());
			assertEquals("1", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	//Final Test
	@Test
	public void checkScoreHoreca() {
		// BranchGroup Horeca
		when(request.getJobMode()).thenReturn(true);
		when(scoreCalculatorBG.getCustomerMasterData().getBranchFamilyId()).thenReturn(100);
		// timezone 1, 2, 3, return +56
		when(customerCD.getTimezoneCd()).thenReturn((long) 2);
		// legal_form_cd 3, return +7
		when(customerMD.getLegalForm()).thenReturn("3");
		// branch_id 387, 478, 479, return +10
		when(customerMD.getBranchId()).thenReturn("387");
		//TWELVEMONTHVISIT <= 132, return +10
		when(customerCD.getNumPurchasesl12m()).thenReturn((long) 130);
		
		//DynamicIndicators //is not yet implemented
//		List<DynamicIndicator> indicatorsList = new ArrayList<>();
//		//D_113 <=10, return +30
//		DynamicIndicator d_113 = new DynamicIndicator();
//		d_113.setIndicator("behv_sell_val_nsp_l12m");
//		d_113.setKey(2l);
//		d_113.setValue(5l);
//		//d_175 <=170, return +35
//		DynamicIndicator d_175 = new DynamicIndicator();
//		d_175.setIndicator("behv_sell_val_nsp_l12m");
//		d_175.setKey(12l);
//		d_175.setValue(170l);
//		//d_176 <=80, return +163
//		DynamicIndicator d_176 = new DynamicIndicator();
//		d_176.setIndicator("behv_sell_val_nsp_l12m");
//		d_176.setKey(13l);
//		d_176.setValue(80l);
//		//d-423 <=0, return +29
//		DynamicIndicator d_423 = new DynamicIndicator();
//		d_423.setIndicator("behv_sell_val_nsp_l12m");
//		d_423.setKey(17l);
//		d_423.setValue(0l);
//		
//		indicatorsList.add(d_113);
//		indicatorsList.add(d_175);
//		indicatorsList.add(d_176);
//		indicatorsList.add(d_423);
//		when(customerCD.getDynamicIndicatorList()).thenReturn(indicatorsList);
		
		//BF_SIXMONTHSALES <=2050,  return +131 is sell_val_nsp_l12m - sell_val_nsp_l6m
		//Total Turnover  <=18200, return +72 is sell_val_nsp_l12m
		when(customerCD.getSellValNspl12m()).thenReturn(8000l);
		when(customerCD.getSellValNspl6m()).thenReturn(6300l);
		//THREEMONTHINVCOUNT  <=45, return +8
		when(customerCD.getNumInvoicesl3m()).thenReturn(45l);

		assertEquals("294", scoreCalculatorBG.calculateScore().getScore().toString());
		assertEquals("3", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	@Ignore
	@Test
	public void checkScoreTrader() {
		// BranchGroup Trader
		when(scoreCalculatorBG.getCustomerMasterData().getBranchFamilyId()).thenReturn(300);
		// legal_form_cd 3, return +41
		when(customerMD.getLegalForm()).thenReturn("3");
		// branch_id 250, return +52
		when(customerMD.getBranchId()).thenReturn("250");
		//TWELVEMONTHVISIT <=19, return +21
		when(customerCD.getNumPurchasesl12m()).thenReturn((long) 18);
		
		//...for starting calculating score need insert scripts in DB for Trader
		assertEquals("0", scoreCalculatorBG.calculateScore().getScore().toString());
		assertEquals("3", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	@Ignore
	@Test
	public void checkScoreSCO() {
		// BranchGroup Trader
		when(scoreCalculatorBG.getCustomerMasterData().getBranchFamilyId()).thenReturn(700);
		// cust_relation <=60, return +52
		when(customerMD.getCustomerRelation()).thenReturn(58);
		// mobile_phone_no
		
		// cust_assort_section_id
//		when(scoreCalculatorBG.get)
		
		// timezone <=4, return +32
		when(customerCD.getTimezoneCd()).thenReturn((long) 4);
		// legal_form_cd 3, return +47
		when(customerMD.getLegalForm()).thenReturn("3");
		
		
		// branch_id 250, return +52
		when(customerMD.getBranchId()).thenReturn("250");
		//TWELVEMONTHVISIT <=19, return +21
		when(customerCD.getNumPurchasesl12m()).thenReturn((long) 18);
		
		//...for starting calculating score need insert scripts in DB for Trader
		assertEquals("0", scoreCalculatorBG.calculateScore().getScore().toString());
		assertEquals("3", scoreCalculatorBG.calculateScore().getTrafficLightID().toString());
	}
	
	@Ignore
	@Test
	public void checkScoreNewCustomer() {
		//...for starting calculating score need insert scripts in DB for Trader
	}
	
}
