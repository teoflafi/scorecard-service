package demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.metrosystems.ScoreCardApplication;
import net.metrosystems.domain.CreditRequest;
import net.metrosystems.domain.CustomerCreditData;
import net.metrosystems.domain.CustomerMasterData;
import net.metrosystems.domain.DynamicIndicator;
import net.metrosystems.domain.Payment;
import net.metrosystems.domain.ProcessTypeEnum;
import net.metrosystems.domain.ScoreRequest;
import net.metrosystems.domain.ScoreRuleRepository;
import net.metrosystems.score.Constants;
import net.metrosystems.score.ScoreCalculator;
import net.metrosystems.score.ScoreCalculatorDE;
import net.metrosystems.score.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ScoreCardApplication.class)
@WebAppConfiguration
public class ScoreCalculatorDETest {
	private ScoreRequest request;
	private CustomerMasterData customerMD;
	private CustomerCreditData customerCD;
	private CreditRequest creditRequest;
	private Payment payment;
	// static ScoreCalculator score;
	private ScoreCalculator scoreCalculatorDE;

	@Autowired
	private ScoreRuleRepository srRepo;

	@Before
	public void setUpBeforeClass() throws Exception {
		request = Mockito.mock(ScoreRequest.class);
		customerMD = mock(CustomerMasterData.class);
		customerCD = mock(CustomerCreditData.class);
		creditRequest = mock(CreditRequest.class);
		payment = mock(Payment.class);

		// checkMandatoryFields
		when(request.getCustomerMasterData()).thenReturn(customerMD);
		when(request.getCustomerCreditData()).thenReturn(customerCD);
		when(request.getCreditRequest()).thenReturn(creditRequest);
		when(request.getPayment()).thenReturn(payment);
		// when(request.getRequestHasAttachment()).thenReturn(Boolean.TRUE);
		scoreCalculatorDE = new ScoreCalculatorDE(request, srRepo);

		when(customerMD.getCompanyName()).thenReturn("MonstersInc");
		when(customerMD.getCompanyFoundationDate()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("01.01.2014"));
		when(customerMD.getStreet()).thenReturn("Inception");
		when(customerMD.getRegistrationDate()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("01.01.2013"));
		when(customerMD.getCity()).thenReturn("GothamCity");
		when(customerMD.getHouseNumber()).thenReturn("LondonHasFallen");
		when(customerMD.getZipCode()).thenReturn("Pan");
		when(customerMD.getVatNumber()).thenReturn("101");

		// isOneManBusiness
		when(customerMD.getLegalForm()).thenReturn("3");
		when(customerMD.getBirthday()).thenReturn(new SimpleDateFormat("DD.MM.YYYY").parse("01.01.1985"));
		when(customerMD.getFirstName()).thenReturn("John");
		when(customerMD.getLastName()).thenReturn("Wick");

		// BranchGroup Horeca
		when(scoreCalculatorDE.getCustomerMasterData().getBranchFamilyId()).thenReturn(105);

		// calculateBeforeSixMonths
		when(customerCD.getSellValNspl12m()).thenReturn((long) 1586945);
		when(customerCD.getSellValNspl6m()).thenReturn((long) 15845);

		// Food and non food
		when(customerCD.getSellValNspl12m()).thenReturn((long) 1586945);
		when(customerCD.getSellValNspl12m()).thenReturn((long) 15865);

		// scoreRuleEngine
		// ScoreRuleEngine scoreRuleEngine=mock(ScoreRuleEngine.class);
		// when(scoreRuleEngine.calculateScore()).thenReturn(264);

		when(customerMD.getStoreNumber()).thenReturn("6");
		when(customerMD.getCustomerNumber()).thenReturn("701120");
		when(customerMD.getCountryCode()).thenReturn("DE");
		when(customerMD.getCompanyId()).thenReturn((long) 1);
		when(creditRequest.getRequestedLimit()).thenReturn((long) 770);

		// only luck can save me now
		when(customerCD.getTimezoneCd()).thenReturn((long) 1);

	}

	@Test
	public void checkSrRepo() {
		assertNotNull(srRepo);
	}

	@Test()
	public void checkEmployee() {
		when(customerMD.getBranchId()).thenReturn("990");
		assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("400", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkMandatoryField() throws ParseException {
		when(customerMD.getCompanyName()).thenReturn(null);
		assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("402", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test
	public void ageOfCustomerLt18() {
		SimpleDateFormat sdf = new SimpleDateFormat("DD.MM.YYYY");
		try {
			Date customerBirthday = sdf.parse("11.01.2000");
			when(request.getCustomerMasterData().getBirthday()).thenReturn(customerBirthday);
			assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
			assertEquals("2", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	// Check blocking reason code
	@Test()
	public void checkBlockingReason() {
		when(customerMD.getBlockingReason()).thenReturn(9);
		assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("405", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test
	public void checkDunningLevel() {
		// is Not oneManBusiness
		// not internal company
		when(customerMD.getLegalForm()).thenReturn("342");
		when(customerCD.getMonitionLevelMaxl12m()).thenReturn((long) 3);
		assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("3", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());

	}

	@Test
	public void checkCustomerIsBlock() {
		// doesn't have attachments and comments
		when(request.getRequestHasAttachment()).thenReturn(false);
		when(request.getRequestHasComment()).thenReturn(false);
		when(customerMD.getCheckoutCheck()).thenReturn(30);
		assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("4", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());

	}

	@Test
	public void checkCountOfBadDebts() {
		// doesn't have attachments and comments
		when(request.getRequestHasAttachment()).thenReturn(false);
		when(request.getRequestHasComment()).thenReturn(false);
		when(customerCD.getNumDebitEntriesl12m()).thenReturn((long) 10);
		assertEquals("1", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals(Constants.REJECT_KO_TOO_MANY_BAD_DEBTS + "",
				scoreCalculatorDE.calculateScore().getRejectReasonID().toString());

	}

	@Test()
	public void checkAttachementsArePresent() {
		// attachments
		when(request.getRequestHasAttachment()).thenReturn(true);
		assertTrue(request.getRequestHasAttachment());
		ScoreCalculatorDE newScoreCalculator = new ScoreCalculatorDE(request, srRepo);
		assertEquals("2", newScoreCalculator.calculateScore().getTrafficLightID().toString());
		assertEquals(Constants.REJECT_SEMI_KO_ATTACHMENTS_PRESENT + "",
				newScoreCalculator.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkCommentsArePresent() {
		// attachments and comments
		when(request.getRequestHasComment()).thenReturn(true);
		// assertTrue(request.getRequestHasAttachment());
		ScoreCalculatorDE newScoreCalculator = new ScoreCalculatorDE(request, srRepo);
		assertEquals("2", newScoreCalculator.calculateScore().getTrafficLightID().toString());
		assertEquals(Constants.REJECT_SEMI_KO_COMMENTS_PRESENT + "",
				newScoreCalculator.calculateScore().getRejectReasonID().toString());

	}

	@Test()
	public void checkTooManyRequest() {
		when(customerMD.getRequestCountL3M()).thenReturn(4);
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("105", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkPaymentIntervalTooHigh() {
		when(payment.getTermName()).thenReturn("15");
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("107", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkSemiKORequestedLimitTooHigh() {
		// branchGroupHORECA
		when(creditRequest.getRequestedLimit()).thenReturn(10001l);
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("109", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());

		// branchGroupTrader
		when(customerMD.getBranchFamilyId()).thenReturn(331);
		when(creditRequest.getRequestedLimit()).thenReturn(10001l);
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("109", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());

		// branchGroupTrader
		when(customerMD.getBranchFamilyId()).thenReturn(580);
		when(creditRequest.getRequestedLimit()).thenReturn(10001l);
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("109", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkTurnoverMissing() {
		when(customerCD.getSellValNspl12m()).thenReturn(null);
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("436", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkGrossProfitMarginTooLow() {
		when(customerMD.getGrossProfitMargin()).thenReturn("1.5");
		assertEquals("2", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("103", scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
	}

	@Test()
	public void checkTraderScorecard() {

		// CustomerMasterData cmd=mock(CustomerMasterData.class);
		when(request.getJobMode()).thenReturn(true);
		Date olddate = new Date();
		Date newDate = DateUtils.addMonths(olddate, -36);
		// age of Company
		when(customerMD.getCompanyFoundationDate()).thenReturn(newDate);
		int numberOfMonthsSinceDate = Utils.numberOfMonthsSinceDate(customerMD.getCompanyFoundationDate());
		when(customerMD.getAgeOfCompany()).thenReturn(numberOfMonthsSinceDate);
		// when(customerMD.getRegistrationDate()).thenReturn(newDate);
		// cust_relation
		when(customerMD.getCustomerRelation()).thenReturn(21);
		// legalFormCd
		when(customerMD.getLegalForm()).thenReturn("11");
		// branchFamilyId
		when(customerMD.getBranchFamilyId()).thenReturn(312);
		// oneMonthVisit 2<=2 21
		when(customerCD.getNumPurchasesl1m()).thenReturn((long) 2);
		// sellValNspL1M
		when(customerCD.getSellValNspl1m()).thenReturn(1l);
		// sellValNspNFL12M
		when(customerCD.getSellValNspnfl12m()).thenReturn(32l);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);

		newDate = DateUtils.addMonths(olddate, -2);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		// selValNspL12M
		when(customerCD.getSellValNspl12m()).thenReturn(0l);
		// grossProfitMargin
		when(customerMD.getGrossProfitMargin()).thenReturn("4");
		// requestedLimit
		when(creditRequest.getRequestedLimit()).thenReturn(200l);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("13");
		when(customerCD.getTimezoneCd()).thenReturn(3l);
		// when(customerMD.getBranch).thenReturn("362");
		when(customerMD.getBranchId()).thenReturn("362");
		when(customerCD.getSellValNspl6m()).thenReturn(860l);
		when(request.getProcessType()).thenReturn(null);

		assertEquals("3", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("203", scoreCalculatorDE.calculateScore().getScore().toString());
		assertEquals("100", scoreCalculatorDE.calculateScore().getCalculatedLimit().toString());
	}

	@Test()
	public void checkHorecaScorecard2() {

		// CustomerMasterData cmd=mock(CustomerMasterData.class);
		when(request.getJobMode()).thenReturn(true);
		Date olddate = new Date();
		Date newDate = DateUtils.addMonths(olddate, -36);
		// age of Company
		when(customerMD.getCompanyFoundationDate()).thenReturn(newDate);
		int numberOfMonthsSinceDate = Utils.numberOfMonthsSinceDate(customerMD.getCompanyFoundationDate());
		when(customerMD.getAgeOfCompany()).thenReturn(numberOfMonthsSinceDate);
		// when(customerMD.getRegistrationDate()).thenReturn(newDate);
		// cust_relation
		when(customerMD.getCustomerRelation()).thenReturn(21);
		// legalFormCd
		when(customerMD.getLegalForm()).thenReturn("13");
		// branchFamilyId
		when(customerMD.getBranchFamilyId()).thenReturn(122);
		// oneMonthVisit 2<=2 21
		when(customerCD.getNumPurchasesl1m()).thenReturn((long) 2);
		// sellValNspL1M
		when(customerCD.getSellValNspl1m()).thenReturn(1l);
		// sellValNspNFL12M
		// TODO not right
		when(customerCD.getSellValNspnfl12m()).thenReturn(32l);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);

		newDate = DateUtils.addMonths(olddate, -2);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		// selValNspL12M
		when(customerCD.getSellValNspl12m()).thenReturn(0l);
		// grossProfitMargin
		when(customerMD.getGrossProfitMargin()).thenReturn("4");
		// requestedLimit
		when(creditRequest.getRequestedLimit()).thenReturn(200l);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("13");
		when(customerCD.getTimezoneCd()).thenReturn(3l);
		// when(customerMD.getBranch).thenReturn("362");
		when(customerMD.getBranchId()).thenReturn("362");
		when(customerCD.getSellValNspl6m()).thenReturn(860l);
		when(request.getProcessType()).thenReturn(null);

		assertEquals("3", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("332", scoreCalculatorDE.calculateScore().getScore().toString());
		assertEquals("100", scoreCalculatorDE.calculateScore().getCalculatedLimit().toString());

	}

	@Test()
	public void checkHorecaScorecard() {

		// CustomerMasterData cmd=mock(CustomerMasterData.class);
		when(request.getJobMode()).thenReturn(true);
		Date olddate = new Date();
		Date newDate = DateUtils.addMonths(olddate, -36);
		// age of Company
		when(customerMD.getCompanyFoundationDate()).thenReturn(newDate);
		int numberOfMonthsSinceDate = Utils.numberOfMonthsSinceDate(customerMD.getCompanyFoundationDate());
		when(customerMD.getAgeOfCompany()).thenReturn(numberOfMonthsSinceDate);
		// when(customerMD.getRegistrationDate()).thenReturn(newDate);
		// cust_relation
		when(customerMD.getCustomerRelation()).thenReturn(21);
		// legalFormCd
		when(customerMD.getLegalForm()).thenReturn("5");
		// branchFamilyId
		when(customerMD.getBranchFamilyId()).thenReturn(122);
		// oneMonthVisit 2<=2 21
		when(customerCD.getNumPurchasesl1m()).thenReturn((long) 2);
		// sellValNspL1M
		when(customerCD.getSellValNspl1m()).thenReturn(1l);
		// sellValNspNFL12M
		when(customerCD.getSellValNspnfl12m()).thenReturn(32l);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);

		newDate = DateUtils.addMonths(olddate, -2);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		// selValNspL12M
		when(customerCD.getSellValNspl12m()).thenReturn(0l);
		// grossProfitMargin
		when(customerMD.getGrossProfitMargin()).thenReturn("4");
		// requestedLimit
		when(creditRequest.getRequestedLimit()).thenReturn(200l);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("13");
		when(customerCD.getTimezoneCd()).thenReturn(3l);
		// when(customerMD.getBranch).thenReturn("362");
		when(customerMD.getBranchId()).thenReturn("362");
		when(customerCD.getSellValNspl6m()).thenReturn(860l);
		when(request.getProcessType()).thenReturn(null);

		assertEquals("3", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("0", scoreCalculatorDE.calculateScore().getScore().toString());
		assertEquals("200", scoreCalculatorDE.calculateScore().getCalculatedLimit().toString());

	}

	@Test()
	public void checkSCOScorecard() {

		// CustomerMasterData cmd=mock(CustomerMasterData.class);
		when(request.getJobMode()).thenReturn(true);
		Date olddate = new Date();

		Date newDate = DateUtils.addMonths(olddate, -36);
		// age of Company
		when(customerMD.getCompanyFoundationDate()).thenReturn(newDate);
		int numberOfMonthsSinceDate = Utils.numberOfMonthsSinceDate(customerMD.getCompanyFoundationDate());
		when(customerMD.getAgeOfCompany()).thenReturn(numberOfMonthsSinceDate);
		// when(customerMD.getRegistrationDate()).thenReturn(newDate);
		// cust_relation
		when(customerMD.getCustomerRelation()).thenReturn(21);
		// legalFormCd
		when(customerMD.getLegalForm()).thenReturn("11");
		// branchFamilyId
		when(customerMD.getBranchFamilyId()).thenReturn(520);
		// oneMonthVisit 2<=2 21
		when(customerCD.getNumPurchasesl1m()).thenReturn((long) 2);
		// sellValNspL1M
		when(customerCD.getSellValNspl1m()).thenReturn(1l);
		// sellValNspNFL12M
		when(customerCD.getSellValNspnfl12m()).thenReturn(32l);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);

		newDate = DateUtils.addMonths(olddate, -2);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		// selValNspL12M
		when(customerCD.getSellValNspl12m()).thenReturn(0l);
		// grossProfitMargin
		when(customerMD.getGrossProfitMargin()).thenReturn("4");
		// requestedLimit
		when(creditRequest.getRequestedLimit()).thenReturn(200l);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("13");
		when(customerCD.getTimezoneCd()).thenReturn(3l);
		// when(customerMD.getBranch).thenReturn("362");
		when(customerMD.getBranchId()).thenReturn("362");
		when(customerCD.getSellValNspl6m()).thenReturn(860l);
		when(request.getProcessType()).thenReturn(null);

		assertEquals("3", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("236", scoreCalculatorDE.calculateScore().getScore().toString());
		assertEquals("100", scoreCalculatorDE.calculateScore().getCalculatedLimit().toString());

	}

	@Test()
	public void checkNewCustomerScorecard() {

		// CustomerMasterData cmd=mock(CustomerMasterData.class);
		when(request.getJobMode()).thenReturn(true);
		Date olddate = new Date();

		Date newDate = DateUtils.addMonths(olddate, -36);
		// age of Company
		when(customerMD.getCompanyFoundationDate()).thenReturn(newDate);
		int numberOfMonthsSinceDate = Utils.numberOfMonthsSinceDate(customerMD.getCompanyFoundationDate());
		when(customerMD.getAgeOfCompany()).thenReturn(numberOfMonthsSinceDate);
		newDate = DateUtils.addMonths(olddate, -10);
		when(customerMD.getRegistrationDate()).thenReturn(newDate);
		// cust_relation
		// when(customerMD.getCustomerRelation()).thenReturn(10);
		// legalFormCd
		when(customerMD.getLegalForm()).thenReturn("11");
		// branchFamilyId
		when(customerMD.getBranchFamilyId()).thenReturn(520);
		// oneMonthVisit 2<=2 21
		when(customerCD.getNumPurchasesl1m()).thenReturn((long) 2);
		// sellValNspL1M
		when(customerCD.getSellValNspl1m()).thenReturn(1l);
		// sellValNspNFL12M
		when(customerCD.getSellValNspnfl12m()).thenReturn(32l);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);

		newDate = DateUtils.addMonths(olddate, -2);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		// selValNspL12M
		when(customerCD.getSellValNspl12m()).thenReturn(0l);
		// grossProfitMargin
		when(customerMD.getGrossProfitMargin()).thenReturn("4");
		// requestedLimit
		when(creditRequest.getRequestedLimit()).thenReturn(200l);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("13");
		when(customerCD.getTimezoneCd()).thenReturn(3l);
		// when(customerMD.getBranch).thenReturn("362");
		when(customerMD.getBranchId()).thenReturn("362");
		when(customerCD.getSellValNspl6m()).thenReturn(860l);
		when(request.getProcessType()).thenReturn(null);

		assertEquals("3", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("246", scoreCalculatorDE.calculateScore().getScore().toString());
		assertEquals("1000", scoreCalculatorDE.calculateScore().getCalculatedLimit().toString());

	}


	@Test
	public void checkScoreWithDynamicIndicatorsHoreca() throws ParseException {

		// CustomerMasterData cmd=mock(CustomerMasterData.class);
		when(request.getJobMode()).thenReturn(true);
		Date olddate = new Date();
		Date newDate = DateUtils.addMonths(olddate, -36);
		// age of Company
		when(customerMD.getCompanyFoundationDate()).thenReturn(newDate);
		int numberOfMonthsSinceDate = Utils.numberOfMonthsSinceDate(customerMD.getCompanyFoundationDate());
		when(customerMD.getAgeOfCompany()).thenReturn(numberOfMonthsSinceDate);
		// when(customerMD.getRegistrationDate()).thenReturn(newDate);
		// cust_relation
		when(customerMD.getCustomerRelation()).thenReturn(21);
		// legalFormCd
		when(customerMD.getLegalForm()).thenReturn("13");
		// branchFamilyId
		when(customerMD.getBranchFamilyId()).thenReturn(122);
		// oneMonthVisit 2<=2 21
		when(customerCD.getNumPurchasesl1m()).thenReturn((long) 2);
		// sellValNspL1M
		when(customerCD.getSellValNspl1m()).thenReturn(1l);
		// sellValNspNFL12M
		// TODO not right
		when(customerCD.getSellValNspnfl12m()).thenReturn(32l);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);

		newDate = DateUtils.addMonths(olddate, -2);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		// selValNspL12M
		when(customerCD.getSellValNspl12m()).thenReturn(0l);
		// grossProfitMargin
		when(customerMD.getGrossProfitMargin()).thenReturn("4");
		// requestedLimit
		when(creditRequest.getRequestedLimit()).thenReturn(200l);
		when(customerCD.getRequestedPaymentTerms()).thenReturn("13");
		when(customerCD.getTimezoneCd()).thenReturn(3l);
		// when(customerMD.getBranch).thenReturn("362");
		when(customerMD.getBranchId()).thenReturn("362");
		when(customerCD.getSellValNspl6m()).thenReturn(860l);
		when(request.getProcessType()).thenReturn(null);
		List<DynamicIndicator> indicatorsList = new ArrayList<DynamicIndicator>();
		// D_622
		DynamicIndicator d_622 = new DynamicIndicator();
		d_622.setIndicator("behv_sell_val_nsp_l12m");
		d_622.setKey(3l);
		d_622.setValue(12l);
		// D_630
		DynamicIndicator d_630 = new DynamicIndicator();
		d_630.setIndicator("behv_sell_val_nsp_l12m");
		d_630.setKey(7l);
		d_630.setValue(500l);
		DynamicIndicator d_631 = new DynamicIndicator();
		d_631.setIndicator("behv_sell_val_nsp_l12m");
		d_631.setKey(8l);
		d_631.setValue(25l);
		DynamicIndicator d_763 = new DynamicIndicator();
		d_763.setIndicator("behv_sell_val_nsp_l12m");
		d_763.setKey(13l);
		d_763.setValue(25l);

		// behv_sell_val_nsp_l12m % from sell_val_nsp_l12m
		DynamicIndicator p_784 = new DynamicIndicator();
		p_784.setIndicator("behv_sell_val_nsp_l12m");
		p_784.setKey(19l);
		p_784.setValue(1l);
		//

		//when(customerCD.getSellValNspnfl12m()).thenReturn((long) 750);

//		DynamicIndicator d_770 = new DynamicIndicator();
//		d_770.setIndicator("behv_sell_val_nsp_l12m");
//		d_770.setKey(3l);
//		d_770.setValue(101l);

		indicatorsList.add(d_622);
		indicatorsList.add(d_630);
		indicatorsList.add(d_631);
		indicatorsList.add(p_784);
		indicatorsList.add(d_763);
//		indicatorsList.add(d_770);
		when(customerCD.getDynamicIndicatorList()).thenReturn(indicatorsList);

		assertEquals("3", scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		assertEquals("374", scoreCalculatorDE.calculateScore().getScore().toString());

	}

	// SAP_TESTS
	@Ignore
	@Test()
	public void checkMissingTurnover() throws ParseException {

		CustomerMasterData customerMD = new CustomerMasterData();
		when(request.getCustomerMasterData()).thenReturn(customerMD);
		when(creditRequest.getRequestedLimit()).thenReturn(1500l);
		when(customerMD.getBranchFamilyId()).thenReturn(383);// =>TRADER
		Date olddate = new Date();
		// Date newDate = DateUtils.addHours(olddate, -3);
		Date newDate = DateUtils.addMonths(olddate, -3);
		when(customerMD.getLastRequestDate()).thenReturn(newDate);

		when(request.getRequestHasComment()).thenReturn(false);
		when(request.getRequestHasAttachment()).thenReturn(false);
		when(request.getProcessType()).thenReturn(ProcessTypeEnum.LIMIT_CHECK_REQUEST_PROCESS);
		// assertEquals("2",
		// scoreCalculatorDE.calculateScore().getTrafficLightID().toString());
		// assertEquals("436",
		// scoreCalculatorDE.calculateScore().getRejectReasonID().toString());
		assertEquals("152", scoreCalculatorDE.calculateScore().getScore().toString());
	}

	

}
